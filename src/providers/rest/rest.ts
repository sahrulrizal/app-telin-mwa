import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {
  apiUrl = "http://150.242.111.44/apibonita/index.php/profile/getProfile/";
  mwaUrl = "http://150.242.111.44/apibonita/index.php/mwa/getOutlets";

  constructor(public http: HttpClient) {
    console.log("RestProvider");
  }
  getUsers(id) {
    return new Promise(resolve => {
      this.http.get(this.apiUrl + id).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
        }
      );
    });
  }
  getOutlet() {
    return new Promise(resolve => {
      this.http.get(this.mwaUrl).subscribe(
        data => {
          resolve(data);
        },
        err => {
          console.log(err);
        }
      );
    });
  }
}
