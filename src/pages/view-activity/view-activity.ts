import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { Observable } from "rxjs/Observable";
import { StockSurveyPage } from '../stock-survey/stock-survey';
import { ServiceProvider } from "../../providers/service/service";

/**
 * Generated class for the ViewActivityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-activity',
  templateUrl: 'view-activity.html',
})
export class ViewActivityPage {
 	isChecked:any = [];
  	id:any = this.navParams.get('id');
	idRtd:any = this.navParams.get('idRtd');

  constructor(public navCtrl: NavController, public service : ServiceProvider, public navParams: NavParams,public http: HttpClient,private sanitizer: DomSanitizer) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewActivityPage');
    this.getViewActiviy();
  }

  data:any = [];
  saved:any;
  	
  cekStatusString(c){
  		if (c == '1') {
  			return true;
  		}else{
  			return false;
  		}
  	}

  	cekStatusInt(c){
  		if (c == true) {
  			return 1;
  		}else{
  			return 0;
  		}
  	}

  	getViewActiviy(){
  		 let data:Observable<any>;
			data = this.http.get(this.service.urlRoot+'mwa/getActivity?idTask='+this.id+'&idRtd='+this.idRtd);
  		 data.subscribe(result => {
  		 	for(let i=0; i < result.length; i++){ // n is array.length
  		 		let d = result[i];
  		 		this.isChecked[d.id] = this.cekStatusString(d.check);
			   this.data.push({ 
			   		id : d.id,
			   		tasks_id : d.tasks_id,
			   		activity : d.activity,
			   		check : this.cekStatusString(d.check)
			   	 });
			}
			console.log(this.data);
  		 })
  	}

  	updateViewActivity(id,check){
  		let data:Observable<any>;
			data = this.http.get(this.service.urlRoot+'mwa/updateActivity?id='+id+'&check='+check);
  		 data.subscribe(result => {
  		 	// this.saved = "<ion-label color='#success' style='font-size:12px;'>saved</ion-label>";
  		 })
  	}

    doCheck(id,check) {
 		this.updateViewActivity(id,this.cekStatusInt(this.isChecked[id]));
    }

	next(){
		this.navCtrl.push(StockSurveyPage);
	}

}
