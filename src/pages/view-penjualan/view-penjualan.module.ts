import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewPenjualanPage } from './view-penjualan';

@NgModule({
  declarations: [
    ViewPenjualanPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewPenjualanPage),
  ],
})
export class ViewPenjualanPageModule {}
