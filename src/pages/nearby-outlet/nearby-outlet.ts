import { Component, ViewChild, ElementRef } from "@angular/core";
import { NavController } from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { Diagnostic } from "@ionic-native/diagnostic";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import * as io from "socket.io-client";
import { ServiceProvider } from "../../providers/service/service";

declare var google;

@Component({
  selector: "nearby-outlet",
  templateUrl: "nearby-outlet.html"
})
export class NearbyOutletPage {
  @ViewChild("maps") mapElement: ElementRef;
  map: any;
  kords: any;
  lastOpenedInfoWindow: any;
  ukur: any;
  lat: any;
  lng: any;
  socket: any;
  marker: any;
  neighborhoods: any = [];

  // neighborhoods = [
  //   { lat: -6.239363, lng: 106.927389 },
  //   { lat: -6.242541, lng: 106.931585 },
  //   { lat: -6.242221, lng: 106.929622 },
  //   { lat: -6.241618, lng: 106.931231 },
  //   { lat: -6.244802, lng: 106.931306 },
  //   { lat: -6.243051, lng: 106.927808 }
  // ];
  markers = [];

  constructor(
    public navCtrl: NavController,
    public service : ServiceProvider,
    public http: HttpClient,
    public geolocation: Geolocation,
    private diagnostic: Diagnostic
  ) {
    let successCallback = isAvailable => {
      if (isAvailable == true) {
      } else {
        alert("Please Enable Your Location.");
      }
    };
    let errorCallback = e => {
      alert("Error GPS tidak ditemukan : " + e);
    };
    this.diagnostic
      .isGpsLocationEnabled()
      .then(successCallback)
      .catch(errorCallback);

    this.socket = io("http://150.242.111.235:3000");
    let watch = this.geolocation.watchPosition();
    watch.subscribe(data => {
      this.lat = data.coords.latitude;
      this.lng = data.coords.longitude;
      this.send(data);
    });
    this.getNearby();
  }

  getNearby() {
    let neighborhoods: Observable<any>;
    neighborhoods = this.http.get(this.service.urlRoot+"mwa/getNearby");
    neighborhoods.subscribe(result => {
      for (var i = 0; i < result.length; ++i) {
        this.neighborhoods.push({
          lat: +result[i].lat,
          lng: +result[i].lng,
          name: result[i].name,
          phone_number: result[i].phone_number,
          email: result[i].email
        });
      }
      console.log(this.neighborhoods);
    });
  }

  send(msg) {
    this.socket.emit("nearby",  {id:this.socket.id , msg:msg});
  }

  ionViewWillEnter(){
    this.loadMap();
  }


  rad(x) {
    return (x * Math.PI) / 180;
  }

  getDistance(p1, p2) {
    var R = 6378137;
    var dLat = this.rad(p2.lat - p1.lat);
    var dLong = this.rad(p2.lng - p1.lng);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.rad(p1.lat)) *
        Math.cos(this.rad(p2.lat)) *
        Math.sin(dLong / 2) *
        Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
  }

  doRefresh(refresher) {
    console.log("Begin async operation", refresher);

    setTimeout(() => {
      console.log("Async operation has ended");
      // refresher.complete();
    }, 2000);
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then(
      position => {
        var latLng = new google.maps.LatLng(
          position.coords.latitude,
          position.coords.longitude
        );
        let lat = position.coords.latitude;
        let lng = position.coords.longitude;

        this.map = new google.maps.Map(this.mapElement.nativeElement);
        let markerUser = new google.maps.Marker({
          position: latLng,
          map: this.map,
          animation: google.maps.Animation.DROP
        });
        this.socket.on("nearby", msg => {
          console.log(msg);
          markerUser.setPosition({ lat: this.lat, lng: this.lng });
          this.ukur;
        });
        let content =
          "<div style='float:right; padding: 10px;'><h5><b>You are here!</b></h5><br/>" +
          lat +
          "," +
          lng +
          "</div>";
        this.addInfoWindow(markerUser, content);
        var circle = new google.maps.Circle({
          map: this.map,
          radius: 400,
          fillColor: "#7caeff",
          strokeOpacity: 0.3,
          strokeWeight: 1
        });
        circle.bindTo("center", markerUser, "position");
        this.map.fitBounds(circle.getBounds());
        for (var i = 0; i < this.neighborhoods.length; i++) {
          var objuser = {
            lat: markerUser.position.lat(),
            lng: markerUser.position.lng()
          };
          var neighLoc = this.getDistance(this.neighborhoods[i], objuser);
          if (neighLoc < 400) {
            this.addMarker(
              this.neighborhoods[i],
              i * 200,
              this.neighborhoods[i].name,
              this.neighborhoods[i].phone_number,
              this.neighborhoods[i].email
            );
            if (neighLoc > 400) {
              this.marker.setMap(null);
            }
          }
        }
      },
      err => {
        console.log(err);
      }
    );
  }

  addMarker(lat, lng, nme, no, email) {
    let cen = this.map.getCenter();
    if (lat !== undefined || lng !== undefined) {
      cen = new google.maps.LatLng(lat, lng);
    }
    let marker = new google.maps.Marker({
      map: this.map,
      icon: "./assets/img/position.png",
      animation: google.maps.Animation.DROP,
      position: cen
    });
    console.log(lat, lng);
    let content =
      "<div style='float:right; padding: 10px;'><b>Name : </b>" + nme +
      "<br/><b>Contact No : </b>" + no +
      "<br/><b>Email : </b>" + email +
      "<br/><hr>" + lat.lat + "," + lat.lng +
      "<button ion-button>Detail</button>"+
      "</div>";
    this.addInfoWindow(marker, content);
  }
  addInfoWindow(marker, content) {
    let infoWindow = new google.maps.InfoWindow({
      content: content,
      maxHeight: 100
    });
    google.maps.event.addListener(marker, "click", () => {
      console.log(marker);
      this.closeLastOpenedInfoWindow();
      infoWindow.open(this.map, marker);
      this.lastOpenedInfoWindow = infoWindow;
    });
  }
  closeLastOpenedInfoWindow() {
    if (this.lastOpenedInfoWindow) {
      this.lastOpenedInfoWindow.close();
    }
  }
}
