import { Component } from '@angular/core';
import { NavController, App, Platform, PopoverController } from "ionic-angular";
import { NearByPage } from '../near-by/near-by';
import { AlertController } from "ionic-angular";
import { AuthServiceProvider } from "../../providers/auth-service/auth-service";
import { ProfilePage } from '../profile/profile';
import { TodoListPage } from '../todo-list/todo-list';
import { MenuController } from "ionic-angular";
import { LoginPage } from "../login/login";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { RestProvider } from "../../providers/rest/rest";
import { Geolocation } from "@ionic-native/geolocation";
import { Diagnostic } from "@ionic-native/diagnostic";
import { ServiceProvider } from "../../providers/service/service";

declare var google;

@Component({
  selector: "page-bonita-mwa",
  templateUrl: "bonita-mwa.html"
})
export class BonitaMWAPage {
  userDetails: any;
  resposeData: any;
  public platfrom: any;
  public users: any;
  public items: any = [];

  userPostData = { user_id: "", token: "" };
  constructor(
    public navCtrl: NavController,
    public geolocation: Geolocation,
    private diagnostic: Diagnostic,
    public alertCtrl: AlertController,
    public authService: AuthServiceProvider,
    public app: App,
    public menuCtrl: MenuController,
    public platform: Platform,
    public restProvider: RestProvider,
    public http: HttpClient,
    public popoverCtrl: PopoverController,
    public service : ServiceProvider
  ) {
    // let successCallback = isAvailable => {
    //   if (isAvailable == true) {
    //   } else {
    //     alert("Please Enable Your Location");
    //   }
    // };
    // let errorCallback = e => {
    //   alert("Error GPS Not Found: " + e);
    // };
    // this.diagnostic
    //   .isGpsLocationEnabled()
    //   .then(successCallback)
    //   .catch(errorCallback);

    // this.getLoc();
    // console.log(latLng);
    let data: Observable<any>;
    // tabel = getTodolist
    data = this.http.get(
      this.service.urlRoot+"mwa/getRouteDataTask?id=" +
        JSON.parse(localStorage.getItem("id"))+'&limit=5'
    );
    data.subscribe(result => {
      for (var i = 0; i < result["data"].length; ++i) {
        this.items.push({
          task_name: result["data"][i].task_name,
          idRt: result["data"][i].route_trip_fk,
          idTask: result["data"][i].id,
          status: this.getStatus(result["data"][i].status)
        });
      }
      console.log(this.items);
    });
    this.getUsers();
    if (localStorage.getItem("id") == null) {
      this.navCtrl.push(LoginPage);
    } else {
      console.log("berhasil login");
    }
  }

  getLoc() {
    this.geolocation.getCurrentPosition().then(
      position => {
        var latLng = new google.maps.LatLng(
          position.coords.latitude,
          position.coords.longitude
        );
    })
  }

  LoginPage() {
    const root = this.app.getRootNav();
    root.popToRoot();
  }
  
  logout() {
    const confirm = this.alertCtrl.create({
      title: "Logout",
      message: "Would you like to logout?",
      buttons: [
        {
          text: "No",
          handler: () => {
            console.log("Failed");
          }
        },
        {
          text: "Yes",
          handler: () => {
            localStorage.clear();
            this.navCtrl.push(LoginPage);
          }
        }
      ]
    });
    confirm.present();
  }
  getUsers() {
    let i = JSON.parse(localStorage.getItem("id"));
    let data: Observable<any>;
    data = this.http.get(
      this.service.urlRoot+"profile/getProfile/" + i
    );
    data.subscribe(result => {
      this.users = result;
      console.log(this.users);
    });
  }

  getStatus(x) {
    if (x === "1") {
      return "In Progress";
    } else if (x === "2") {
      return "Follow Up";
    } else if (x === "3") {
      return "Completed";
    }
  }

  goToProfile() {
    this.navCtrl.push(ProfilePage);
  }
  goToTodoList() {
    this.navCtrl.push(TodoListPage);
  }
  goToNearBy() {
    this.navCtrl.push(NearByPage);
  }
}