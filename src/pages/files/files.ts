import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Observable } from "rxjs/Observable";
import { HttpClient } from "@angular/common/http";
import { PhotoViewer } from "@ionic-native/photo-viewer";
import { File } from "@ionic-native/file";
import { ServiceProvider } from "../../providers/service/service";

/**
 * Generated class for the FilesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-files",
  templateUrl: "files.html"
})
export class FilesPage {
  x: string;
  private dirs: any;
  nativepath: string;
  data: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private photoViewer: PhotoViewer,
    private http: HttpClient,
    private file: File,
    public service : ServiceProvider
  ) {
    this.goToDir();
  }
  goToDir() {
    this.file
      .listDir(this.file.externalRootDirectory, "Download/Bonita-MWA/uploads/")
      .then(list => {
        this.dirs = list;
      });
  }

  open(x) {
    this.photoViewer.show(this.file.externalRootDirectory + "/Download/Bonita-MWA/uploads/" + x.name);
  }
}
