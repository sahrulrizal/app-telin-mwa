import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController, LoadingController } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
import { RoutePage } from '../route/route';
import { ServiceProvider } from "../../providers/service/service";

/**
* Generated class for the SalesOrderPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-sales-order',
  templateUrl: 'sales-order.html',
})
export class SalesOrderPage {
  
  nominal:any = [];
  discount:any = [];
  unit_price:any = [];
  url:any = "http://150.242.111.235/apibonita/index.php/mwa/";
  jml:any;
  product:any;
  balance:any;
  default:any = 0;
  simcard:any = {};
  etopup:any = {};
  total:any = [];
  dollar:any = [];
  
  constructor(public http: HttpClient,public service : ServiceProvider,public navCtrl: NavController,public loadingCtrl: LoadingController, public navParams: NavParams,private alertCtrl: AlertController) {
    this.getIDJProduct(2);
    this.getIDJProductSimcard(1);
    this.getIDJProductEtopup(3);
    
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad SalesOrderPage');
    
  }
  
  changeValue(id){
      let voucher = [];
    
      let nom = this.nominal.length;
      let dis = this.discount.length;
      let jml = 0;
      
      if(nom != 0){
        jml = nom;
      }else if(dis != 0){
        jml = dis;
      }
      
      for (var i = 0; i < jml; ++i) {
        if(this.nominal[i] != undefined || this.discount[i] != undefined){
          this.dollar[i] = (this.nominal[i] * this.unit_price[i] *  this.discount[i]) / 100
          this.total[i] = ((this.nominal[i] * this.unit_price[i]) - this.dollar[i]);
        }
      }
      
      // Not Looping
      this.simcard.dollar = (this.simcard.nominal * this.simcard.price *  this.simcard.discount) / 100; 
      this.simcard.total = (this.simcard.nominal * this.simcard.price - this.simcard.dollar);
      
      this.etopup.dollar = (this.etopup.nominal * this.etopup.price * this.etopup.discount) / 100; 
      this.etopup.total = (this.etopup.nominal * this.etopup.price - this.etopup.dollar);
  }
  
  getIDJProduct(id) {
    this.product = [];
    this.http.get(this.service.urlRoot+'mwa/getIDJProduct?id='+id).subscribe(data => {
      let d:any = data; 
      for (let i = 0; i < d.length; i++) {
        
        this.total[d[i].id] = '0';
        this.unit_price[d[i].id] = '0';
        this.discount[d[i].id] = '0';
        this.dollar[d[i].id] = '0';
        this.nominal[d[i].id] = '0';

        this.product.push({
          id : d[i].id,
          unit_price : d[i].unit_price,
          discount : d[i].discount,
          product_name : d[i].product_name,
          value : '0'
        });
    }
    });
  }
  
  getIDJProductSimcard(id) {
    this.http.get(this.service.urlRoot+'mwa/getIDJProduct?id='+id).subscribe(data => {
      this.simcard.price = data[0].unit_price;
      this.simcard.discount = data[0].discount;
    });
  }
  
  getIDJProductEtopup(id) {
    this.http.get(this.service.urlRoot+'mwa/getIDJProduct?id='+id).subscribe(data => {
      this.etopup.price = data[0].unit_price;
      this.etopup.discount = data[0].discount;
    });
  }
  
  toSalesOrder(){
    
    let loading = this.loadingCtrl.create({
      content: "Please Wait.."
    });
    loading.present();

    let voucher = [];
    
    let nom = this.nominal.length;
    let dis = this.discount.length;
    let jml = 0;
    
    if(nom != 0){
      jml = nom;
    }else if(dis != 0){
      jml = dis;
    }
    
    for (var i = 0; i < jml; ++i) {
      if(this.nominal[i] != undefined || this.discount[i] != undefined){
        voucher.push({ 
          nominal : this.nominal[i],
          discount : this.discount[i],
          idProduct : i,
          dollar : this.dollar[i],
          idJp : 2,
          total: this.total[i]
        });
      }
    }
    
    let s = this.simcard;
    let e = this.etopup;
    
    let SE = {
      simcard : [{
        nominal : this.validValue(s.nominal),
        discount : this.validValue(s.discount),
        idProduct : 9999,
        dollar : s.dollar, 
        total : s.total, 
        idJp : 1
      }],
      etopup : [{
        nominal : this.validValue(e.nominal),
        discount : this.validValue(e.discount),
        idProduct : 6666,
        dollar : e.dollar,
        total : e.total, 
        idJp : 3
      }],
    }
    
    let data = new FormData;
    data.append('SE', JSON.stringify(SE));
    data.append('order', JSON.stringify(voucher));
    data.append('idu', localStorage.getItem('id'));
    data.append('idCh', localStorage.getItem('idCh'));
    data.append('oId', localStorage.getItem('oId'));
    data.append('idTask', localStorage.getItem('idTask'));
    data.append('idRtd', localStorage.getItem('idRtd'));
    data.append('stId', localStorage.getItem('stId'));
    data.append('balance', this.balance);
    this.http.post(this.service.urlRoot+'mwa/saveSalesOrder',data).subscribe( d => {
      let alert = this.alertCtrl.create({
        title: 'Info',
        subTitle: 'The Data Survey has been Added',
        buttons: ['Okay']
      });
      alert.present();
      loading.dismiss();

      this.navCtrl.push(RoutePage, {
        'id' : localStorage.getItem('idFromRoot'),
        'idTask' : localStorage.getItem('idTask')
      });
    });
    
  }
  
  validValue(v){
    if(v == undefined){
      v = '';  
    }
    return v;
  }
  
}
