import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  LoadingController,
  NavParams,
  AlertController,
  MenuController,
  ToastController
} from "ionic-angular";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { BonitaMWAPage } from "../bonita-mwa/bonita-mwa";
import { Geolocation } from "@ionic-native/geolocation";
import { ServiceProvider } from "../../providers/service/service";

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  items: any;
  alert: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpClient,
    private geolocation: Geolocation,
    public loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private menu: MenuController,
    public toastCtrl: ToastController,
    public service : ServiceProvider
  ) {
    if (localStorage.getItem("id") != null) {
      this.navCtrl.setRoot(BonitaMWAPage);
    }
    console.log("ini adalah : " + localStorage.getItem("id"));
  }

  @ViewChild("username") uname;
  @ViewChild("password") password;

  ionViewDidLoad() {
    console.log("ionViewDidLoad LoginPage");
    this.menu.swipeEnable(false);
  }

  alertGagal(u, p) {
    if (u == "") {
      let alert = this.alertCtrl.create({
        title: "Cannot Login",
        subTitle: "Please enter your username!",
        buttons: ["Okay"]
      });
      alert.present();
    } else if (p == "") {
      let alert = this.alertCtrl.create({
        title: "Cannot Login",
        subTitle: "Please enter your password!",
        buttons: ["Okay"]
      });
      alert.present();
    } else {
      let alert = this.alertCtrl.create({
        title: "Cannot Login",
        subTitle:
          "The username or password you entered is incorrect. Please try again!",
        buttons: ["Try Again"]
      });
      alert.present();
    }
  }

  login() {
    let loading = this.loadingCtrl.create({
      content: "Logging in..."
    });
    loading.present();
    let data: Observable<any>;
    data = this.http.get(this.service.urlRoot+"auth/loginMwa?username=" + this.uname.value + "&password=" + this.password.value);
    data.subscribe(result => {
      this.items = result;
      if (this.items["success"] == false) {
        this.alertGagal(this.uname.value, this.password.value);
        loading.dismiss();
      } else {
        localStorage.setItem("id",this.items[0].id);
        localStorage.setItem("idCh", this.items[0].idch);
      localStorage.setItem("chID", this.items[0].chID);
        localStorage.setItem("stId", this.items[0].stId);
        localStorage.setItem("cl", this.items[0].cluster);
        
        this.geolocation.getCurrentPosition().then((resp) => {
         this.updateLocationSales(resp.coords.latitude,resp.coords.longitude);
        }).catch((error) => {
          console.log('Error getting location', error);
        });

        this.navCtrl.setRoot(BonitaMWAPage);
        loading.dismiss();
        console.log(this.items);
      }
    });
  }

  updateLocationSales(lat, lng) {

    let body: FormData = new FormData();
    body.append("active", '1');
    body.append("lat", lat);
    body.append("lng", lng);
    body.append("users_id", JSON.parse(localStorage.getItem("id")));

    this.http
      .post(
        this.service.urlRoot+"mwa/upLocationSales",
        body
      )
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        }
      );

  }

  forgetPassword() {
    const prompt = this.alertCtrl.create({
      title: "Forget Password",
      subTitle: "Enter your username:",
      inputs: [{
          name: "Username",
          placeholder: "Username"
        }],
      buttons: [{
          text: "Cancel",
          handler: data => {
            console.log("Cancel Clicked");
          }},
        {
          text: "Confirm",
          handler: data => {
            let body: FormData = new FormData();
            body.append("username", data.Username);
            this.http
              .post(
                this.service.urlRoot+"mwa/forgetPassword",
                body
              )
              .subscribe(res => {
                if (res !== null) {
                  console.log(res);
                  this.toast(
                    "We have sent message to your phone number that registered to your account."
                  );
                } else {
                  this.toast("Username is not registered.");
                }
              });
          }
        }
      ]
    });
    prompt.present();
  }
  toast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "top"
    });
    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });
    toast.present();
  }
}