import { Component } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ServiceProvider } from "../../providers/service/service";

/**
 * Generated class for the ViewAllPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-all',
  templateUrl: 'view-all.html',
})
export class ViewAllPage {

  stock:any = {};
  spenjualan:any = {};
  order:any = {};
  viewActivity:any;
  survey:any;
  menu:any;

  url = "http://150.242.111.235/apibonita/index.php/mwa/";
  oId = localStorage.getItem('oId');
  constructor(public navCtrl: NavController,private http: HttpClient,public service : ServiceProvider, public navParams: NavParams) {
    this.menu = "sStock";
    this.getStockSurvei(this.oId);
    this.getSalesOrder(this.oId);
    this.getSurvey(this.oId);
    this.getViewActivity(this.oId);
    this.getSurveiPenjualan(this.oId);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewAllPage');
  }

  getStockSurvei(id){
    this.http.get(this.service.urlRoot+'mwa/getStockSurvei?id='+id+'&idTask='+localStorage.getItem('idTask')).subscribe(data => {
      this.stock.simcard = data['simcard'];
      this.stock.etopup = data['etopup'];
      this.stock.voucher = data['voucher'];
      this.stock.bundling = data['bundling'];
    });
  }
  
  getSalesOrder(id){
    this.http.get(this.service.urlRoot+'mwa/getSalesOrder?id='+id+'&idTask='+localStorage.getItem('idTask')).subscribe(data => {
      this.order.simcard = data['simcard'];
      this.order.etopup = data['etopup'];
      this.order.voucher = data['voucher'];
    });
  }

  getSurveiPenjualan(id){
    this.http.get(this.service.urlRoot+'mwa/getSurveiPenjualan?id='+id+'&idTask='+localStorage.getItem('idTask')).subscribe(data => {
      this.spenjualan.simcard = data['simcard'];
      this.spenjualan.etopup = data['etopup'];
      this.spenjualan.voucher = data['voucher'];
      this.spenjualan.bundling = data['bundling'];
    });
  }

  getSurvey(id){
    this.http.get(this.service.urlRoot+'mwa/getSurvey?id='+id+'&idTask='+localStorage.getItem('idTask')).subscribe(data => {
      this.survey = data;
    });
  }

  getViewActivity(id){
    this.http.get(this.service.urlRoot+'mwa/getViewActivity?id='+id+'&idTask='+localStorage.getItem('idTask')).subscribe(data => {
      this.viewActivity= data;
    });
  }

}
