import { Component } from "@angular/core";
import {
  NavController,
  LoadingController,
  ToastController,
  AlertController,
  ActionSheetController,
  Platform
} from "ionic-angular";
import {
  FileTransfer,
  FileTransferObject
} from "@ionic-native/file-transfer";
import { File } from "@ionic-native/file";
import { HttpClient } from "@angular/common/http";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { Observable } from "rxjs/Observable";
import { PhotoViewer } from "@ionic-native/photo-viewer";
// import { FileOpener } from "@ionic-native/file-opener";
// import { FileChooser } from '@ionic-native/file-chooser';
import { FilePath } from "@ionic-native/file-path";
import { FilesPage } from "../files/files";
import { ServiceProvider } from "../../providers/service/service";

@Component({
  selector: "page-cloud-sharing",
  templateUrl: "cloud-sharing.html"
})
export class CloudSharingPage {
  cloud: string = "My Phone";
  isAndroid: boolean = true;
  img1: any;
  imageURI: any;
  imageFileName: any;
  tempFile: any;
  data: any;

  constructor(
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    private transfer: FileTransfer,
    private camera: Camera,
    public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private http: HttpClient,
    private photoViewer: PhotoViewer,
    private file: File,
    private filePath: FilePath,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public service : ServiceProvider

  ) {
  }

  ionViewWillEnter(){
    this.getCloudSharing();
  }

  ubah(){
    this.getCloudSharing();
  }

  show(v) {
    // this.photoViewer.show("http://150.242.111.235/apibonita/" + v);
    let loading = this.loadingCtrl.create({
      content: "Downloading..."
    });
    loading.present();
    const FileTransfer: FileTransferObject = this.transfer.create();
    const url = this.service.url + v;
    var Path = this.file.externalRootDirectory + "/Download/Bonita-MWA/";
    console.log(url);
    FileTransfer.download(url, Path + v).then(
      entry => {
        this.presentToast("Download Complete: " + entry.toURL());
        loading.dismiss();
      },
      error => {
        this.presentToast("Error " + error);
        console.log(error);
        loading.dismiss();
      }
    );
  }

  getCloudSharing() {
    let data: Observable<any>;
    data = this.http.get(
      this.service.urlRoot+"mwa/getCloudSharing"
    );
    data.subscribe(result => {
      this.data = result;
      console.log(data);
    });
  }

  uploadFile(event) {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (event: any) => {
        this.imageURI = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
    let fileList: FileList = event.target.files;

    let files: FileList = event.target.files;
    this.tempFile = files[0];
    debugger;
    let bodyRequest: FormData = new FormData();
    bodyRequest.append("id", JSON.parse(localStorage.getItem("id")));
    bodyRequest.append("image", this.tempFile);
    bodyRequest.append("photo",  event.target.files);
    this.uploadToServer(bodyRequest).subscribe(res => {
      this.presentToast("Success upload file");
      console.log("Upload Success");
    });
  }

  getImage() {
    this.presentActionSheet();
  }

  uploadToServer(bodyRequest): Observable<any> {
    return this.http
      .post(
        this.service.urlRoot+"mwa/uploadCloudSharing",
        bodyRequest
      )
      .map((res: Response) => {
        // this.presentToast("Upload Success!")
      });
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "bottom"
    });
    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });
    toast.present();
  }

  fileChange(event) {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (event: any) => {
        this.img1 = event.target.result;
      };
      reader.readAsDataURL(event.target.files[0]);
    }
    let fileList: FileList = event.target.files;
    let files: FileList = event.target.files;
    this.tempFile = files[0];
    let bodyRequest: FormData = new FormData();
    bodyRequest.append("photo", this.tempFile);
    bodyRequest.append("img", event.target.result);
    this.uploadToServer(bodyRequest).subscribe(res => {
      console.log(res);
      console.log(event.target.result);
    });
  }

  gotoFiles() {
    this.navCtrl.push(FilesPage);
  }

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Select Image Source",
      buttons: [
        {
          text: "Load from Library",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: "Use Camera",
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: "Cancel",
          role: "cancel"
        }
      ]
    });
    actionSheet.present();
  }

  takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then(
      imageData => {
        this.imageURI = imageData;
        let p = "data:image/jpeg;base64," + imageData;
      },
      err => {
        console.log(err);
        this.presentToast(err);
      }
    );
  }

  upload() {
    let loader = this.loadingCtrl.create({
      content: "Uploading..."
    });
    loader.present();
    const fileTransfer: FileTransferObject = this.transfer.create();

    fileTransfer
      .upload(
        this.imageURI,
        this.service.urlRoot+"mwa/uploadCloudSharing"
      )
      .then(
        data => {
          console.log(data + " Uploaded Successfully");
          loader.dismiss();
          this.presentToast("Image uploaded successfully");
        },
        err => {
          console.log(err);
          loader.dismiss();
          this.presentToast(err);
        }
      );
  }
}
//   chooseFile() {
//     this.fileChooser.open().then(file=>{
//       this.filePath.resolveNativePath(file).then(resolveFilePath => {
//         this.fileOpener.open(resolveFilePath, 'application/pdf').then(value => {
//           alert('It worked!')
//         }).catch(err => {
//           alert(JSON.stringify(err));
//         });
//       }).catch(err => {
//         alert(JSON.stringify(err));
//     });
//   }).catch (err => {
//     alert(JSON.stringify(err));
// });
// }
