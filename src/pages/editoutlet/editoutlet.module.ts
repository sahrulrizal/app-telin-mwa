import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditoutletPage } from './editoutlet';

@NgModule({
  declarations: [
    EditoutletPage,
  ],
  imports: [
    IonicPageModule.forChild(EditoutletPage),
  ],
})
export class EditoutletPageModule {}
