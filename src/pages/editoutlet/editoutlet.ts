import { Component } from "@angular/core";
import { ActionSheetController } from "ionic-angular";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController,
  ToastController
} from "ionic-angular";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Geolocation } from "@ionic-native/geolocation";
import {
  FileUploadOptions,
  FileTransferObject
} from "@ionic-native/file-transfer";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { OutletPage } from "../outlet/outlet";
import { ServiceProvider } from "../../providers/service/service";

/**
 * Generated class for the AddoutletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
   selector: 'page-editoutlet',
  templateUrl: 'editoutlet.html',
})
export class EditoutletPage {
  items:any ;
  valdeia:any;id_area:any;
  name: any;
  contact_number: any;
  digital_number: any;
  address: any;
  lat: any;
  lng: any;
  municipio: any;
  posto_adm: any;
  suco: any;
  aldeia: any;
  created_by: any;
  data: any;
  img: any;
  photo: any;
  photoz: any;
  munic: any;
  type_outlet:any;
  type:any;
  idu:any;
  posto: any;
  sucos: any;
  aldeias: any;
  email: any;
  area:any;
  i: any = [];
  // url:any = this.service.urlRoot+"";
  clusters:any ;
  cluster:any ;
  areaCluster:any;
  p:any = [];
  s:any = [];
  constructor(
    public navCtrl: NavController,
    private camera: Camera,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public http: HttpClient,
    private geolocation: Geolocation,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public service : ServiceProvider
  ) {}

  presentActionSheet() {
    let actionSheet = this.actionSheetCtrl.create({
      title: "Choose Upload Photo",
      buttons: [
        {
          text: "Open Gallery",
          role: "gallery",
          handler: () => {
            this.toGetCamera(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: "Open Camera",
          role: "camera",
          handler: () => {
            this.toGetCamera(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: "Cancel",
          role: "cancel",
          handler: () => {
            console.log("Cancel clicked");
          }
        }
      ]
    });

    actionSheet.present();
  }

  ionViewDidLoad() {
    this.img = "../assets/img/users.png";
    let cl = localStorage.getItem('cl');
    this.getIdCluster(cl);
    this.getOutlet();
    this.getAreaCluster();
    this.getTypeOutlet();
  }

  // TO -> GET

  getTypeOutlet() {
    let data: Observable<any>;
    data = this.http.get(
      this.service.urlRoot+"mwa/getTypeOutlet"
    );
    data.subscribe(result => {
      if(result['success'] == true){
        this.type_outlet = result['data'];
      }else{
        console.log("getTypeOutlet : ",result['success']);
      }
    });
  }

  toGetPosto() {
    this.getPosto(this.municipio);
  }

  toGetSuco() {
    this.getSuco(this.posto_adm);
  }

  toGetAldeia() {
    this.getAldeia(this.suco);
  }

  toGetLocation() {
    this.geolocation
      .getCurrentPosition()
      .then(resp => {
        this.lat = resp.coords.latitude;
        this.lng = resp.coords.longitude;
      })
      .catch(error => {
        this.toast("Error getting location");
      });
  }

  getBase64(file) {
    let reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function() {
      console.log(reader.result);
    };
    reader.onerror = function(error) {
      console.log("Error: ", error);
    };
  }

  toGetCamera(mt) {
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType: mt
    };

    this.camera.getPicture(options).then(
      imageData => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        let p = "data:image/jpeg;base64," + imageData;
        this.img = p;
        this.photo = p;
      },
      err => {
        // Handle error
      }
    );
  }

  toUploadPhoto(event) {
    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.onload = (event: any) => {
        this.img = event.target.result;
        this.address = this.img;
      };
      this.address = event.target.files[0];
      reader.readAsDataURL(event.target.files[0]);
    }

    let fileList: FileList = event.target.files;
    this.photo = fileList[0];
  }

  // POST
  addOutlet() {
    
    let loading = this.loadingCtrl.create({
      content: "Please Wait.."
    });
    loading.present();

    if(this.name == "undefined" || this.name == undefined) {
      this.toast("Outlet name can not be empty!");
      loading.dismiss();
      
    } else if(this.contact_number == "undefined" || this.contact_number == undefined) {
      this.toast("Contact No. can not be empty!");
      loading.dismiss(); 
    } else {
      let body: FormData = new FormData();
      let rex = this.area.split(',');

      body.append("id", this.navParams.data.id);
      body.append("image", this.photo);
      body.append("name", this.name);
      body.append("idu", this.idu);
      body.append("contact_number", this.contact_number);
      body.append("pd_no", this.digital_number);
      body.append("cl", localStorage.getItem('cl'));
      body.append("address", this.address);
      body.append("email", this.email);
      body.append("typeOutlet", this.type);
      body.append("lat", this.lat);
      body.append("lng", this.lng);
      body.append("municipio", rex[1]);
      body.append("posto_adm", rex[2]);
      body.append("suco", rex[3]);
      body.append("aldeia", rex[0]);
      body.append("photo", this.photoz);
      body.append("created_by", localStorage.getItem("id"));
      body.append("idCh", localStorage.getItem("idCh"));

  // if(body == "") {
  //     this.toast("Error");
  // }

      this.http.post(this.service.urlRoot+"mwa/editOutlets", body)
        .subscribe(
          res => {
            this.i = res;
            if (this.i.info == 1) {
              this.toast(
                "Sorry your name has been used, please enter other name!"
              );
            } else if (this.i.info == 2) {
              this.toast(
                "Sorry your contact number has been used, please enter other number!"
              );
            }else if (this.i.info == 3) {
              this.toast(
                "Sorry your digital number credit has been used, please enter other number!"
              );
            }else if (this.i.info == 4) {
              this.toast(
                "Sorry your email entered has been used, please enter  the other email!"
              );
            }else {
              this.toast("Success to Edit Outlet");
              this.navCtrl.push(OutletPage);
            }
          },
          err => {
            console.log(err);
            this.toast("Error to Add Outlet");
          });

        loading.dismiss();
    }
  }

  // GET

  getIdCluster(id){
    this.http.get(this.service.urlRoot+'mwa/getIdCluster?id='+id).subscribe((d) => {
       this.clusters = d[0].cluster;
    });
  }

  getAreaCluster(){
    let data = new FormData;
    this.http.get(this.service.urlRoot+'mwa/getAreaCluster?cluster='+localStorage.getItem('cl')).subscribe((d) => {
       this.municipio = d['municipio'];
       this.posto_adm = d['posto_adm'];
      });
  }

  getAreaClusterSuco(){
    let loading = this.loadingCtrl.create({
      content: "Please Wait.."
    });
    
    this.http.get(this.service.urlRoot+'mwa/getAreaCluster?cluster='+localStorage.getItem('cl')+'&posto='+this.p).subscribe((d) => {
       this.suco = d['suco'];
    });
  }

  getAreaClusterAldeia(){
    let loading = this.loadingCtrl.create({
      content: "Please Wait.."
    });
    this.http.get(this.service.urlRoot+'mwa/getAreaCluster?cluster='+localStorage.getItem('cl')+'&suco='+this.s).subscribe((d) => {
       this.aldeias = d['aldeia'];
    });
  }

  

  getMuicipio() {
    let data: Observable<any>;
    data = this.http.get(
      this.service.urlRoot+"mwa/getMunicipio"
    );
    data.subscribe(result => {
      this.munic = result;
    });
  }

  getPosto(id) {
    let data: Observable<any>;
    data = this.http.get(
      this.service.urlRoot+"mwa/getPosto?id=" + id
    );
    data.subscribe(result => {
      this.posto = result;
    });
  }

  getSuco(id) {
    let data: Observable<any>;
    data = this.http.get(
      this.service.urlRoot+"mwa/getSuco?id=" + id
    );
    data.subscribe(result => {
      this.sucos = result;
    });
  }

  getAldeia(id) {
    let data: Observable<any>;
    data = this.http.get(
      this.service.urlRoot+"mwa/getAldeia?id=" + id
    );
    data.subscribe(result => {
      this.aldeias = result;
    });
  }

  toast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "bottom"
    });
    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });
    toast.present();
  }

  getOutlet(){
   let id = this.navParams.data.id;
    this.http.get(this.service.urlRoot+'mwa/getOutlets?id='+id).subscribe( data => {
       let v = data[0];
      this.name = v.name;
      this.contact_number = v.contact_number;
      this.digital_number = v.pd_no;
      this.address = v.address;
      this.lat = v.lat;
      this.email = v.email;
      this.type = v.id_typeOutlet;
      this.idu = v.id_user;
      this.lng = v.lng;
      this.p = v.p_id;
      this.s = v.s_id;
      this.getAreaClusterSuco();
      this.getAreaClusterAldeia();
      this.id_area = v.a_id+','+v.m_id+','+v.p_id+','+v.s_id;
      this.area = this.id_area;
      this.img = this.service.url+'uploads/'+v.outlet_photo;
      console.log(this.p);      
    });
  }
}
