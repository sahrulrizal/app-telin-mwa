import { Component, ViewChild } from "@angular/core";
import { NavController, ToastController, Navbar } from "ionic-angular";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { RoutePage } from "../route/route";
import { BonitaMWAPage } from "../bonita-mwa/bonita-mwa";
import { ServiceProvider } from "../../providers/service/service";

@Component({
  selector: "page-todo-list",
  templateUrl: "todo-list.html"
})
export class TodoListPage {
  public items: any = [];
  @ViewChild(Navbar) navBar: Navbar;

  constructor(
    public navCtrl: NavController,
    public http: HttpClient,
    public toastCtrl: ToastController,
    public service : ServiceProvider
  ) {
    let data: Observable<any>;
    // tabel = getTodolist
    data = this.http.get(this.service.urlRoot+"mwa/getRouteDataTask?id=" + JSON.parse(localStorage.getItem("id")));
    data.subscribe(result => {
      for (var i = 0; i < result["data"].length; ++i) {
        this.items.push({
          task_name: result["data"][i].task_name,
          idRt: result["data"][i].route_trip_fk,
          idTask: result["data"][i].id,
          status: this.getStatus(result["data"][i].status)
        });
      }
      console.log(this.items);
    });
  }

  ionViewDidLoad() {
    this.navBar.backButtonClick = (e: UIEvent) => {
      // todo something
      this.navCtrl.setRoot(BonitaMWAPage);
    };
  }

  goToRoute(idRt, idTask) {
    this.navCtrl.push(RoutePage, {
      id: idRt,
      idTask: idTask
    });
  }

  getStatus(x) {
    if (x === "1") {
      return "In Progress";
    } else if (x === "2") {
      return "Follow Up";
    } else if (x === "3") {
      return "Completed";
    }
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "bottom"
    });
    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });
    toast.present();
  }
}
