import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddoutletPage } from './addoutlet';

@NgModule({
  declarations: [
    AddoutletPage,
  ],
  imports: [
    IonicPageModule.forChild(AddoutletPage),
  ],
})
export class AddoutletPageModule {}
