import { Component, ViewChild, ElementRef } from "@angular/core";
import {
  NavController,
  NavParams,
  Toast,
  Navbar,
  ToastController
} from "ionic-angular";
import { Geolocation } from "@ionic-native/geolocation";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { ViewActivityPage } from "../view-activity/view-activity";
import { CheckinPage } from "../checkin/checkin";
import { TodoListPage } from "../todo-list/todo-list";
import { Diagnostic } from "@ionic-native/diagnostic";
import * as io from "socket.io-client";
import { ServiceProvider } from "../../providers/service/service";
import { ViewAllPage } from "../view-all/view-all";

declare var google;

@Component({
  selector: "page-route",
  templateUrl: "route.html"
})
export class RoutePage {
  @ViewChild("map") mapElement: ElementRef;
  @ViewChild(Navbar) navBar: Navbar;
  map: any;
  uuid: any;
  ceks: any;
  socket: any;
  idx: any;
  ids: any = [];
  gmarker: any = [];
  waypts: any = [];
  kordinat: any = [];
  items: any;
  ukur: any;
  ukurReal: any;
  lat: any;
  cordova: any;
  lng: any;
  latawal: any;
  lngawal: any;
  objlatawal: any;
  objnext: any;
  lastOpenedInfoWindow: any;
  locawal = { lat: 123, lng: 123 };
  id: any = this.navParams.get("id");
  idTask: any = this.navParams.get("idTask");

  constructor(
    public navCtrl: NavController,
    private diagnostic: Diagnostic,
    public geolocation: Geolocation,
    public navParams: NavParams,
    private toastCtrl: ToastController,
    public http: HttpClient,
    public service : ServiceProvider,
    private setting : OpenNativeSettings
  ) {


    let successCallback = isAvailable => {
      if (isAvailable == true) {
      } else {
        this.setting.open("location");
      }
    };
    let errorCallback = e => {
      alert("Error GPS Not Found: " + e);
    };
    this.diagnostic
      .isGpsLocationAvailable()
      .then(successCallback)
      .catch(errorCallback);

    this.socket = io("http://150.242.111.235:3000");
    let watch = this.geolocation.watchPosition();
    watch.subscribe(data => {
      this.lat = data.coords.latitude;
      this.lng = data.coords.longitude;
      this.send(this.id);
      this.ukurReal;
    });
  }

  send(msg) {
    this.uuid = this.socket.emit("message", {id:this.socket.id , msg:msg});
  }

  rad(x) {
    return (x * Math.PI) / 180;
  }

  getDistance(p1, p2) {
    var R = 6378137;
    var dLat = this.rad(p2.lat - p1.lat);
    var dLong = this.rad(p2.lng - p1.lng);
    var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(this.rad(p1.lat)) *
        Math.cos(this.rad(p2.lat)) *
        Math.sin(dLong / 2) *
        Math.sin(dLong / 2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;
    return d;
  }

  ionViewDidLoad() {
    this.getRoute();
    
    this.navBar.backButtonClick = (e:UIEvent)=>{
      // todo something
      this.navCtrl.push(TodoListPage);
     }
     localStorage.setItem('idTask',this.idTask);
  }

  getRoute() {
    let data: Observable<any>;
    data = this.http.get(this.service.urlRoot+"mwa/getRouteMapUrut?id=" + this.id+'&idTask='+ this.idTask);
    data.subscribe(result => {
      this.items = result;
      if (this.items["success"] == false) {
        this.ceks = 0;
        console.log(this.ceks);
        this.socket.off("message");
        this.socket.disconnect("message");
      } else {
        this.ceks = 1;
        this.loadMap();
        console.log(this.items);
      }
    });
  }

  upTask(s, idTask,s2='',id='') {
    let data: Observable<any>;
    data = this.http.get(this.service.urlRoot+"mwa/upStatusTask/" + s + "/" + idTask +'/'+ s2+'/'+id);
    data.subscribe(result => {
      console.log(result);
    });
  }

  getRoute2(a = "", b = "") {
    let data: Observable<any>;
    data = this.http.get(this.service.urlRoot+"mwa/getRouteGMapUrut?id=" + this.id + '&idTask='+ this.idTask);
    data.subscribe(result => {
      for (var i = 0; i < result.length; ++i) {
        this.kordinat.push({
          lat: +result[i].lat,
          lng: +result[i].lng
        });
        this.ids.push({
          id: +result[i].id
        });
        this.objlatawal = { lat: this.lat, lng: this.lng };
        this.objnext = { lat: result[i].lat, lng: result[i].lng };
        this.ukurReal = this.getDistance(this.objlatawal, this.objnext);
      }
      this.kordinat.push({
        lat: this.lat,
        lng: this.lng,
        img: 1
      });
      console.log(this.kordinat);
    });
  }

  loadMap() {
    this.geolocation.getCurrentPosition().then(
      position => {
        let latLngUtama = new google.maps.LatLng(this.lat, this.lng);
        let latLng = new google.maps.LatLng(
          position.coords.latitude,
          position.coords.longitude
        );
        this.getRoute2();

        setTimeout(() => {
          let mapOptions = {
            center: latLng,
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };
          let map = new google.maps.Map(
            this.mapElement.nativeElement,
            mapOptions
          );
          latLng = new google.maps.LatLng(this.lat, this.lng);
          mapOptions = {
            center: latLng,
            zoom: 18,
            mapTypeId: google.maps.MapTypeId.ROADMAP
          };

          for (let i = 0; i < this.kordinat.length; i++) {
            let obj = {};
            obj["location"] = new google.maps.LatLng(
              parseFloat(this.kordinat[i].lat),
              parseFloat(this.kordinat[i].lng)
            );
            obj["stopover"] = true;
            this.waypts.push(obj);
          }
          console.log(this.waypts);
          map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
          let directionsService = new google.maps.DirectionsService();
          let directionsDisplay = new google.maps.DirectionsRenderer({
            suppressInfoWindows: true,
            suppressMarkers: true
          });
          let locend = this.kordinat[this.kordinat.length - 1];
          console.log(locend);
          directionsDisplay.setMap(map);
          directionsService.route(
            {
              origin: new google.maps.LatLng(this.lat, this.lng),
              destination: new google.maps.LatLng(locend.lat, locend.lng),
              waypoints: this.waypts,
              optimizeWaypoints: true,
              travelMode: "DRIVING"
            },
            function(response, status) {
              if (status === "OK") {
                directionsDisplay.setDirections(response);
              }
            }
          );

          let marker;
          for (var i = 0; i < this.kordinat.length; i++) {
            if (this.kordinat[i].img == 1) {
              marker = new google.maps.Marker({
                position: this.kordinat[i],
                icon: "./assets/img/home.png",
                map: map,
                title: "Home"
              });
            } else {
              marker = new google.maps.Marker({
                position: this.kordinat[i],
                map: map,
                // icon : './assets/img/flag.png',
                // labelContent: "ABCD",
                title: "Order to " + i
              });
            }
            this.gmarker.push(marker);
            let ok = this.ids[i];
            this.addInfoWindow(marker, ok);
          }
          // debugger;
          directionsDisplay.setMap(map);
          // var jalur = new google.maps.Polyline({
          //   path: this.kordinat,
          //   geodesic: true,
          //   strokeColor: '#FF0000',
          //   strokeOpacity: 1.0,
          //   strokeWeight: 2
          // });

          var lokasiHp = new google.maps.Marker({
            position: latLng,
            map: map,
            icon: "./assets/img/run.png",
            title: "Me "
          });

          this.socket.on("message", msg => {
            console.log(msg);
            lokasiHp.setPosition({ lat: this.lat, lng: this.lng });
            this.ukur;
            this.ukurReal;
          });

          // jalur.setMap(map);
        }, 500);

        // console.log(position.coords.latitude+','+position.coords.longitude);
      },
      err => {
        console.log(err);
      }
    );
  }

  addInfoWindow(marker, id) {
    google.maps.event.addListener(marker, "click", () => {
      this.closeLastOpenedInfoWindow();
      let posisiAwal = { lat: this.lat, lng: this.lng };
      let kePosisi = {
        lat: marker.getPosition().lat(),
        lng: marker.getPosition().lng()
      };
      this.ukur = this.getDistance(posisiAwal, kePosisi);
      this.ukur = parseFloat(this.ukur.toFixed(1));
      marker.setAnimation(google.maps.Animation.BOUNCE);
      this.idx = id.id;
      let infoWindow = new google.maps.InfoWindow({
        content:"<b>Distance:</b> " + this.ukur + " meter"
      });
      infoWindow.open(this.map, marker);
      this.lastOpenedInfoWindow = infoWindow;
      setTimeout(function() {
        this.cls = "aktif";
        marker.setAnimation(null);
        infoWindow.close();
      }, 2000);
      console.log(this.ukur);
      console.log(kePosisi);
    });
  }

  closeLastOpenedInfoWindow() {
    if (this.lastOpenedInfoWindow) {
      this.lastOpenedInfoWindow.close();
    }
  }

  // Removes the markers from the map, but keeps them in the array.
  clearOrigin() {
    this.kordinat;
    this.gmarker;
    this.waypts;
    for (var i = 0; i <= this.kordinat.length; ++i) {
      this.kordinat.pop();
      this.kordinat.pop();
      this.gmarker.pop();
      this.gmarker.pop();
      this.waypts.pop();
      this.waypts.pop();
    }
    this.getRoute();
  }

  // Remove Socket

  removeSocket() {
    this.socket.off("message");
    // delete this.send('message');
  }

  skip(id) {
    let data: Observable<any>;
    data = this.http.get(this.service.urlRoot+"mwa/delRouteMapUrut?id=" + id);
    data.subscribe(result => {
      this.upTask("3", this.idTask,'0',id);
    });
    this.clearOrigin();
  }

  toViewActivity(idTask, idRtd) {
    this.navCtrl.push(ViewActivityPage, {
      id: idTask,
      idRtd: idRtd
    });
  }

  toCheckin(idTask, idRtd, oId) {
    this.navCtrl.push(CheckinPage, {
      idRt: this.id,
      idTask: idTask,
      idRtd: idRtd
    });
    localStorage.setItem('idRtd',idRtd);
    localStorage.setItem('oId',oId);
    localStorage.setItem('idFromRoot',this.id);
  }

  toViewAll(oId){
    this.navCtrl.push('ViewAllPage');
    localStorage.setItem('oId',oId);
  }

  presentToast(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: "bottom"
    });
    toast.onDidDismiss(() => {
      console.log("Dismissed toast");
    });
    toast.present();
  }
}
