import { NgModule, ErrorHandler } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { IonicApp, IonicModule, IonicErrorHandler } from "ionic-angular";
import { MyApp } from "./app.component";
import { BonitaMWAPage } from "../pages/bonita-mwa/bonita-mwa";
import { LoginPageModule } from "../pages/login/login.module";
import { AddoutletPageModule } from "../pages/addoutlet/addoutlet.module";
import { ViewActivityPageModule } from "../pages/view-activity/view-activity.module";
import { CheckinPageModule } from "../pages/checkin/checkin.module";
import { ProfilePage } from "../pages/profile/profile";
import { TodoListPage } from "../pages/todo-list/todo-list";
import { CloudSharingPage } from "../pages/cloud-sharing/cloud-sharing";
import { OutletPage } from "../pages/outlet/outlet";
import { EditoutletPageModule } from "../pages/editoutlet/editoutlet.module";
import { UpdateOutletProfilePage } from "../pages/update-outlet-profile/update-outlet-profile";
import { NearByPage } from "../pages/near-by/near-by";
import { ChangePasswordPage } from "../pages/change-password/change-password";
import { Geolocation } from "@ionic-native/geolocation";
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from "@ionic-native/image-picker";
import { Base64 } from "@ionic-native/base64";
import { RoutePage } from "../pages/route/route";
import { HttpModule } from "@angular/http";
import { Network } from "@ionic-native/network";
import { Toast } from '@ionic-native/toast';
import { Diagnostic } from '@ionic-native/diagnostic';
import { FilePath } from "@ionic-native/file-path";
import { StatusBar } from "@ionic-native/status-bar";
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { SplashScreen } from "@ionic-native/splash-screen";
import { HttpClientModule } from "@angular/common/http";
import { RestProvider } from '../providers/rest/rest';
import { AuthServiceProvider } from '../providers/auth-service/auth-service';
import { FilesPageModule } from "../pages/files/files.module";
import { StockSurveyPageModule } from "../pages/stock-survey/stock-survey.module";
import { SalesOrderPageModule } from "../pages/sales-order/sales-order.module";
import { ViewAllPageModule } from "../pages/view-all/view-all.module";
import { OpenNativeSettings } from "@ionic-native/open-native-settings";
import { NearbyOutletPage } from "../pages/nearby-outlet/nearby-outlet";
import { NearbyOutletsPage } from "../pages/nearby-outlets/nearby-outlets";
import { ServiceProvider } from '../providers/service/service';

@NgModule({
  declarations: [
    MyApp,
    BonitaMWAPage,
    // LoginPage,
    // AddoutletPage,
    ProfilePage,
    // CheckinPage,
    // ViewActivityPage,
    NearbyOutletPage,
    NearbyOutletsPage,
    TodoListPage,
    CloudSharingPage,
    OutletPage,
    UpdateOutletProfilePage,
    NearByPage,
    ChangePasswordPage,
    RoutePage
    // FilesPage
  ],
  imports: [
    BrowserModule,
    EditoutletPageModule,
    AddoutletPageModule,
    CheckinPageModule,
    HttpClientModule,
    ViewActivityPageModule,
    StockSurveyPageModule,
    LoginPageModule,
    FilesPageModule,
    SalesOrderPageModule,
    ViewAllPageModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      scrollPadding: false,
      scrollAssist: false,
      autoFocusAssist: false
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BonitaMWAPage,
    // ViewActivityPage,
    // CheckinPage,
    // AddoutletPage,
    // LoginPage,
    ProfilePage,
    TodoListPage,
    CloudSharingPage,
    OutletPage,
    NearbyOutletPage,
    NearbyOutletsPage,
    UpdateOutletProfilePage,
    NearByPage,
    ChangePasswordPage,
    RoutePage
    // FilesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Diagnostic,
    PhotoViewer,
    Geolocation,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    FileTransfer,
    FileTransferObject,
    File,
    FilePath,
    Camera,
    OpenNativeSettings,
    ImagePicker,
    Base64,
    RestProvider,
    AuthServiceProvider,
    Network,
    Toast,
    ServiceProvider
  ]
})
export class AppModule {}
