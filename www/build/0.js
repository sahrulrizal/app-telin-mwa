webpackJsonp([0],{

/***/ 366:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewPenjualanPageModule", function() { return ViewPenjualanPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view_penjualan__ = __webpack_require__(367);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewPenjualanPageModule = /** @class */ (function () {
    function ViewPenjualanPageModule() {
    }
    ViewPenjualanPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__view_penjualan__["a" /* ViewPenjualanPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__view_penjualan__["a" /* ViewPenjualanPage */]),
            ],
        })
    ], ViewPenjualanPageModule);
    return ViewPenjualanPageModule;
}());

//# sourceMappingURL=view-penjualan.module.js.map

/***/ }),

/***/ 367:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewPenjualanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the StockSurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewPenjualanPage = /** @class */ (function () {
    function ViewPenjualanPage(navCtrl, service, loadingCtrl, http, navParams) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.navParams = navParams;
        this.url = "http://150.242.111.235/apibonita/index.php/mwa/";
        this.product = [];
        this.voucher = [];
        this.simcard = {};
        this.etopup = {};
        this.telkomcel = [0];
        this.telecom = [];
        this.telemor = [];
        this.vtelkomcel = [];
        this.vtelecom = [];
        this.vtelemor = [];
        this.no = 1;
        this.getIDJProduct(4);
        this.getIDJVoucher(2);
    }
    ViewPenjualanPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad StockSurveyPage');
    };
    ViewPenjualanPage.prototype.getIDJProduct = function (id) {
        var _this = this;
        this.product = [];
        this.http.get(this.service.urlRoot + 'mwa/getIDJProduct?id=' + id).subscribe(function (data) {
            var d = data;
            for (var i = 0; i < d.length; i++) {
                _this.telkomcel[d[i].id] = '0';
                _this.telecom[d[i].id] = '0';
                _this.telemor[d[i].id] = '0';
                _this.product.push({
                    id: d[i].id,
                    product_name: d[i].product_name,
                    value: '0'
                });
            }
        });
    };
    ViewPenjualanPage.prototype.getIDJVoucher = function (id) {
        var _this = this;
        this.voucher = [];
        this.http.get(this.service.urlRoot + 'mwa/getIDJProduct?id=' + id).subscribe(function (data) {
            var d = data;
            for (var i = 0; i < d.length; i++) {
                _this.vtelkomcel[d[i].id] = '0';
                _this.vtelecom[d[i].id] = '0';
                _this.vtelemor[d[i].id] = '0';
                _this.voucher.push({
                    id: d[i].id,
                    product_name: d[i].product_name,
                    value: '0'
                });
            }
        });
    };
    // TO
    ViewPenjualanPage.prototype.buildStockArray = function () {
        var bund = [];
        var vouc = [];
        var s = this.simcard;
        var e = this.etopup;
        var jml;
        var jml2;
        if (this.telkomcel.length != 0) {
            jml = this.telkomcel.length;
        }
        else if (this.telecom.length != 0) {
            jml = this.telecom.length;
        }
        else if (this.telemor.length != 0) {
            jml = this.telemor.length;
        }
        if (this.vtelkomcel.length != 0) {
            jml2 = this.vtelkomcel.length;
        }
        else if (this.vtelecom.length != 0) {
            jml2 = this.vtelecom.length;
        }
        else if (this.vtelemor.length != 0) {
            jml2 = this.vtelemor.length;
        }
        for (var i = 0; i < jml; ++i) {
            if (this.telkomcel[i] != undefined || this.telemor[i] != undefined || this.telecom[i] != undefined) {
                bund.push({
                    telkomcel: this.telkomcel[i],
                    telecom: this.telecom[i],
                    telemor: this.telemor[i],
                    idProduct: i,
                    idJp: 4
                });
            }
        }
        for (var i = 0; i < jml2; ++i) {
            if (this.vtelkomcel[i] != undefined || this.vtelemor[i] != undefined || this.vtelecom[i] != undefined) {
                vouc.push({
                    telkomcel: this.vtelkomcel[i],
                    telecom: this.vtelecom[i],
                    telemor: this.vtelemor[i],
                    idProduct: i,
                    idJp: 2
                });
            }
        }
        var arr = {
            simcard: [{
                    telkomcel: this.validValue(s.telkomcel),
                    telecom: this.validValue(s.telecom),
                    telemor: this.validValue(s.telemor),
                    idProduct: 9999,
                    idJp: 1
                }],
            etopup: [{
                    telkomcel: this.validValue(e.telkomcel),
                    telecom: this.validValue(e.telecom),
                    telemor: this.validValue(e.telemor),
                    idProduct: 6666,
                    idJp: 3
                }],
            voucher: vouc,
            bundling: bund
        };
        return arr;
    };
    ViewPenjualanPage.prototype.validValue = function (v) {
        if (v == undefined) {
            v = '';
        }
        return v;
    };
    ViewPenjualanPage.prototype.toSalesOrder = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Please Wait.."
        });
        loading.present();
        console.log(this.buildStockArray());
        var data = new FormData;
        data.append('stock', JSON.stringify(this.buildStockArray()));
        data.append('idCh', localStorage.getItem('idCh'));
        data.append('idTask', localStorage.getItem('idTask'));
        data.append('oId', localStorage.getItem('oId'));
        data.append('stId', localStorage.getItem('stId'));
        this.http.post(this.service.urlRoot + 'mwa/saveSurveiPenjualan', data).subscribe(function (d) {
            loading.dismiss();
            _this.navCtrl.push("SalesOrderPage");
        });
    };
    ViewPenjualanPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-view-penjualan',template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/view-penjualan/view-penjualan.html"*/'<!--\n  Generated template for the StockSurveyPage page.\n  \n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  \n    <ion-navbar>\n      <ion-title>Sales Survey</ion-title>\n    </ion-navbar>\n    \n  </ion-header>\n  \n  \n  <ion-content padding>\n    <ion-grid>\n      <ion-row>\n        <ion-col col-12>\n          <div><b>#Simcard</b></div>\n          <hr>\n        </ion-col>\n        <ion-col col-12 >\n          <table border="1" width="100%">\n              <tr>\n                  <td colspan="1"><b>Simcard</b></td>\n                </tr>  \n              <tr>\n                  <td colspan="3">Total Amount</td>\n                </tr>\n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td><ion-input type="number" value="0" (click)="simcard.telkomcel = \'\'" [(ngModel)]="simcard.telkomcel"></ion-input></td>\n              <td> <ion-input type="number" value="0" (click)="simcard.telecom = \'\'" [(ngModel)]="simcard.telecom"></ion-input></td>\n              <td><ion-input type="number" value="0" (click)="simcard.telemor = \'\'" [(ngModel)]="simcard.telemor"></ion-input></td>\n            </tr>\n          </table>\n        </ion-col>\n      </ion-row>\n      \n      <ion-row>\n        <ion-col col-12>\n          <div><b>#E-Topup</b></div>\n          <hr>\n        </ion-col>\n        <ion-col col-12 >\n          \n          <table border="1" width="100%">\n              <tr>\n                  <td colspan="1"><b>Etopup</b></td>\n                </tr>  \n            <tr>\n              <td colspan="3">Total Amount</td>\n            </tr>\n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td><ion-input type="number" value="0" (click)="etopup.telkomcel = \'\'" [(ngModel)]="etopup.telkomcel"></ion-input></td>\n              <td> <ion-input type="number" value="0" (click)="etopup.telecom = \'\'" [(ngModel)]="etopup.telecom"></ion-input></td>\n              <td><ion-input type="number" value="0" (click)="etopup.telemor = \'\'" [(ngModel)]="etopup.telemor"></ion-input></td>\n            </tr>\n          </table>\n          \n        </ion-col>\n      </ion-row>\n      \n      <ion-row>\n        <ion-col col-12>\n          <div><b>#Voucher</b></div>\n          <hr>\n        </ion-col>\n        <ion-col col-12 >\n          <div *ngFor="let i of voucher;">\n            \n            <ion-label stacked style="color:#000;font-weight:bold;" margin-bottom>{{i.product_name}}</ion-label>\n  \n            <table border="1" width="100%">\n                <tr>\n                    <td colspan="1"><b>{{i.product_name}}</b></td>\n                  </tr>  \n                <tr>\n                    <td colspan="3">Total Amount</td>\n                  </tr>\n              <tr>\n                <td>Telkomcel</td>\n                <td>Timor Telecom</td>\n                <td>Telemor</td>\n              </tr>\n              <tr>\n                <td><ion-input type="number"   (click)="vtelkomcel[i.id] = \'\'" [(ngModel)]="vtelkomcel[i.id]"></ion-input></td>\n                <td> <ion-input type="number" (click)="vtelecom[i.id] = \'\'" [(ngModel)]="vtelecom[i.id]"></ion-input></td>\n                <td><ion-input type="number" (click)="vtelemor[i.id] = \'\'" [(ngModel)]="vtelemor[i.id]"></ion-input></td>\n              </tr>\n            </table>\n            \n          </div>\n        </ion-col>\n      </ion-row>\n      \n      <ion-row>\n        <ion-col col-12>\n          <div><b>#Bundling</b></div>\n          <hr>\n        </ion-col>\n        <ion-col col-12 >\n            \n            <div margin-bottom *ngFor="let i of product;">\n          <ion-label stacked style="color:#000;font-weight:bold;" margin-bottom>{{i.product_name}}</ion-label>\n  \n            <table border="1" width="100%">\n              \n                <tr>\n                    <td colspan="1">{{i.product_name}}</td>\n                  </tr>\n                <tr>\n                    <td colspan="3">Total Amount</td>\n                  </tr>\n              <tr>\n                <td>Telkomcel</td>\n                <td>Timor Telecom</td>\n                <td>Telemor</td>\n              </tr>\n              <tr>\n                <td><ion-input type="number" (click)="telkomcel[i.id] = \'\'" [(ngModel)]="telkomcel[i.id]"></ion-input></td>\n                <td> <ion-input type="number" (click)="telecom[i.id] = \'\'" [(ngModel)]="telecom[i.id]"></ion-input></td>\n                <td><ion-input type="number" (click)="telemor[i.id] = \'\'" [(ngModel)]="telemor[i.id]"></ion-input></td>\n              </tr>\n            </table>\n          </div>\n  \n        </ion-col>\n      </ion-row>\n      \n      <button ion-button round outline block (click)="toSalesOrder()">Next</button>\n    </ion-grid>\n  </ion-content>\n  '/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/view-penjualan/view-penjualan.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], ViewPenjualanPage);
    return ViewPenjualanPage;
}());

//# sourceMappingURL=view-penjualan.js.map

/***/ })

});
//# sourceMappingURL=0.js.map