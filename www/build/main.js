webpackJsonp([1],{

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the ServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var ServiceProvider = /** @class */ (function () {
    // public urlRoot:string = "http://150.242.111.235/apibonita/index.php/";
    // public url:string = "http://150.242.111.235/apibonita/";
    // public urlRoot:string = "https://bonita.telkomcel.tl/apibonita/index.php/";
    // public url:string = "https://bonita.telkomcel.tl/apibonita/";
    function ServiceProvider(http) {
        this.http = http;
        this.urlRoot = "http://localhost/apitelin-mwa/index.php/";
        this.url = "http://localhost/apitelin-mwa/";
        console.log('Hello ServiceProvider Provider');
    }
    ServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]) === "function" && _a || Object])
    ], ServiceProvider);
    return ServiceProvider;
    var _a;
}());

//# sourceMappingURL=service.js.map

/***/ }),

/***/ 100:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__bonita_mwa_bonita_mwa__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams, http, geolocation, loadingCtrl, alertCtrl, menu, toastCtrl, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.geolocation = geolocation;
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.menu = menu;
        this.toastCtrl = toastCtrl;
        this.service = service;
        if (localStorage.getItem("id") != null) {
            this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__bonita_mwa_bonita_mwa__["a" /* BonitaMWAPage */]);
        }
        console.log("ini adalah : " + localStorage.getItem("id"));
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log("ionViewDidLoad LoginPage");
        this.menu.swipeEnable(false);
    };
    LoginPage.prototype.alertGagal = function (u, p) {
        if (u == "") {
            var alert_1 = this.alertCtrl.create({
                title: "Cannot Login",
                subTitle: "Please enter your username!",
                buttons: ["Okay"]
            });
            alert_1.present();
        }
        else if (p == "") {
            var alert_2 = this.alertCtrl.create({
                title: "Cannot Login",
                subTitle: "Please enter your password!",
                buttons: ["Okay"]
            });
            alert_2.present();
        }
        else {
            var alert_3 = this.alertCtrl.create({
                title: "Cannot Login",
                subTitle: "The username or password you entered is incorrect. Please try again!",
                buttons: ["Try Again"]
            });
            alert_3.present();
        }
    };
    LoginPage.prototype.login = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Logging in..."
        });
        loading.present();
        var data;
        data = this.http.get(this.service.urlRoot + "auth/loginMwa?username=" + this.uname.value + "&password=" + this.password.value);
        data.subscribe(function (result) {
            _this.items = result;
            if (_this.items["success"] == false) {
                _this.alertGagal(_this.uname.value, _this.password.value);
                loading.dismiss();
            }
            else {
                localStorage.setItem("id", _this.items[0].id);
                localStorage.setItem("idCh", _this.items[0].idch);
                localStorage.setItem("chID", _this.items[0].chID);
                localStorage.setItem("stId", _this.items[0].stId);
                localStorage.setItem("cl", _this.items[0].cluster);
                _this.geolocation.getCurrentPosition().then(function (resp) {
                    _this.updateLocationSales(resp.coords.latitude, resp.coords.longitude);
                }).catch(function (error) {
                    console.log('Error getting location', error);
                });
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_3__bonita_mwa_bonita_mwa__["a" /* BonitaMWAPage */]);
                loading.dismiss();
                console.log(_this.items);
            }
        });
    };
    LoginPage.prototype.updateLocationSales = function (lat, lng) {
        var body = new FormData();
        body.append("active", '1');
        body.append("lat", lat);
        body.append("lng", lng);
        body.append("users_id", JSON.parse(localStorage.getItem("id")));
        this.http
            .post(this.service.urlRoot + "mwa/upLocationSales", body)
            .subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log(err);
        });
    };
    LoginPage.prototype.forgetPassword = function () {
        var _this = this;
        var prompt = this.alertCtrl.create({
            title: "Forget Password",
            subTitle: "Enter your username:",
            inputs: [{
                    name: "Username",
                    placeholder: "Username"
                }],
            buttons: [{
                    text: "Cancel",
                    handler: function (data) {
                        console.log("Cancel Clicked");
                    }
                },
                {
                    text: "Confirm",
                    handler: function (data) {
                        var body = new FormData();
                        body.append("username", data.Username);
                        _this.http
                            .post(_this.service.urlRoot + "mwa/forgetPassword", body)
                            .subscribe(function (res) {
                            if (res !== null) {
                                console.log(res);
                                _this.toast("We have sent message to your phone number that registered to your account.");
                            }
                            else {
                                _this.toast("Username is not registered.");
                            }
                        });
                    }
                }
            ]
        });
        prompt.present();
    };
    LoginPage.prototype.toast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: "top"
        });
        toast.onDidDismiss(function () {
            console.log("Dismissed toast");
        });
        toast.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("username"),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "uname", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("password"),
        __metadata("design:type", Object)
    ], LoginPage.prototype, "password", void 0);
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-login",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/login/login.html"*/'<!-- -->\n<ion-content padding class="animated fadeIn login auth-page">\n  <div class="login-content">\n\n    <!-- Logo -->\n    <div padding-horizontal text-center class="animated fadeInDown">\n      <div class="logo" style="width: auto;"></div>\n      <h2 ion-text class="text-white logo-text">\n        Smart\n        <br>Sales Support System\n      </h2>\n    </div>\n\n    <form class="login-form">\n      <ion-item>\n        <ion-label floating>\n          <ion-icon name="person" item-start class="text-dark"></ion-icon>\n          Username\n        </ion-label>\n        <ion-input type="text" #username></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-label floating>\n          <ion-icon name="lock" item-start class="text-dark"></ion-icon>\n          Password\n        </ion-label>\n        <ion-input type="password" #password></ion-input>\n      </ion-item>\n    </form>\n    <button id="btn-Login" ion-button color="danger" block (click)="login()">\n      Log in\n    </button>\n    <button id="btn-ForgetPassword" ion-button clear color="light" (click)="forgetPassword()" block href-inappbrowser="/signup">\n      Forget Password\n    </button>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/login/login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_service_service__["a" /* ServiceProvider */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 104:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ProfilePage = /** @class */ (function () {
    function ProfilePage(navCtrl, restProvider, http, service) {
        this.navCtrl = navCtrl;
        this.restProvider = restProvider;
        this.http = http;
        this.service = service;
        this.getUsers();
    }
    ProfilePage.prototype.getUsers = function () {
        var _this = this;
        var i = JSON.parse(localStorage.getItem('id'));
        var data;
        data = this.http.get(this.service.urlRoot + 'profile/getProfile/' + i);
        data.subscribe(function (result) {
            _this.users = result;
            console.log(_this.users);
        });
    };
    ProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-profile",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/profile/profile.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>\n      Profile\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page3" *ngFor="let user of users">\n  <img class="center" src="assets/img/users.png" style="display:block; margin-top: 60px" />\n  <ion-item class="item-profile">\n    <h5 class="center">{{user.name}}</h5>\n    <p class="center">{{user.username}}</p>\n  </ion-item>\n  <ion-card>\n    <ion-item id="profile-input3">\n      <h4>Gender</h4>\n      <p *ngIf="user.gender == 1;else f;">Male</p>\n      <ng-template #f>Female</ng-template>\n    </ion-item>\n    <ion-item id="profile-input4">\n      <h4>Email</h4>\n      <p>{{user.email}}</p>\n    </ion-item>\n    <ion-item id="profile-input5">\n      <h4>Birth Place</h4>\n      <p>{{user.birth_place}}</p>\n    </ion-item>\n    <ion-item id="profile-input6">\n      <h4>Date of Birth</h4>\n      <p>{{user.date_of_birth}}</p>\n    </ion-item>\n    <ion-item id="profile-input7">\n      <h4>Identity No.</h4>\n      <p>{{user.identity_number}}</p>\n    </ion-item>\n    <ion-item id="profile-input8">\n      <h4>Contact No.</h4>\n      <p>{{user.phone_number}}</p>\n    </ion-item>\n    <ion-item id="profile-input9">\n      <h4>NPWP</h4>\n      <p>{{user.npwp_number}}</p>\n    </ion-item>\n  </ion-card>\n  \n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/profile/profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */]])
    ], ProfilePage);
    return ProfilePage;
}());

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RestProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var RestProvider = /** @class */ (function () {
    function RestProvider(http) {
        this.http = http;
        this.apiUrl = "http://150.242.111.44/apibonita/index.php/profile/getProfile/";
        this.mwaUrl = "http://150.242.111.44/apibonita/index.php/mwa/getOutlets";
        console.log("RestProvider");
    }
    RestProvider.prototype.getUsers = function (id) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.apiUrl + id).subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider.prototype.getOutlet = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.http.get(_this.mwaUrl).subscribe(function (data) {
                resolve(data);
            }, function (err) {
                console.log(err);
            });
        });
    };
    RestProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], RestProvider);
    return RestProvider;
}());

//# sourceMappingURL=rest.js.map

/***/ }),

/***/ 106:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoutePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_open_native_settings__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__view_activity_view_activity__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__checkin_checkin__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__todo_list_todo_list__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_diagnostic__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_socket_io_client__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var RoutePage = /** @class */ (function () {
    function RoutePage(navCtrl, diagnostic, geolocation, navParams, toastCtrl, http, service, setting) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.diagnostic = diagnostic;
        this.geolocation = geolocation;
        this.navParams = navParams;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.service = service;
        this.setting = setting;
        this.ids = [];
        this.gmarker = [];
        this.waypts = [];
        this.kordinat = [];
        this.locawal = { lat: 123, lng: 123 };
        this.id = this.navParams.get("id");
        this.idTask = this.navParams.get("idTask");
        var successCallback = function (isAvailable) {
            if (isAvailable == true) {
            }
            else {
                _this.setting.open("location");
            }
        };
        var errorCallback = function (e) {
            alert("Error GPS Not Found: " + e);
        };
        this.diagnostic
            .isGpsLocationAvailable()
            .then(successCallback)
            .catch(errorCallback);
        this.socket = __WEBPACK_IMPORTED_MODULE_9_socket_io_client__("http://150.242.111.235:3000");
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
            _this.lat = data.coords.latitude;
            _this.lng = data.coords.longitude;
            _this.send(_this.id);
            _this.ukurReal;
        });
    }
    RoutePage.prototype.send = function (msg) {
        this.uuid = this.socket.emit("message", { id: this.socket.id, msg: msg });
    };
    RoutePage.prototype.rad = function (x) {
        return (x * Math.PI) / 180;
    };
    RoutePage.prototype.getDistance = function (p1, p2) {
        var R = 6378137;
        var dLat = this.rad(p2.lat - p1.lat);
        var dLong = this.rad(p2.lng - p1.lng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(p1.lat)) *
                Math.cos(this.rad(p2.lat)) *
                Math.sin(dLong / 2) *
                Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    };
    RoutePage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.getRoute();
        this.navBar.backButtonClick = function (e) {
            // todo something
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__todo_list_todo_list__["a" /* TodoListPage */]);
        };
        localStorage.setItem('idTask', this.idTask);
    };
    RoutePage.prototype.getRoute = function () {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getRouteMapUrut?id=" + this.id + '&idTask=' + this.idTask);
        data.subscribe(function (result) {
            _this.items = result;
            if (_this.items["success"] == false) {
                _this.ceks = 0;
                console.log(_this.ceks);
                _this.socket.off("message");
                _this.socket.disconnect("message");
            }
            else {
                _this.ceks = 1;
                _this.loadMap();
                console.log(_this.items);
            }
        });
    };
    RoutePage.prototype.upTask = function (s, idTask, s2, id) {
        if (s2 === void 0) { s2 = ''; }
        if (id === void 0) { id = ''; }
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/upStatusTask/" + s + "/" + idTask + '/' + s2 + '/' + id);
        data.subscribe(function (result) {
            console.log(result);
        });
    };
    RoutePage.prototype.getRoute2 = function (a, b) {
        var _this = this;
        if (a === void 0) { a = ""; }
        if (b === void 0) { b = ""; }
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getRouteGMapUrut?id=" + this.id + '&idTask=' + this.idTask);
        data.subscribe(function (result) {
            for (var i = 0; i < result.length; ++i) {
                _this.kordinat.push({
                    lat: +result[i].lat,
                    lng: +result[i].lng
                });
                _this.ids.push({
                    id: +result[i].id
                });
                _this.objlatawal = { lat: _this.lat, lng: _this.lng };
                _this.objnext = { lat: result[i].lat, lng: result[i].lng };
                _this.ukurReal = _this.getDistance(_this.objlatawal, _this.objnext);
            }
            _this.kordinat.push({
                lat: _this.lat,
                lng: _this.lng,
                img: 1
            });
            console.log(_this.kordinat);
        });
    };
    RoutePage.prototype.loadMap = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (position) {
            var latLngUtama = new google.maps.LatLng(_this.lat, _this.lng);
            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            _this.getRoute2();
            setTimeout(function () {
                var mapOptions = {
                    center: latLng,
                    zoom: 18,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                var map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
                latLng = new google.maps.LatLng(_this.lat, _this.lng);
                mapOptions = {
                    center: latLng,
                    zoom: 18,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };
                for (var i_1 = 0; i_1 < _this.kordinat.length; i_1++) {
                    var obj = {};
                    obj["location"] = new google.maps.LatLng(parseFloat(_this.kordinat[i_1].lat), parseFloat(_this.kordinat[i_1].lng));
                    obj["stopover"] = true;
                    _this.waypts.push(obj);
                }
                console.log(_this.waypts);
                map = new google.maps.Map(_this.mapElement.nativeElement, mapOptions);
                var directionsService = new google.maps.DirectionsService();
                var directionsDisplay = new google.maps.DirectionsRenderer({
                    suppressInfoWindows: true,
                    suppressMarkers: true
                });
                var locend = _this.kordinat[_this.kordinat.length - 1];
                console.log(locend);
                directionsDisplay.setMap(map);
                directionsService.route({
                    origin: new google.maps.LatLng(_this.lat, _this.lng),
                    destination: new google.maps.LatLng(locend.lat, locend.lng),
                    waypoints: _this.waypts,
                    optimizeWaypoints: true,
                    travelMode: "DRIVING"
                }, function (response, status) {
                    if (status === "OK") {
                        directionsDisplay.setDirections(response);
                    }
                });
                var marker;
                for (var i = 0; i < _this.kordinat.length; i++) {
                    if (_this.kordinat[i].img == 1) {
                        marker = new google.maps.Marker({
                            position: _this.kordinat[i],
                            icon: "./assets/img/home.png",
                            map: map,
                            title: "Home"
                        });
                    }
                    else {
                        marker = new google.maps.Marker({
                            position: _this.kordinat[i],
                            map: map,
                            // icon : './assets/img/flag.png',
                            // labelContent: "ABCD",
                            title: "Order to " + i
                        });
                    }
                    _this.gmarker.push(marker);
                    var ok = _this.ids[i];
                    _this.addInfoWindow(marker, ok);
                }
                // debugger;
                directionsDisplay.setMap(map);
                // var jalur = new google.maps.Polyline({
                //   path: this.kordinat,
                //   geodesic: true,
                //   strokeColor: '#FF0000',
                //   strokeOpacity: 1.0,
                //   strokeWeight: 2
                // });
                var lokasiHp = new google.maps.Marker({
                    position: latLng,
                    map: map,
                    icon: "./assets/img/run.png",
                    title: "Me "
                });
                _this.socket.on("message", function (msg) {
                    console.log(msg);
                    lokasiHp.setPosition({ lat: _this.lat, lng: _this.lng });
                    _this.ukur;
                    _this.ukurReal;
                });
                // jalur.setMap(map);
            }, 500);
            // console.log(position.coords.latitude+','+position.coords.longitude);
        }, function (err) {
            console.log(err);
        });
    };
    RoutePage.prototype.addInfoWindow = function (marker, id) {
        var _this = this;
        google.maps.event.addListener(marker, "click", function () {
            _this.closeLastOpenedInfoWindow();
            var posisiAwal = { lat: _this.lat, lng: _this.lng };
            var kePosisi = {
                lat: marker.getPosition().lat(),
                lng: marker.getPosition().lng()
            };
            _this.ukur = _this.getDistance(posisiAwal, kePosisi);
            _this.ukur = parseFloat(_this.ukur.toFixed(1));
            marker.setAnimation(google.maps.Animation.BOUNCE);
            _this.idx = id.id;
            var infoWindow = new google.maps.InfoWindow({
                content: "<b>Distance:</b> " + _this.ukur + " meter"
            });
            infoWindow.open(_this.map, marker);
            _this.lastOpenedInfoWindow = infoWindow;
            setTimeout(function () {
                this.cls = "aktif";
                marker.setAnimation(null);
                infoWindow.close();
            }, 2000);
            console.log(_this.ukur);
            console.log(kePosisi);
        });
    };
    RoutePage.prototype.closeLastOpenedInfoWindow = function () {
        if (this.lastOpenedInfoWindow) {
            this.lastOpenedInfoWindow.close();
        }
    };
    // Removes the markers from the map, but keeps them in the array.
    RoutePage.prototype.clearOrigin = function () {
        this.kordinat;
        this.gmarker;
        this.waypts;
        for (var i = 0; i <= this.kordinat.length; ++i) {
            this.kordinat.pop();
            this.kordinat.pop();
            this.gmarker.pop();
            this.gmarker.pop();
            this.waypts.pop();
            this.waypts.pop();
        }
        this.getRoute();
    };
    // Remove Socket
    RoutePage.prototype.removeSocket = function () {
        this.socket.off("message");
        // delete this.send('message');
    };
    RoutePage.prototype.skip = function (id) {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/delRouteMapUrut?id=" + id);
        data.subscribe(function (result) {
            _this.upTask("3", _this.idTask, '0', id);
        });
        this.clearOrigin();
    };
    RoutePage.prototype.toViewActivity = function (idTask, idRtd) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__view_activity_view_activity__["a" /* ViewActivityPage */], {
            id: idTask,
            idRtd: idRtd
        });
    };
    RoutePage.prototype.toCheckin = function (idTask, idRtd, oId) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__checkin_checkin__["a" /* CheckinPage */], {
            idRt: this.id,
            idTask: idTask,
            idRtd: idRtd
        });
        localStorage.setItem('idRtd', idRtd);
        localStorage.setItem('oId', oId);
        localStorage.setItem('idFromRoot', this.id);
    };
    RoutePage.prototype.toViewAll = function (oId) {
        this.navCtrl.push('ViewAllPage');
        localStorage.setItem('oId', oId);
    };
    RoutePage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: "bottom"
        });
        toast.onDidDismiss(function () {
            console.log("Dismissed toast");
        });
        toast.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("map"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], RoutePage.prototype, "mapElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Navbar */])
    ], RoutePage.prototype, "navBar", void 0);
    RoutePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-route",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/route/route.html"*/'<ion-header>\n    <ion-navbar color="danger" (click)="removeSocket()">\n        <ion-title>\n            Route\n        </ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content >\n    <div #map id="map"></div>\n    <ion-item color="none" id="todo-list-item3" *ngFor="let item of items">\n        <span [ngClass]="{\'aktif\' : item.id == idx}">{{item.outletname}}</span>\n        <br>\n        <span *ngIf="item.checkin == 0" >\n            <button *ngIf="ukur <= 15 && idx == item.id;else close;" ion-button (click)="toCheckin(idTask,item.id,item.oId)">Check In</button>\n        </span>\n            <ng-template #close>\n                <button *ngIf="item.checkin == 0 && item.status == 1" disabled ion-button color="light" (click)="toCheckin(idTask,item.id,item.oId)">Check In</button>\n            </ng-template>\n\n        <!-- <button ion-button color="light" (click)="toViewActivity(idTask,item.id)" *ngIf="item.status != 0" >View Activity List </button> -->\n        <button ion-button color="light" *ngIf="item.checkin == 1" (click)="toViewAll(item.oId)">All Activity</button>\n        <button ion-button color="light" (click)="skip(item.id)" *ngIf="item.checkin == 0" >Skip</button>\n    </ion-item>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/route/route.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_10__providers_service_service__["a" /* ServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_open_native_settings__["a" /* OpenNativeSettings */]])
    ], RoutePage);
    return RoutePage;
}());

//# sourceMappingURL=route.js.map

/***/ }),

/***/ 139:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 139;

/***/ }),

/***/ 180:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/addoutlet/addoutlet.module": [
		210
	],
	"../pages/checkin/checkin.module": [
		181
	],
	"../pages/editoutlet/editoutlet.module": [
		207
	],
	"../pages/files/files.module": [
		184
	],
	"../pages/login/login.module": [
		187
	],
	"../pages/sales-order/sales-order.module": [
		203
	],
	"../pages/stock-survey/stock-survey.module": [
		204
	],
	"../pages/view-activity/view-activity.module": [
		205
	],
	"../pages/view-all/view-all.module": [
		206
	],
	"../pages/view-penjualan/view-penjualan.module": [
		366,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 180;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 181:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CheckinPageModule", function() { return CheckinPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__checkin__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var CheckinPageModule = /** @class */ (function () {
    function CheckinPageModule() {
    }
    CheckinPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__checkin__["a" /* CheckinPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__checkin__["a" /* CheckinPage */]),
            ],
        })
    ], CheckinPageModule);
    return CheckinPageModule;
}());

//# sourceMappingURL=checkin.module.js.map

/***/ }),

/***/ 182:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckinPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__view_activity_view_activity__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the CheckinPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var CheckinPage = /** @class */ (function () {
    function CheckinPage(service, navCtrl, loading, navParams, http, alertCtrl) {
        this.service = service;
        this.navCtrl = navCtrl;
        this.loading = loading;
        this.navParams = navParams;
        this.http = http;
        this.alertCtrl = alertCtrl;
        this.aksi = [];
        this.point = [];
        this.task_id = [];
        this.survey_id = [];
        this.idRt = this.navParams.get('idRt');
        this.id = this.navParams.get('idTask');
        this.idRtd = this.navParams.get('idRtd');
    }
    CheckinPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad CheckinPage');
        this.getCheckin();
    };
    CheckinPage.prototype.getCheckin = function () {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + 'mwa/getCheckin?idTask=' + this.id + '&idRtd=' + this.idRtd);
        data.subscribe(function (result) {
            _this.n = 0;
            _this.d = result;
            console.log(_this.d);
        });
    };
    CheckinPage.prototype.save = function () {
        var _this = this;
        var d = [];
        for (var i = 0; i < this.point.length; ++i) {
            if (this.point[i] != undefined || this.point[i] != null) {
                d.push({
                    answer: this.point[i],
                    task_id: this.task_id[i],
                    survey_id: this.survey_id[i],
                    outlet_id: localStorage.getItem('oId'),
                    idRtd: this.idRtd,
                });
            }
        }
        console.log(d);
        var loading = this.loading.create({
            content: "Please Wait.."
        });
        for (var i = 0; i < d.length; ++i) {
            this.http.post(this.service.urlRoot + "mwa/saveCheckin", JSON.stringify(d[i]))
                .subscribe(function (res) {
                _this.aksi.push({ i: res });
                console.log(res);
            }, function (err) {
                _this.aksi.push({ i: err });
                console.log(err);
            });
        }
        ;
        console.log(this.aksi.length);
        if (this.aksi) {
            loading.dismiss();
            this.toSurveyStock();
        }
    };
    /* SURVEY STOK */
    CheckinPage.prototype.toSurveyStock = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__view_activity_view_activity__["a" /* ViewActivityPage */], {
            id: localStorage.getItem('idTask'),
            idRtd: localStorage.getItem('idRtd')
        });
    };
    CheckinPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-checkin',template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/checkin/checkin.html"*/'<!--\n  Generated template for the CheckinPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="danger">\n    <ion-title>checkin</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n<ion-card *ngFor="let item of d" >\n	\n	<div *ngIf="(item.survey_type == 1);else costum;">\n	  <ion-card-header style="background: #f92020;\n	    color: #FFF;\n	    font-weight: bold;white-space: normal;">\n	 	 {{item.survey_name}}\n	  </ion-card-header>\n\n	  	<input type="hidden"  value="item.idTask" [(ngModel)]="task_id[item.id]=item.idTask">	\n	  	<input type="hidden"  value="item.id" [(ngModel)]="survey_id[item.id]=item.id">	\n	  <!-- PG -->\n	  <ion-list radio-group [(ngModel)]="point[item.id]">\n	  	<ion-item>\n	  		<ion-label>A. {{item.survey_opt_a}}</ion-label>\n	  		<ion-radio value="survey_opt_a"></ion-radio>\n	  	</ion-item>\n	  	<ion-item>\n	  		<ion-label>B. {{item.survey_opt_b}}</ion-label>\n	  		<ion-radio value="survey_opt_b"></ion-radio>\n	  	</ion-item>\n	  	<ion-item>\n	  		<ion-label>C. {{item.survey_opt_c}}</ion-label>\n	  		<ion-radio value="survey_opt_c"></ion-radio>\n	  	</ion-item>\n	  	<ion-item>\n	  		<ion-label>D. {{item.survey_opt_d}}</ion-label>\n	  		<ion-radio value="survey_opt_d"></ion-radio>\n	  	</ion-item>\n	  	<ion-item>\n	  		<ion-label>E. {{item.survey_opt_e}}</ion-label>\n	  		<ion-radio value="survey_opt_e"></ion-radio>\n	  	</ion-item>\n	  </ion-list>\n	</div>\n\n	<!-- Costume -->\n	<ng-template #costum>\n		<input type="hidden"  value="item.idTask" [(ngModel)]="task_id[item.id]=item.idTask">	\n	  	<input type="hidden"  value="item.id" [(ngModel)]="survey_id[item.id]=item.id">	\n	  	\n		<ion-card-header style="background: #f92020;\n			color: #FFF;\n			font-weight: bold;white-space: normal;">\n		 {{item.survey_name}}\n		</ion-card-header>\n\n		<ion-list radio-group [(ngModel)]="point[item.id]">\n			<ion-item>\n				<ion-textarea placeholder="Enter a description"></ion-textarea>\n			</ion-item>\n		</ion-list>\n	</ng-template>\n</ion-card>\n\n	<div padding>\n	<button ion-button round outline block (click)="save()">Next</button>\n	</div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/checkin/checkin.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], CheckinPage);
    return CheckinPage;
}());

//# sourceMappingURL=checkin.js.map

/***/ }),

/***/ 183:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StockSurveyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the StockSurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var StockSurveyPage = /** @class */ (function () {
    function StockSurveyPage(navCtrl, service, loadingCtrl, http, navParams) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.loadingCtrl = loadingCtrl;
        this.http = http;
        this.navParams = navParams;
        this.url = "http://150.242.111.235/apibonita/index.php/mwa/";
        this.simcard = {};
        this.etopup = {};
        this.telkomcel = [];
        this.telecom = [];
        this.telemor = [];
        this.vtelkomcel = [];
        this.vtelecom = [];
        this.vtelemor = [];
        this.no = 1;
        this.getIDJProduct(4);
        this.getIDJVoucher(2);
    }
    StockSurveyPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad StockSurveyPage');
    };
    StockSurveyPage.prototype.getIDJProduct = function (id) {
        var _this = this;
        this.product = [];
        this.http.get(this.service.urlRoot + 'mwa/getIDJProduct?id=' + id).subscribe(function (data) {
            var d = data;
            for (var i = 0; i < d.length; i++) {
                _this.telkomcel[d[i].id] = '0';
                _this.telecom[d[i].id] = '0';
                _this.telemor[d[i].id] = '0';
                _this.product.push({
                    id: d[i].id,
                    product_name: d[i].product_name,
                    value: '0'
                });
            }
        });
    };
    StockSurveyPage.prototype.getIDJVoucher = function (id) {
        var _this = this;
        this.voucher = [];
        this.http.get(this.service.urlRoot + 'mwa/getIDJProduct?id=' + id).subscribe(function (data) {
            var d = data;
            for (var i = 0; i < d.length; i++) {
                _this.vtelkomcel[d[i].id] = '0';
                _this.vtelecom[d[i].id] = '0';
                _this.vtelemor[d[i].id] = '0';
                _this.voucher.push({
                    id: d[i].id,
                    product_name: d[i].product_name,
                    value: '0'
                });
            }
        });
    };
    // TO
    StockSurveyPage.prototype.buildStockArray = function () {
        var bund = [];
        var vouc = [];
        var s = this.simcard;
        var e = this.etopup;
        var jml;
        var jml2;
        if (this.telkomcel.length != 0) {
            jml = this.telkomcel.length;
        }
        else if (this.telecom.length != 0) {
            jml = this.telecom.length;
        }
        else if (this.telemor.length != 0) {
            jml = this.telemor.length;
        }
        if (this.vtelkomcel.length != 0) {
            jml2 = this.vtelkomcel.length;
        }
        else if (this.vtelecom.length != 0) {
            jml2 = this.vtelecom.length;
        }
        else if (this.vtelemor.length != 0) {
            jml2 = this.vtelemor.length;
        }
        for (var i = 0; i < jml; ++i) {
            if (this.telkomcel[i] != undefined || this.telemor[i] != undefined || this.telecom[i] != undefined) {
                bund.push({
                    telkomcel: this.telkomcel[i],
                    telecom: this.telecom[i],
                    telemor: this.telemor[i],
                    idProduct: i,
                    idJp: 4
                });
            }
        }
        for (var i = 0; i < jml2; ++i) {
            if (this.vtelkomcel[i] != undefined || this.vtelemor[i] != undefined || this.vtelecom[i] != undefined) {
                vouc.push({
                    telkomcel: this.vtelkomcel[i],
                    telecom: this.vtelecom[i],
                    telemor: this.vtelemor[i],
                    idProduct: i,
                    idJp: 2
                });
            }
        }
        var arr = {
            simcard: [{
                    telkomcel: this.validValue(s.telkomcel),
                    telecom: this.validValue(s.telecom),
                    telemor: this.validValue(s.telemor),
                    idProduct: 9999,
                    idJp: 1
                }],
            etopup: [{
                    telkomcel: this.validValue(e.telkomcel),
                    telecom: this.validValue(e.telecom),
                    telemor: this.validValue(e.telemor),
                    idProduct: 6666,
                    idJp: 3
                }],
            voucher: vouc,
            bundling: bund
        };
        return arr;
    };
    StockSurveyPage.prototype.validValue = function (v) {
        if (v == undefined) {
            v = '';
        }
        return v;
    };
    StockSurveyPage.prototype.toSalesOrder = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Please Wait.."
        });
        loading.present();
        var data = new FormData;
        data.append('stock', JSON.stringify(this.buildStockArray()));
        data.append('idCh', localStorage.getItem('idCh'));
        data.append('idTask', localStorage.getItem('idTask'));
        data.append('oId', localStorage.getItem('oId'));
        data.append('stId', localStorage.getItem('stId'));
        this.http.post(this.service.urlRoot + 'mwa/saveStockSurvey', data).subscribe(function (d) {
            loading.dismiss();
            _this.navCtrl.push("ViewPenjualanPage");
        });
    };
    StockSurveyPage.prototype.resetInput = function (event) {
        console.log(event);
    };
    StockSurveyPage.prototype.changeValue = function (v) {
        if (v != '') {
            return v = '';
        }
        else {
            return v = '0';
        }
    };
    StockSurveyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-stock-survey',template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/stock-survey/stock-survey.html"*/'<!--\n  Generated template for the StockSurveyPage page.\n  \n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  \n  <ion-navbar>\n    <ion-title>Stock Survey</ion-title>\n  </ion-navbar>\n  \n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    <ion-row>\n      <ion-col col-12>\n        <div><b>#Simcard</b></div>\n        <hr>\n      </ion-col>\n      <ion-col col-12 >\n        <table border="1" width="100%">\n            <tr>\n                <td colspan="3"><b>Simcard</b></td>\n              </tr> \n          <tr>\n            <td>Telkomcel</td>\n            <td>Timor Telecom</td>\n            <td>Telemor</td>\n          </tr>\n          <tr>\n            <td><ion-input type="number" value="0" (click)="simcard.telkomcel= \'\'" [(ngModel)]="simcard.telkomcel"></ion-input></td>\n            <td> <ion-input type="number" value="0" (click)="simcard.telecom= \'\'" [(ngModel)]="simcard.telecom"></ion-input></td>\n            <td><ion-input type="number" value="0" (click)="simcard.telemor= \'\'" [(ngModel)]="simcard.telemor"></ion-input></td>\n          </tr>\n        </table>\n      </ion-col>\n    </ion-row>\n    \n    <ion-row>\n      <ion-col col-12>\n        <div><b>#E-Topup</b></div>\n        <hr>\n      </ion-col>\n      <ion-col col-12 >\n        \n        <table border="1" width="100%">\n            <tr>\n                <td colspan="3"><b>E-topup</b></td>\n              </tr> \n          <tr>\n            <td>Telkomcel</td>\n            <td>Timor Telecom</td>\n            <td>Telemor</td>\n          </tr>\n          <tr>\n            <td><ion-input type="number" value="0"  (click)="etopup.telkomcel= \'\'" [(ngModel)]="etopup.telkomcel"></ion-input></td>\n            <td> <ion-input type="number" value="0" (click)="etopup.telecom= \'\'" [(ngModel)]="etopup.telecom"></ion-input></td>\n            <td><ion-input type="number" value="0"  (click)="etopup.telemor= \'\'" [(ngModel)]="etopup.telemor"></ion-input></td>\n          </tr>\n        </table>\n        \n      </ion-col>\n    </ion-row>\n    \n    <ion-row>\n      <ion-col col-12>\n        <div><b>#Voucher</b></div>\n        <hr>\n      </ion-col>\n      <ion-col col-12 >\n        <div *ngFor="let i of voucher;" margin-bottom>\n          \n          <table border="1" width="100%">\n              <tr>\n                  <td colspan="3"><b>{{i.product_name}}</b></td>\n                </tr>  \n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td><ion-input type="number"  (click)="vtelkomcel[i.id]= \'\'" [(ngModel)]="vtelkomcel[i.id]"></ion-input></td>\n              <td> <ion-input type="number" (click)="vtelecom[i.id]= \'\'" [(ngModel)]="vtelecom[i.id]"></ion-input></td>\n              <td><ion-input type="number" (click)="vtelemor[i.id]= \'\'" [(ngModel)]="vtelemor[i.id]"></ion-input></td>\n            </tr>\n          </table>\n          \n        </div>\n      </ion-col>\n    </ion-row>\n    \n    <ion-row>\n      <ion-col col-12>\n        <div><b>#Bundling</b></div>\n        <hr>\n      </ion-col>\n      <ion-col col-12 >\n          \n          <div margin-bottom *ngFor="let i of product;">\n          <table border="1" width="100%">\n              <tr>\n                  <td colspan="3"><b>{{i.product_name}}</b></td>\n                </tr>  \n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td><ion-input type="number"  (click)="telkomcel[i.id] = \'\' " [(ngModel)]="telkomcel[i.id]"></ion-input></td>\n              <td> <ion-input type="number" (click)="telecom[i.id] = \'\' " [(ngModel)]="telecom[i.id]"></ion-input></td>\n              <td><ion-input type="number" (click)="telemor[i.id] = \'\' " [(ngModel)]="telemor[i.id]"></ion-input></td>\n            </tr>\n          </table>\n        </div>\n\n      </ion-col>\n    </ion-row>\n    \n    <button ion-button round outline block (click)="toSalesOrder()">Next</button>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/stock-survey/stock-survey.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */]])
    ], StockSurveyPage);
    return StockSurveyPage;
}());

//# sourceMappingURL=stock-survey.js.map

/***/ }),

/***/ 184:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FilesPageModule", function() { return FilesPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__files__ = __webpack_require__(185);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var FilesPageModule = /** @class */ (function () {
    function FilesPageModule() {
    }
    FilesPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__files__["a" /* FilesPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__files__["a" /* FilesPage */]),
            ],
        })
    ], FilesPageModule);
    return FilesPageModule;
}());

//# sourceMappingURL=files.module.js.map

/***/ }),

/***/ 185:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_photo_viewer__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the FilesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var FilesPage = /** @class */ (function () {
    function FilesPage(navCtrl, navParams, photoViewer, http, file, service) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.photoViewer = photoViewer;
        this.http = http;
        this.file = file;
        this.service = service;
        this.goToDir();
    }
    FilesPage.prototype.goToDir = function () {
        var _this = this;
        this.file
            .listDir(this.file.externalRootDirectory, "Download/Bonita-MWA/uploads/")
            .then(function (list) {
            _this.dirs = list;
        });
    };
    FilesPage.prototype.open = function (x) {
        this.photoViewer.show(this.file.externalRootDirectory + "/Download/Bonita-MWA/uploads/" + x.name);
    };
    FilesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-files",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/files/files.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>Files</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-item *ngFor="let x of dirs" (click)="open(x)">\n    <ion-icon name=\'images\' class="file-color" item-start></ion-icon>\n    {{x.name}}\n  </ion-item>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/files/files.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_photo_viewer__["a" /* PhotoViewer */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_5__providers_service_service__["a" /* ServiceProvider */]])
    ], FilesPage);
    return FilesPage;
}());

//# sourceMappingURL=files.js.map

/***/ }),

/***/ 187:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__login__ = __webpack_require__(100);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__login__["a" /* LoginPage */]),
            ],
        })
    ], LoginPageModule);
    return LoginPageModule;
}());

//# sourceMappingURL=login.module.js.map

/***/ }),

/***/ 199:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__ = __webpack_require__(339);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// import { HttpClient } from '@angular/common/http';




/*
  Generated class for the AuthServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var apiUrl = "https://bonita.telkomcel.tl/apibonita/index.php/Auth/login/";
var AuthServiceProvider = /** @class */ (function () {
    function AuthServiceProvider(http, network) {
        this.http = http;
        this.network = network;
        console.log("Hello AuthServiceProvider Provider");
    }
    AuthServiceProvider.prototype.isOnline = function () {
        console.log(this.network.type);
        if (this.network.type != "none") {
            return true;
        }
        else {
            return false;
        }
    };
    AuthServiceProvider.prototype.postData = function (credentials, type) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]();
            _this.http
                .post(apiUrl + type, JSON.stringify(credentials), { headers: headers })
                .subscribe(function (res) {
                resolve(res.json());
            }, function (err) {
                reject(err);
            });
        });
    };
    AuthServiceProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */], __WEBPACK_IMPORTED_MODULE_1__ionic_native_network__["a" /* Network */]])
    ], AuthServiceProvider);
    return AuthServiceProvider;
}());

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 203:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SalesOrderPageModule", function() { return SalesOrderPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__sales_order__ = __webpack_require__(340);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var SalesOrderPageModule = /** @class */ (function () {
    function SalesOrderPageModule() {
    }
    SalesOrderPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__sales_order__["a" /* SalesOrderPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__sales_order__["a" /* SalesOrderPage */]),
            ],
        })
    ], SalesOrderPageModule);
    return SalesOrderPageModule;
}());

//# sourceMappingURL=sales-order.module.js.map

/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StockSurveyPageModule", function() { return StockSurveyPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__stock_survey__ = __webpack_require__(183);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var StockSurveyPageModule = /** @class */ (function () {
    function StockSurveyPageModule() {
    }
    StockSurveyPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__stock_survey__["a" /* StockSurveyPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__stock_survey__["a" /* StockSurveyPage */]),
            ],
        })
    ], StockSurveyPageModule);
    return StockSurveyPageModule;
}());

//# sourceMappingURL=stock-survey.module.js.map

/***/ }),

/***/ 205:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewActivityPageModule", function() { return ViewActivityPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view_activity__ = __webpack_require__(96);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewActivityPageModule = /** @class */ (function () {
    function ViewActivityPageModule() {
    }
    ViewActivityPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__view_activity__["a" /* ViewActivityPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__view_activity__["a" /* ViewActivityPage */]),
            ],
        })
    ], ViewActivityPageModule);
    return ViewActivityPageModule;
}());

//# sourceMappingURL=view-activity.module.js.map

/***/ }),

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ViewAllPageModule", function() { return ViewAllPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__view_all__ = __webpack_require__(341);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ViewAllPageModule = /** @class */ (function () {
    function ViewAllPageModule() {
    }
    ViewAllPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__view_all__["a" /* ViewAllPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__view_all__["a" /* ViewAllPage */]),
            ],
        })
    ], ViewAllPageModule);
    return ViewAllPageModule;
}());

//# sourceMappingURL=view-all.module.js.map

/***/ }),

/***/ 207:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditoutletPageModule", function() { return EditoutletPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__editoutlet__ = __webpack_require__(342);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var EditoutletPageModule = /** @class */ (function () {
    function EditoutletPageModule() {
    }
    EditoutletPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__editoutlet__["a" /* EditoutletPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__editoutlet__["a" /* EditoutletPage */]),
            ],
        })
    ], EditoutletPageModule);
    return EditoutletPageModule;
}());

//# sourceMappingURL=editoutlet.module.js.map

/***/ }),

/***/ 208:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UpdateOutletProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__near_by_near_by__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UpdateOutletProfilePage = /** @class */ (function () {
    function UpdateOutletProfilePage(navCtrl, service, navParams, http) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.navParams = navParams;
        this.http = http;
        this.id = navParams.get("id");
        this.loadData();
    }
    UpdateOutletProfilePage.prototype.loadData = function () {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + 'mwa/getOutlets?id=' + this.id);
        data.subscribe(function (result) {
            _this.items = result;
        });
    };
    UpdateOutletProfilePage.prototype.editOutlet = function (id) {
        this.navCtrl.push("EditoutletPage", { id: id });
    };
    UpdateOutletProfilePage.prototype.toNearby = function (lat, lng) {
        if (lat === void 0) { lat = ''; }
        if (lng === void 0) { lng = ''; }
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__near_by_near_by__["a" /* NearByPage */], { lat: lat, lng: lng });
    };
    UpdateOutletProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-update-outlet-profile",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/update-outlet-profile/update-outlet-profile.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>\n      Outlet Profile\n    </ion-title>\n    <ion-buttons end *ngFor="let item of items">\n      <button (click)="editOutlet(item.id)" ion-button>\n        Edit \n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page7" *ngFor="let item of items">\n  <img class="center" *ngIf="item.outlet_photo !== null && item.outlet_photo !== \'null\'; else open; " src="{{\'http://150.242.111.235/apibonita/uploads/\'+item.outlet_photo}}" style="display:block;margin-top: 60px"/>\n  <ng-template #open>\n  <img class="center" src="assets/img/users.png" style="display:block;margin-top: 60px"/>\n  </ng-template>\n  <ion-item class="item-profile">\n    <h5 class="center">{{item.name}}</h5>\n    <p class="center">{{item.contact_number}}</p>\n  </ion-item>\n  <ion-card>\n      <ion-item id="updateOutletProfile-input12" >\n          <h4>Nomor Pulsa Digital</h4>\n          <p style="white-space: initial !important;">{{item.pd_no}}</p>\n        </ion-item>\n      <ion-item id="updateOutletProfile-input12" >\n        <h4>Address</h4>\n        <p style="white-space: initial !important;">{{item.address}}</p>\n      </ion-item>\n      <ion-item id="updateOutletProfile-input12" >\n        <h4>Type Outlet</h4>\n        <p style="white-space: initial !important;">{{item.typeOutlet}}</p>\n      </ion-item>\n      <ion-item id="updateOutletProfile-select2">\n        <h4>Municipio</h4>\n        <p>{{item.municipio}}</p>\n      </ion-item>\n      <ion-item id="updateOutletProfile-select3">\n        <h4>Posto Administativo</h4>\n        <p>{{item.posto_adm}}</p>\n      </ion-item>\n    <ion-item id="updateOutletProfile-select3">\n        <h4>Cluster</h4>\n        <p>{{item.cluster}}</p>\n      </ion-item>\n      <ion-item id="updateOutletProfile-select4">\n        <h4>Suco</h4>\n        <p>{{item.suco}}</p>\n      </ion-item>\n      <ion-item id="updateOutletProfile-select5">\n        <h4>Aldeia</h4>\n        <p>{{item.aldeia}}</p>\n      </ion-item>\n      <ion-item id="updateOutletProfile-input13">\n        <h4>Latitude</h4>\n        <p>{{item.lat}}</p>\n      </ion-item>\n      <ion-item id="updateOutletProfile-input14">\n        <h4>Longtitude</h4>\n        <p>{{item.lng}}</p>\n      </ion-item>\n  </ion-card>\n   <ion-fab right bottom>\n    <button ion-fab color="success" (click)="toNearby(item.lat,item.lng)"><ion-icon name="pin"></ion-icon></button>\n  </ion-fab>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/update-outlet-profile/update-outlet-profile.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], UpdateOutletProfilePage);
    return UpdateOutletProfilePage;
}());

//# sourceMappingURL=update-outlet-profile.js.map

/***/ }),

/***/ 209:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddoutletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__outlet_outlet__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the AddoutletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var AddoutletPage = /** @class */ (function () {
    function AddoutletPage(navCtrl, camera, toastCtrl, navParams, http, loadingCtrl, geolocation, actionSheetCtrl, service) {
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.geolocation = geolocation;
        this.actionSheetCtrl = actionSheetCtrl;
        this.service = service;
        this.p = [];
        this.s = [];
        this.i = [];
    }
    AddoutletPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: "Choose Upload Photo",
            buttons: [
                {
                    text: "Open Gallery",
                    role: "gallery",
                    handler: function () {
                        _this.toGetCamera(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: "Open Camera",
                    role: "camera",
                    handler: function () {
                        _this.toGetCamera(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                        console.log("Cancel clicked");
                    }
                }
            ]
        });
        actionSheet.present();
    };
    AddoutletPage.prototype.ionViewDidLoad = function () {
        this.img = "../assets/img/users.png";
        var cl = localStorage.getItem('cl');
        this.getIdCluster(cl);
        this.getAreaCluster();
        this.getTypeOutlet();
    };
    // TO -> GET
    AddoutletPage.prototype.toGetPosto = function () {
        this.getPosto(this.municipio);
    };
    AddoutletPage.prototype.toGetSuco = function () {
        this.getSuco(this.posto_adm);
    };
    AddoutletPage.prototype.toGetAldeia = function () {
        this.getAldeia(this.suco);
    };
    AddoutletPage.prototype.toGetLocation = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Please Wait.."
        });
        loading.present();
        this.geolocation
            .getCurrentPosition()
            .then(function (resp) {
            _this.lat = resp.coords.latitude;
            _this.lng = resp.coords.longitude;
            loading.dismiss();
        })
            .catch(function (error) {
            _this.toast("Error getting location");
            loading.dismiss();
        });
    };
    AddoutletPage.prototype.getBase64 = function (file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            console.log(reader.result);
        };
        reader.onerror = function (error) {
            console.log("Error: ", error);
        };
    };
    AddoutletPage.prototype.toGetCamera = function (mt) {
        var _this = this;
        var options = {
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            sourceType: mt
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            var p = "data:image/jpeg;base64," + imageData;
            _this.img = p;
            _this.photo = p;
        }, function (err) {
            // Handle error
        });
    };
    AddoutletPage.prototype.toUploadPhoto = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.img = event.target.result;
                _this.address = _this.img;
            };
            this.address = event.target.files[0];
            reader.readAsDataURL(event.target.files[0]);
        }
        var fileList = event.target.files;
        this.photo = fileList[0];
    };
    // POST
    AddoutletPage.prototype.addOutlet = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Please Wait.."
        });
        loading.present();
        console.log(this.area);
        if (this.name == "undefined" || this.name == undefined) {
            this.toast("Outlet name can not be empty!");
            loading.dismiss();
        }
        else if (this.digital_number == "undefined" || this.digital_number == undefined) {
            this.toast("Pulsa Digital Number. can not be empty!");
            loading.dismiss();
        }
        else if (this.area == undefined || this.area == 'undefined' || this.area == '') {
            this.toast("Aldeia. can not be empty!");
            loading.dismiss();
        }
        else {
            var body = new FormData();
            var rex = [];
            rex = this.area.split(',');
            body.append("image", this.photo);
            body.append("name", this.name);
            body.append("contact_number", this.contact_number);
            body.append("pd_no", this.digital_number);
            body.append("cl", localStorage.getItem('cl'));
            body.append("chID", localStorage.getItem('chID'));
            body.append("sales_team_id", localStorage.getItem('stId'));
            body.append("address", this.address);
            body.append("email", this.email);
            body.append("lat", this.lat);
            body.append("lng", this.lng);
            body.append("typeOutlet", this.type);
            body.append("municipio", rex[1]);
            body.append("posto_adm", rex[2]);
            body.append("suco", rex[3]);
            body.append("aldeia", rex[0]);
            body.append("photo", this.photoz);
            body.append("created_by", localStorage.getItem("id"));
            body.append("idCh", localStorage.getItem("idCh"));
            // if(body == "") {
            //     this.toast("Error");
            // }
            this.http.post(this.service.urlRoot + "mwa/addOutlets", body)
                .subscribe(function (res) {
                _this.i = res;
                debugger;
                if (_this.i.info == 1) {
                    _this.toast("Sorry your name has been used, please enter other name!");
                }
                else if (_this.i.info == 3) {
                    _this.toast("Sorry your digital number credit has been used, please enter other number!");
                }
                else {
                    _this.toast("Success to Add Outlet");
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__outlet_outlet__["a" /* OutletPage */]);
                }
            }, function (err) {
                console.log(err);
                _this.toast("Error to Add Outlet");
            });
            loading.dismiss();
        }
    };
    // GET
    AddoutletPage.prototype.getIdCluster = function (id) {
        var _this = this;
        this.http.get(this.service.urlRoot + 'mwa/getIdCluster?id=' + id).subscribe(function (d) {
            _this.clusters = d[0].cluster;
        });
    };
    AddoutletPage.prototype.getAreaCluster = function () {
        var _this = this;
        var data = new FormData;
        this.http.get(this.service.urlRoot + 'mwa/getAreaCluster?cluster=' + localStorage.getItem('cl')).subscribe(function (d) {
            _this.municipio = d['municipio'];
            _this.posto_adm = d['posto_adm'];
        });
    };
    AddoutletPage.prototype.getAreaClusterSuco = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Please Wait.."
        });
        loading.present();
        this.http.get(this.service.urlRoot + 'mwa/getAreaCluster?cluster=' + localStorage.getItem('cl') + '&posto=' + this.p).subscribe(function (d) {
            _this.suco = d['suco'];
            loading.dismiss();
        });
    };
    AddoutletPage.prototype.getAreaClusterAldeia = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Please Wait.."
        });
        loading.present();
        this.http.get(this.service.urlRoot + 'mwa/getAreaCluster?cluster=' + localStorage.getItem('cl') + '&suco=' + this.s).subscribe(function (d) {
            _this.aldeias = d['aldeia'];
            loading.dismiss();
        });
    };
    AddoutletPage.prototype.getMuicipio = function () {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getMunicipio");
        data.subscribe(function (result) {
            _this.munic = result;
        });
    };
    AddoutletPage.prototype.getTypeOutlet = function () {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getTypeOutlet");
        data.subscribe(function (result) {
            if (result['success'] == true) {
                _this.type_outlet = result['data'];
            }
            else {
                console.log("getTypeOutlet : ", result['success']);
            }
        });
    };
    AddoutletPage.prototype.getPosto = function (id) {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getPosto?id=" + id);
        data.subscribe(function (result) {
            _this.posto = result;
        });
    };
    AddoutletPage.prototype.getSuco = function (id) {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getSuco?id=" + id);
        data.subscribe(function (result) {
            _this.sucos = result;
        });
    };
    AddoutletPage.prototype.getAldeia = function (id) {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getAldeia?id=" + id);
        data.subscribe(function (result) {
            _this.aldeias = result;
        });
    };
    AddoutletPage.prototype.toast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: "bottom"
        });
        toast.onDidDismiss(function () {
            console.log("Dismissed toast");
        });
        toast.present();
    };
    AddoutletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-addoutlet",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/addoutlet/addoutlet.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>Add Outlet</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n  <ion-label color="danger" stacked>Upload Photo</ion-label>\n  <!-- <ion-input type="file"  success block (change)="toUploadPhoto($event)"></ion-input> -->\n  <button ion-button color="danger" block (click)="presentActionSheet()">\n    Upload Foto\n  </button>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-4>\n        <img [src]="img" (click)="toGetCamera($event)" src="https://ionicframework.com/dist/preview-app/www/assets/img/venkman.jpg">\n      </ion-col>\n      <ion-col col-8>\n        <ion-item id="updateOutletProfile-input10">\n          <ion-label color="danger" floating>Outlet Name.</ion-label>\n          <ion-input required type="text" [(ngModel)]="name"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-item id="updateOutletProfile-input11">\n    <ion-label color="danger" floating>Contact No.</ion-label>\n    <ion-input type="number" [(ngModel)]="contact_number"></ion-input>\n  </ion-item>\n  <ion-item id="updateOutletProfile-input11">\n    <ion-label color="danger" floating>Pulsa Digital No.</ion-label>\n    <ion-input type="number" [(ngModel)]="digital_number"></ion-input>\n  </ion-item>\n  <ion-item id="updateOutletProfile-input13">\n    <ion-label color="danger" floating>Address</ion-label>\n    <ion-textarea type="text" [(ngModel)]="address"></ion-textarea>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-input12">\n    <ion-label color="danger" floating>Cluster</ion-label>\n    <ion-input type="text" readonly [(ngModel)]="clusters" ></ion-input>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-input12">\n    <ion-label color="danger" floating>Type Outlet</ion-label>\n    <ion-select [(ngModel)]="type">\n      <ion-option *ngFor="let i of type_outlet" value="{{i.id}}" >{{i.name}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-input12">\n    <ion-label color="danger" floating>Municipio</ion-label>\n    <ion-input type="text" readonly [(ngModel)]="municipio" ></ion-input>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-select5">\n    <ion-label color="danger" floating>Posto Administativo</ion-label>\n    <ion-select [(ngModel)]="p" (ionChange)="getAreaClusterSuco()">\n      <ion-option *ngFor="let i of posto_adm" value="{{i.id}}" >{{i.posto_adm}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-select5">\n    <ion-label color="danger" floating>Suco</ion-label>\n    <ion-select [(ngModel)]="s" (ionChange)="getAreaClusterAldeia()">\n      <ion-option *ngFor="let i of suco" value="{{i.id}}">{{i.suco}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-select5">\n    <ion-label color="danger" floating>Aldeia</ion-label>\n    <ion-select [(ngModel)]="area" >\n      <ion-option *ngFor="let i of aldeias" value="{{i.a_id}},{{i.m_id}},{{i.pa_id}},{{i.s_id}}">{{i.aldeia}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-input12">\n      <ion-label color="danger" floating>Email</ion-label>\n      <ion-input type="email"  [(ngModel)]="email" ></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <button id="updateOutletProfile-button5" (click)="toGetLocation()" color="secondary" ion-button success block>\n      Get Current GPS Location\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="danger" floating>Latitude</ion-label>\n    <ion-input type="text" [(ngModel)]="lat"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label color="danger" floating>Longitude</ion-label>\n    <ion-input type="text" [(ngModel)]="lng"></ion-input>\n  </ion-item>\n\n  <button id="updateOutletProfile-button6" ion-button color="danger" block (click)="addOutlet()">\n    Save\n  </button>\n\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/addoutlet/addoutlet.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_service_service__["a" /* ServiceProvider */]])
    ], AddoutletPage);
    return AddoutletPage;
}());

//# sourceMappingURL=addoutlet.js.map

/***/ }),

/***/ 210:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AddoutletPageModule", function() { return AddoutletPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__addoutlet__ = __webpack_require__(209);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AddoutletPageModule = /** @class */ (function () {
    function AddoutletPageModule() {
    }
    AddoutletPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__addoutlet__["a" /* AddoutletPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__addoutlet__["a" /* AddoutletPage */]),
            ],
        })
    ], AddoutletPageModule);
    return AddoutletPageModule;
}());

//# sourceMappingURL=addoutlet.module.js.map

/***/ }),

/***/ 252:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CloudSharingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_file_transfer__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_photo_viewer__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__files_files__ = __webpack_require__(185);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







// import { FileOpener } from "@ionic-native/file-opener";
// import { FileChooser } from '@ionic-native/file-chooser';



var CloudSharingPage = /** @class */ (function () {
    function CloudSharingPage(alertCtrl, navCtrl, transfer, camera, loadingCtrl, toastCtrl, http, photoViewer, file, filePath, actionSheetCtrl, platform, service) {
        this.alertCtrl = alertCtrl;
        this.navCtrl = navCtrl;
        this.transfer = transfer;
        this.camera = camera;
        this.loadingCtrl = loadingCtrl;
        this.toastCtrl = toastCtrl;
        this.http = http;
        this.photoViewer = photoViewer;
        this.file = file;
        this.filePath = filePath;
        this.actionSheetCtrl = actionSheetCtrl;
        this.platform = platform;
        this.service = service;
        this.cloud = "My Phone";
        this.isAndroid = true;
    }
    CloudSharingPage.prototype.ionViewWillEnter = function () {
        this.getCloudSharing();
    };
    CloudSharingPage.prototype.ubah = function () {
        this.getCloudSharing();
    };
    CloudSharingPage.prototype.show = function (v) {
        var _this = this;
        // this.photoViewer.show("http://150.242.111.235/apibonita/" + v);
        var loading = this.loadingCtrl.create({
            content: "Downloading..."
        });
        loading.present();
        var FileTransfer = this.transfer.create();
        var url = this.service.url + v;
        var Path = this.file.externalRootDirectory + "/Download/Bonita-MWA/";
        console.log(url);
        FileTransfer.download(url, Path + v).then(function (entry) {
            _this.presentToast("Download Complete: " + entry.toURL());
            loading.dismiss();
        }, function (error) {
            _this.presentToast("Error " + error);
            console.log(error);
            loading.dismiss();
        });
    };
    CloudSharingPage.prototype.getCloudSharing = function () {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getCloudSharing");
        data.subscribe(function (result) {
            _this.data = result;
            console.log(data);
        });
    };
    CloudSharingPage.prototype.uploadFile = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.imageURI = event.target.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }
        var fileList = event.target.files;
        var files = event.target.files;
        this.tempFile = files[0];
        debugger;
        var bodyRequest = new FormData();
        bodyRequest.append("id", JSON.parse(localStorage.getItem("id")));
        bodyRequest.append("image", this.tempFile);
        bodyRequest.append("photo", event.target.files);
        this.uploadToServer(bodyRequest).subscribe(function (res) {
            _this.presentToast("Success upload file");
            console.log("Upload Success");
        });
    };
    CloudSharingPage.prototype.getImage = function () {
        this.presentActionSheet();
    };
    CloudSharingPage.prototype.uploadToServer = function (bodyRequest) {
        return this.http
            .post(this.service.urlRoot + "mwa/uploadCloudSharing", bodyRequest)
            .map(function (res) {
            // this.presentToast("Upload Success!")
        });
    };
    CloudSharingPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: "bottom"
        });
        toast.onDidDismiss(function () {
            console.log("Dismissed toast");
        });
        toast.present();
    };
    CloudSharingPage.prototype.fileChange = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.img1 = event.target.result;
            };
            reader.readAsDataURL(event.target.files[0]);
        }
        var fileList = event.target.files;
        var files = event.target.files;
        this.tempFile = files[0];
        var bodyRequest = new FormData();
        bodyRequest.append("photo", this.tempFile);
        bodyRequest.append("img", event.target.result);
        this.uploadToServer(bodyRequest).subscribe(function (res) {
            console.log(res);
            console.log(event.target.result);
        });
    };
    CloudSharingPage.prototype.gotoFiles = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__files_files__["a" /* FilesPage */]);
    };
    CloudSharingPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: "Select Image Source",
            buttons: [
                {
                    text: "Load from Library",
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: "Use Camera",
                    handler: function () {
                        _this.takePicture(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: "Cancel",
                    role: "cancel"
                }
            ]
        });
        actionSheet.present();
    };
    CloudSharingPage.prototype.takePicture = function (sourceType) {
        var _this = this;
        // Create options for the Camera Dialog
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Get the data of an image
        this.camera.getPicture(options).then(function (imageData) {
            _this.imageURI = imageData;
            var p = "data:image/jpeg;base64," + imageData;
        }, function (err) {
            console.log(err);
            _this.presentToast(err);
        });
    };
    CloudSharingPage.prototype.upload = function () {
        var _this = this;
        var loader = this.loadingCtrl.create({
            content: "Uploading..."
        });
        loader.present();
        var fileTransfer = this.transfer.create();
        fileTransfer
            .upload(this.imageURI, this.service.urlRoot + "mwa/uploadCloudSharing")
            .then(function (data) {
            console.log(data + " Uploaded Successfully");
            loader.dismiss();
            _this.presentToast("Image uploaded successfully");
        }, function (err) {
            console.log(err);
            loader.dismiss();
            _this.presentToast(err);
        });
    };
    CloudSharingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-cloud-sharing",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/cloud-sharing/cloud-sharing.html"*/'t<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>\n      Cloud Sharing\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page5">\n  <ion-segment [(ngModel)]="cloud" ionChange="ubah($event)">\n    <ion-segment-button color="danger" value="My Phone">\n      My Phone\n    </ion-segment-button>\n    <ion-segment-button color="danger" value="My Cloud">\n      My Cloud\n    </ion-segment-button>\n  </ion-segment>\n\n  <ion-content>\n    <div [ngSwitch]="cloud">\n      <ion-list *ngSwitchCase="\'My Phone\'" >\n        <form id="form" action="#" method="post" enctype="multipart/form-data">\n          <ion-item color="none" id="My Files" (click)="gotoFiles()">\n            <ion-avatar item-left>\n              <img src="assets/img/outbox.png" />\n            </ion-avatar>\n            <h2>\n              My Files\n            </h2>\n            <p>All bonita downloaded files</p>\n          </ion-item>\n          <!-- <ion-item color="none" id="My Files" (click)="getImage()" (change)="uploadFile($event)">\n          <ion-avatar item-left>\n            <img src="assets/img/upload.png" />\n          </ion-avatar>\n          <h2>\n            Upload Photo\n          </h2>\n          <p>All bonita downloaded files</p>\n        </ion-item> -->\n          <ion-item color="none" id="Upload">\n            <h2>\n              Upload File\n            </h2>\n            <p>Click to upload Files to server</p>\n            <input id="uploadImage" type="file" accept="*" name="image" (change)="uploadFile($event)" />\n          </ion-item>\n          <img *ngIf="imageURI" [src]="imageURI" />\n          <div id="preview"></div>\n          <br>\n          <!-- <button ion-button color="blue" block (change)="uploadFile($event)">Upload</button> -->\n        </form>\n        <!-- <button id="upload-btn" ion-button color="danger" block (click)="upload()">\n          Upload\n        </button> -->\n      </ion-list>\n      \n      <ion-list *ngSwitchCase="\'My Cloud\'">\n        <ion-item id="My Files">\n          <ion-item *ngFor="let i of data" (click)="show(i.file_name)">\n            <ion-icon name=\'images\' class="file-color" item-start></ion-icon>\n            <p>{{i.file_name}}</p>\n          </ion-item>\n        </ion-item>\n      </ion-list>\n    </div>\n  </ion-content>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/cloud-sharing/cloud-sharing.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_photo_viewer__["a" /* PhotoViewer */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_file_path__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_9__providers_service_service__["a" /* ServiceProvider */]])
    ], CloudSharingPage);
    return CloudSharingPage;
}());

//   chooseFile() {
//     this.fileChooser.open().then(file=>{
//       this.filePath.resolveNativePath(file).then(resolveFilePath => {
//         this.fileOpener.open(resolveFilePath, 'application/pdf').then(value => {
//           alert('It worked!')
//         }).catch(err => {
//           alert(JSON.stringify(err));
//         });
//       }).catch(err => {
//         alert(JSON.stringify(err));
//     });
//   }).catch (err => {
//     alert(JSON.stringify(err));
// });
// }
//# sourceMappingURL=cloud-sharing.js.map

/***/ }),

/***/ 255:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangePasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChangePasswordPage = /** @class */ (function () {
    function ChangePasswordPage(service, navCtrl, alertCtrl, http) {
        this.service = service;
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.http = http;
        console.log(localStorage.getItem('id'));
    }
    ChangePasswordPage.prototype.changePassword = function () {
        var _this = this;
        var data;
        var postData = new FormData();
        postData.append('oldpassword', this.oldpassword.value);
        postData.append('password', this.password.value);
        postData.append('id', localStorage.getItem('id'));
        postData.append('newpassword', this.newpassword.value);
        data = this.http.post(this.service.urlRoot + "mwa/change_password", postData);
        data.subscribe(function (result) {
            _this.items = result;
            console.log(_this.items);
            if (_this.items['action'] == 1) {
                _this.alerts('Error', 'Your old password was entered incorrectly. Please enter it again.');
            }
            else if (_this.items['action'] == 2) {
                _this.alerts('Error', 'Password do not match!');
            }
            else if (_this.items['action'] == 3) {
                _this.alerts('Error', 'Your new password can not be the same as old password.');
            }
            else if (_this.items['action'] == 4) {
                _this.alerts("Success", "Your password has been successfully changed.");
                _this.navCtrl.pop();
            }
        });
    };
    ChangePasswordPage.prototype.showConfirm = function () {
        this.checkInput(this.oldpassword.value, this.password.value, this.newpassword.value);
    };
    ChangePasswordPage.prototype.checkInput = function (a, b, c) {
        var _this = this;
        if (a == "" || b == "" || c == "") {
            this.alerts('Input Required', 'Please enter your password!');
        }
        else {
            var confirm_1 = this.alertCtrl.create({
                title: 'Change Password',
                message: 'Do you agree to change your password?',
                buttons: [
                    {
                        text: 'Disagree',
                        handler: function () {
                            console.log('Failed');
                        }
                    },
                    {
                        text: 'Agree',
                        handler: function () {
                            _this.changePassword();
                        }
                    }
                ]
            });
            confirm_1.present();
        }
    };
    ChangePasswordPage.prototype.alerts = function (t, st) {
        var alert = this.alertCtrl.create({
            title: t,
            subTitle: st,
            buttons: ['Dismiss']
        });
        alert.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('oldpassword'),
        __metadata("design:type", Object)
    ], ChangePasswordPage.prototype, "oldpassword", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('password'),
        __metadata("design:type", Object)
    ], ChangePasswordPage.prototype, "password", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('newpassword'),
        __metadata("design:type", Object)
    ], ChangePasswordPage.prototype, "newpassword", void 0);
    ChangePasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-change-password",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/change-password/change-password.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>\n      Change Password\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content padding id="page2">\n  <ion-card class="card-pwd">\n    <form id="login-form1" class="form-pwd">\n      <ion-item id="login-input1">\n        <ion-label floating block>Old Password</ion-label>\n        <ion-input type="password" #oldpassword required></ion-input>\n      </ion-item>\n      <ion-item id="login-input2">\n        <ion-label floating block>New Password</ion-label>\n        <ion-input type="password" #password required></ion-input>\n      </ion-item>\n      <ion-item id="login-input2">\n        <ion-label floating block>Confirm New Password</ion-label>\n        <ion-input type="password" #newpassword required></ion-input>\n      </ion-item>\n      <div class="spacer" style="height:10px;" id="login-spacer1"></div>\n      <button id="btn-Login" ion-button color="danger" block (click)="showConfirm()">\n        Save Change\n      </button>\n    </form>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/change-password/change-password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */]])
    ], ChangePasswordPage);
    return ChangePasswordPage;
}());

//# sourceMappingURL=change-password.js.map

/***/ }),

/***/ 256:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(276);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 276:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_bonita_mwa_bonita_mwa__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login_module__ = __webpack_require__(187);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_addoutlet_addoutlet_module__ = __webpack_require__(210);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_view_activity_view_activity_module__ = __webpack_require__(205);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_checkin_checkin_module__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_todo_list_todo_list__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_cloud_sharing_cloud_sharing__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_outlet_outlet__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_editoutlet_editoutlet_module__ = __webpack_require__(207);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_update_outlet_profile_update_outlet_profile__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_near_by_near_by__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_change_password_change_password__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_geolocation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__ = __webpack_require__(253);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_file__ = __webpack_require__(99);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_camera__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_image_picker__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_base64__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_route_route__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__angular_http__ = __webpack_require__(201);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__ionic_native_network__ = __webpack_require__(200);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__ionic_native_toast__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__ionic_native_diagnostic__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__ionic_native_file_path__ = __webpack_require__(254);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__ionic_native_status_bar__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__ionic_native_photo_viewer__ = __webpack_require__(97);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__ionic_native_splash_screen__ = __webpack_require__(251);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__providers_rest_rest__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__providers_auth_service_auth_service__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_files_files_module__ = __webpack_require__(184);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_stock_survey_stock_survey_module__ = __webpack_require__(204);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_sales_order_sales_order_module__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_view_all_view_all_module__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__ionic_native_open_native_settings__ = __webpack_require__(202);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_nearby_outlet_nearby_outlet__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_nearby_outlets_nearby_outlets__ = __webpack_require__(365);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











































var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_bonita_mwa_bonita_mwa__["a" /* BonitaMWAPage */],
                // LoginPage,
                // AddoutletPage,
                __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__["a" /* ProfilePage */],
                // CheckinPage,
                // ViewActivityPage,
                __WEBPACK_IMPORTED_MODULE_40__pages_nearby_outlet_nearby_outlet__["a" /* NearbyOutletPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_nearby_outlets_nearby_outlets__["a" /* NearbyOutletsPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_todo_list_todo_list__["a" /* TodoListPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_cloud_sharing_cloud_sharing__["a" /* CloudSharingPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_outlet_outlet__["a" /* OutletPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_update_outlet_profile_update_outlet_profile__["a" /* UpdateOutletProfilePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_near_by_near_by__["a" /* NearByPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_change_password_change_password__["a" /* ChangePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_route_route__["a" /* RoutePage */]
                // FilesPage
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_13__pages_editoutlet_editoutlet_module__["EditoutletPageModule"],
                __WEBPACK_IMPORTED_MODULE_6__pages_addoutlet_addoutlet_module__["AddoutletPageModule"],
                __WEBPACK_IMPORTED_MODULE_8__pages_checkin_checkin_module__["CheckinPageModule"],
                __WEBPACK_IMPORTED_MODULE_32__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_7__pages_view_activity_view_activity_module__["ViewActivityPageModule"],
                __WEBPACK_IMPORTED_MODULE_36__pages_stock_survey_stock_survey_module__["StockSurveyPageModule"],
                __WEBPACK_IMPORTED_MODULE_5__pages_login_login_module__["LoginPageModule"],
                __WEBPACK_IMPORTED_MODULE_35__pages_files_files_module__["FilesPageModule"],
                __WEBPACK_IMPORTED_MODULE_37__pages_sales_order_sales_order_module__["SalesOrderPageModule"],
                __WEBPACK_IMPORTED_MODULE_38__pages_view_all_view_all_module__["ViewAllPageModule"],
                __WEBPACK_IMPORTED_MODULE_24__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    scrollPadding: false,
                    scrollAssist: false,
                    autoFocusAssist: false
                }, {
                    links: [
                        { loadChildren: '../pages/checkin/checkin.module#CheckinPageModule', name: 'CheckinPage', segment: 'checkin', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/files/files.module#FilesPageModule', name: 'FilesPage', segment: 'files', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/sales-order/sales-order.module#SalesOrderPageModule', name: 'SalesOrderPage', segment: 'sales-order', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/stock-survey/stock-survey.module#StockSurveyPageModule', name: 'StockSurveyPage', segment: 'stock-survey', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/view-activity/view-activity.module#ViewActivityPageModule', name: 'ViewActivityPage', segment: 'view-activity', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/view-all/view-all.module#ViewAllPageModule', name: 'ViewAllPage', segment: 'view-all', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/view-penjualan/view-penjualan.module#ViewPenjualanPageModule', name: 'ViewPenjualanPage', segment: 'view-penjualan', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/editoutlet/editoutlet.module#EditoutletPageModule', name: 'EditoutletPage', segment: 'editoutlet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/addoutlet/addoutlet.module#AddoutletPageModule', name: 'AddoutletPage', segment: 'addoutlet', priority: 'low', defaultHistory: [] }
                    ]
                })
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_bonita_mwa_bonita_mwa__["a" /* BonitaMWAPage */],
                // ViewActivityPage,
                // CheckinPage,
                // AddoutletPage,
                // LoginPage,
                __WEBPACK_IMPORTED_MODULE_9__pages_profile_profile__["a" /* ProfilePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_todo_list_todo_list__["a" /* TodoListPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_cloud_sharing_cloud_sharing__["a" /* CloudSharingPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_outlet_outlet__["a" /* OutletPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_nearby_outlet_nearby_outlet__["a" /* NearbyOutletPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_nearby_outlets_nearby_outlets__["a" /* NearbyOutletsPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_update_outlet_profile_update_outlet_profile__["a" /* UpdateOutletProfilePage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_near_by_near_by__["a" /* NearByPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_change_password_change_password__["a" /* ChangePasswordPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_route_route__["a" /* RoutePage */]
                // FilesPage
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_29__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_31__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_27__ionic_native_diagnostic__["a" /* Diagnostic */],
                __WEBPACK_IMPORTED_MODULE_30__ionic_native_photo_viewer__["a" /* PhotoViewer */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_geolocation__["a" /* Geolocation */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__["a" /* FileTransfer */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_transfer__["b" /* FileTransferObject */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_file__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_28__ionic_native_file_path__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_39__ionic_native_open_native_settings__["a" /* OpenNativeSettings */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_image_picker__["a" /* ImagePicker */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_base64__["a" /* Base64 */],
                __WEBPACK_IMPORTED_MODULE_33__providers_rest_rest__["a" /* RestProvider */],
                __WEBPACK_IMPORTED_MODULE_34__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
                __WEBPACK_IMPORTED_MODULE_25__ionic_native_network__["a" /* Network */],
                __WEBPACK_IMPORTED_MODULE_26__ionic_native_toast__["a" /* Toast */],
                __WEBPACK_IMPORTED_MODULE_42__providers_service_service__["a" /* ServiceProvider */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 335:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 340:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SalesOrderPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__route_route__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
* Generated class for the SalesOrderPage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/
var SalesOrderPage = /** @class */ (function () {
    function SalesOrderPage(http, service, navCtrl, loadingCtrl, navParams, alertCtrl) {
        this.http = http;
        this.service = service;
        this.navCtrl = navCtrl;
        this.loadingCtrl = loadingCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.nominal = [];
        this.discount = [];
        this.unit_price = [];
        this.url = "http://150.242.111.235/apibonita/index.php/mwa/";
        this.default = 0;
        this.simcard = {};
        this.etopup = {};
        this.total = [];
        this.dollar = [];
        this.getIDJProduct(2);
        this.getIDJProductSimcard(1);
        this.getIDJProductEtopup(3);
    }
    SalesOrderPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad SalesOrderPage');
    };
    SalesOrderPage.prototype.changeValue = function (id) {
        var voucher = [];
        var nom = this.nominal.length;
        var dis = this.discount.length;
        var jml = 0;
        if (nom != 0) {
            jml = nom;
        }
        else if (dis != 0) {
            jml = dis;
        }
        for (var i = 0; i < jml; ++i) {
            if (this.nominal[i] != undefined || this.discount[i] != undefined) {
                this.dollar[i] = (this.nominal[i] * this.unit_price[i] * this.discount[i]) / 100;
                this.total[i] = ((this.nominal[i] * this.unit_price[i]) - this.dollar[i]);
            }
        }
        // Not Looping
        this.simcard.dollar = (this.simcard.nominal * this.simcard.price * this.simcard.discount) / 100;
        this.simcard.total = (this.simcard.nominal * this.simcard.price - this.simcard.dollar);
        this.etopup.dollar = (this.etopup.nominal * this.etopup.price * this.etopup.discount) / 100;
        this.etopup.total = (this.etopup.nominal * this.etopup.price - this.etopup.dollar);
    };
    SalesOrderPage.prototype.getIDJProduct = function (id) {
        var _this = this;
        this.product = [];
        this.http.get(this.service.urlRoot + 'mwa/getIDJProduct?id=' + id).subscribe(function (data) {
            var d = data;
            for (var i = 0; i < d.length; i++) {
                _this.total[d[i].id] = '0';
                _this.unit_price[d[i].id] = '0';
                _this.discount[d[i].id] = '0';
                _this.dollar[d[i].id] = '0';
                _this.nominal[d[i].id] = '0';
                _this.product.push({
                    id: d[i].id,
                    unit_price: d[i].unit_price,
                    discount: d[i].discount,
                    product_name: d[i].product_name,
                    value: '0'
                });
            }
        });
    };
    SalesOrderPage.prototype.getIDJProductSimcard = function (id) {
        var _this = this;
        this.http.get(this.service.urlRoot + 'mwa/getIDJProduct?id=' + id).subscribe(function (data) {
            _this.simcard.price = data[0].unit_price;
            _this.simcard.discount = data[0].discount;
        });
    };
    SalesOrderPage.prototype.getIDJProductEtopup = function (id) {
        var _this = this;
        this.http.get(this.service.urlRoot + 'mwa/getIDJProduct?id=' + id).subscribe(function (data) {
            _this.etopup.price = data[0].unit_price;
            _this.etopup.discount = data[0].discount;
        });
    };
    SalesOrderPage.prototype.toSalesOrder = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Please Wait.."
        });
        loading.present();
        var voucher = [];
        var nom = this.nominal.length;
        var dis = this.discount.length;
        var jml = 0;
        if (nom != 0) {
            jml = nom;
        }
        else if (dis != 0) {
            jml = dis;
        }
        for (var i = 0; i < jml; ++i) {
            if (this.nominal[i] != undefined || this.discount[i] != undefined) {
                voucher.push({
                    nominal: this.nominal[i],
                    discount: this.discount[i],
                    idProduct: i,
                    dollar: this.dollar[i],
                    idJp: 2,
                    total: this.total[i]
                });
            }
        }
        var s = this.simcard;
        var e = this.etopup;
        var SE = {
            simcard: [{
                    nominal: this.validValue(s.nominal),
                    discount: this.validValue(s.discount),
                    idProduct: 9999,
                    dollar: s.dollar,
                    total: s.total,
                    idJp: 1
                }],
            etopup: [{
                    nominal: this.validValue(e.nominal),
                    discount: this.validValue(e.discount),
                    idProduct: 6666,
                    dollar: e.dollar,
                    total: e.total,
                    idJp: 3
                }],
        };
        var data = new FormData;
        data.append('SE', JSON.stringify(SE));
        data.append('order', JSON.stringify(voucher));
        data.append('idu', localStorage.getItem('id'));
        data.append('idCh', localStorage.getItem('idCh'));
        data.append('oId', localStorage.getItem('oId'));
        data.append('idTask', localStorage.getItem('idTask'));
        data.append('idRtd', localStorage.getItem('idRtd'));
        data.append('stId', localStorage.getItem('stId'));
        data.append('balance', this.balance);
        this.http.post(this.service.urlRoot + 'mwa/saveSalesOrder', data).subscribe(function (d) {
            var alert = _this.alertCtrl.create({
                title: 'Info',
                subTitle: 'The Data Survey has been Added',
                buttons: ['Okay']
            });
            alert.present();
            loading.dismiss();
            _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__route_route__["a" /* RoutePage */], {
                'id': localStorage.getItem('idFromRoot'),
                'idTask': localStorage.getItem('idTask')
            });
        });
    };
    SalesOrderPage.prototype.validValue = function (v) {
        if (v == undefined) {
            v = '';
        }
        return v;
    };
    SalesOrderPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sales-order',template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/sales-order/sales-order.html"*/'<!--\n  Generated template for the StockSurveyPage page.\n  \n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  \n  <ion-navbar>\n    <ion-title>Sales Order</ion-title>\n  </ion-navbar>\n  \n</ion-header>\n\n\n<ion-content padding>\n  <ion-grid>\n    \n    <ion-row>\n      <ion-col col-12>\n        <div><b>#Simcard</b></div>\n        <hr>\n      </ion-col>\n      <ion-col col-12 >\n        <table border="1" width="100%">\n            <tr>\n                <td colspan="4"><b>Simcard</b></td>\n              </tr>\n            <tr>\n              <td>QTY</td>\n              <td>Price</td>\n              <td>Discount (%)</td>\n              <td>Discount ($)</td>\n            \n            </tr>\n               \n            <tr>\n              <td><ion-input type="number"   [(ngModel)]="simcard.nominal" value="0" (click)="simcard.nominal = \'\'"  (keyup)="changeValue($event)"></ion-input></td>\n              <td><ion-input type="number" readonly value="0.5" [(ngModel)]="simcard.price" value="0"></ion-input></td>\n              <td> <ion-input type="number"  [(ngModel)]="simcard.discount" (keyup)="changeValue($event)"  value="0"></ion-input></td>\n              <td><ion-input type="number" readonly [(ngModel)]="simcard.dollar" value="0"></ion-input></td>\n            </tr>\n            <tr>\n              <td colspan="2">Total</td>\n              <td colspan="2"><ion-input type="number"  value="0" readonly [(ngModel)]="simcard.total"></ion-input></td>\n            </tr>\n          </table>\n\n      </ion-col>\n    </ion-row>\n    \n    <ion-row>\n      <ion-col col-12>\n        <div><b>#Pulsa Digital</b></div>\n        <hr>\n      </ion-col>\n      <ion-col col-12 >\n\n          <table border="1" width="100%">\n              <tr>\n                  <td colspan="4"><b>Pulsa Digital</b></td>\n                </tr>\n              <tr>\n                <td>Nominal</td>\n                <td>Price</td>\n                <td>Discount (%)</td>\n                <td>Discount ($)</td>\n              </tr>\n             \n              <tr>\n                <td><ion-input type="number"   [(ngModel)]="etopup.nominal" value="0" (click)="etopup.nominal = \'\'" (keyup)="changeValue($event)"></ion-input></td>\n                <td><ion-input type="number" readonly value="0" [(ngModel)]="etopup.price" ></ion-input></td>\n                <td> <ion-input type="number"  [(ngModel)]="etopup.discount" value="0"  (keyup)="changeValue($event)"></ion-input></td>\n                <td><ion-input type="number" readonly value="0" [(ngModel)]="etopup.dollar"></ion-input></td>\n              </tr>\n              <tr>\n                <td colspan="2">Total</td>\n                <td colspan="2"><ion-input  type="number" readonly  value="0" readonly [(ngModel)]="etopup.total"></ion-input></td>\n              </tr>\n            </table>\n          \n      </ion-col>\n    </ion-row>\n    \n    <ion-row>\n      <ion-col col-12>\n        <div><b>#Voucher</b></div>\n        <hr>\n      </ion-col>\n      <ion-col col-12 >\n          \n          <div *ngFor="let i of product;" margin-bottom>\n            <table border="1" width="100%">\n                <tr>\n                    <td colspan="4"><b>{{i.product_name}}</b></td>\n                  </tr>\n                <tr>\n                  <td>QTY</td>\n                  <td>Price</td>\n                  <td>Discount (%)</td>\n                  <td>Discount ($)</td>\n                </tr>\n               \n                <tr>\n                  <td><ion-input type="number" [(ngModel)]="nominal[i.id]" (click)="nominal[i.id] = \'\'" (keyup)="changeValue($event)" ></ion-input></td>\n                  <td><ion-input type="number" readonly [(ngModel)]="unit_price[i.id]=i.unit_price"></ion-input></td>\n                  <td> <ion-input type="number"  [(ngModel)]="discount[i.id]=i.discount" (keyup)="changeValue($event)"></ion-input></td>\n                  <td><ion-input type="number" readonly [(ngModel)]="dollar[i.id]"></ion-input></td>\n                </tr>\n                <tr>\n                  <td colspan="2">Total</td>\n                  <td colspan="2">\n                    <ion-input type="number" readonly [(ngModel)]="total[i.id]"></ion-input>\n                  </td>\n                </tr> \n              </table>\n              \n          </div>\n      </ion-col>\n    </ion-row>\n    \n    <button ion-button round outline block (click)="toSalesOrder()">Submit</button>\n  </ion-grid>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/sales-order/sales-order.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_4__providers_service_service__["a" /* ServiceProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */]])
    ], SalesOrderPage);
    return SalesOrderPage;
}());

//# sourceMappingURL=sales-order.js.map

/***/ }),

/***/ 341:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewAllPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the ViewAllPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewAllPage = /** @class */ (function () {
    function ViewAllPage(navCtrl, http, service, navParams) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.service = service;
        this.navParams = navParams;
        this.stock = {};
        this.spenjualan = {};
        this.order = {};
        this.url = "http://150.242.111.235/apibonita/index.php/mwa/";
        this.oId = localStorage.getItem('oId');
        this.menu = "sStock";
        this.getStockSurvei(this.oId);
        this.getSalesOrder(this.oId);
        this.getSurvey(this.oId);
        this.getViewActivity(this.oId);
        this.getSurveiPenjualan(this.oId);
    }
    ViewAllPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ViewAllPage');
    };
    ViewAllPage.prototype.getStockSurvei = function (id) {
        var _this = this;
        this.http.get(this.service.urlRoot + 'mwa/getStockSurvei?id=' + id + '&idTask=' + localStorage.getItem('idTask')).subscribe(function (data) {
            _this.stock.simcard = data['simcard'];
            _this.stock.etopup = data['etopup'];
            _this.stock.voucher = data['voucher'];
            _this.stock.bundling = data['bundling'];
        });
    };
    ViewAllPage.prototype.getSalesOrder = function (id) {
        var _this = this;
        this.http.get(this.service.urlRoot + 'mwa/getSalesOrder?id=' + id + '&idTask=' + localStorage.getItem('idTask')).subscribe(function (data) {
            _this.order.simcard = data['simcard'];
            _this.order.etopup = data['etopup'];
            _this.order.voucher = data['voucher'];
        });
    };
    ViewAllPage.prototype.getSurveiPenjualan = function (id) {
        var _this = this;
        this.http.get(this.service.urlRoot + 'mwa/getSurveiPenjualan?id=' + id + '&idTask=' + localStorage.getItem('idTask')).subscribe(function (data) {
            _this.spenjualan.simcard = data['simcard'];
            _this.spenjualan.etopup = data['etopup'];
            _this.spenjualan.voucher = data['voucher'];
            _this.spenjualan.bundling = data['bundling'];
        });
    };
    ViewAllPage.prototype.getSurvey = function (id) {
        var _this = this;
        this.http.get(this.service.urlRoot + 'mwa/getSurvey?id=' + id + '&idTask=' + localStorage.getItem('idTask')).subscribe(function (data) {
            _this.survey = data;
        });
    };
    ViewAllPage.prototype.getViewActivity = function (id) {
        var _this = this;
        this.http.get(this.service.urlRoot + 'mwa/getViewActivity?id=' + id + '&idTask=' + localStorage.getItem('idTask')).subscribe(function (data) {
            _this.viewActivity = data;
        });
    };
    ViewAllPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-view-all',template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/view-all/view-all.html"*/'<!--\n  Generated template for the ViewAllPage page.\n  \n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  \n  <ion-navbar>\n    <ion-title>View All Activity</ion-title>\n  </ion-navbar>\n  \n</ion-header>\n\n<ion-content >\n  \n  <ion-segment class="pBT" [(ngModel)]="menu">\n    <ion-segment-button value="sStock">\n      Survey Stock\n    </ion-segment-button>\n    <ion-segment-button value="sSurveiPenjualan">\n      Sales Survey\n    </ion-segment-button>\n    <ion-segment-button value="sSales">\n      Sales Order\n    </ion-segment-button>\n    <ion-segment-button value="activityList">\n      Activity List\n    </ion-segment-button>\n    <ion-segment-button value="survey">\n      Survey\n    </ion-segment-button>\n  </ion-segment>\n  \n  <div [ngSwitch]="menu">\n    \n    <ion-grid *ngSwitchCase="\'sStock\'">\n      <ion-row>\n        <ion-col col-12 *ngFor="let i of stock.simcard">\n          <ion-row>\n            <ion-col col-12  style="border-bottom:solid 1px #EEE;">\n              <b>#Simcard</b>\n            </ion-col>\n          </ion-row>\n          \n          <table border="1" width="100%">\n              <tr>\n                  <td colspan="3"><b>Simcard</b></td>\n                </tr>\n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td>{{i.telkomcel}}</td>\n              <td>{{i.timor_telecom}}</td>\n              <td>{{i.telemor}}</td>\n            </tr>\n          </table>\n          \n        </ion-col>\n        <ion-col col-12  *ngFor="let i of stock.etopup">\n          <ion-row>\n            <ion-col col-12  style="border-bottom:solid 1px #EEE;">\n              <b>#Etopup</b>\n            </ion-col>\n          </ion-row>\n          \n          <table border="1" width="100%">\n              <tr>\n                  <td colspan="3"><b>Etopup</b></td>\n                </tr>\n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td>{{i.telkomcel}}</td>\n              <td>{{i.timor_telecom}}</td>\n              <td>{{i.telemor}}</td>\n            </tr>\n          </table>\n          \n        </ion-col>\n        <ion-col col-12><b>#Voucher</b></ion-col>\n        <ion-col col-12  *ngFor="let i of stock.voucher">\n          \n          <table border="1" width="100%">\n            <tr>\n              <td colspan="3"><b>{{i.product_name}}</b></td>\n            </tr>\n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td>{{i.telkomcel}}</td>\n              <td>{{i.timor_telecom}}</td>\n              <td>{{i.telemor}}</td>\n            </tr>\n          </table>\n          \n          \n        </ion-col>\n        <ion-col col-12><b>#Bundling</b></ion-col>\n        <ion-col col-12  *ngFor="let i of stock.bundling">\n          <table border="1" width="100%">\n            <tr>\n              <td colspan="3"><b>{{i.product_name}}</b></td>\n            </tr>\n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td>{{i.telkomcel}}</td>\n              <td>{{i.timor_telecom}}</td>\n              <td>{{i.telemor}}</td>\n            </tr>\n          </table>\n          \n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    \n    <ion-grid *ngSwitchCase="\'sSurveiPenjualan\'">\n      <ion-row>\n        <ion-col col-12 *ngFor="let i of spenjualan.simcard">\n          <ion-row>\n            <ion-col col-12  style="border-bottom:solid 1px #EEE;">\n              <b>#Simcard</b>\n            </ion-col>\n          </ion-row>\n          \n          <table border="1" width="100%">\n              <tr>\n                  <td colspan="1"><b>Simcard</b></td>\n                </tr>    \n                <tr>\n                  <td colspan="3">Total Amount</td>\n                </tr>\n\n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td>{{i.telkomcel}}</td>\n              <td>{{i.timor_telecom}}</td>\n              <td>{{i.telemor}}</td>\n            </tr>\n          </table>\n          \n        </ion-col>\n        <ion-col col-12  *ngFor="let i of spenjualan.etopup">\n          <ion-row>\n            <ion-col col-12  style="border-bottom:solid 1px #EEE;">\n              <b>#Etopup</b>\n            </ion-col>\n          </ion-row>\n          \n          <table border="1" width="100%">\n            <tr>\n              <td colspan="1"><b>Etopup</b></td>\n            </tr>    \n            <tr>\n              <td colspan="3">Total Amount</td>\n            </tr>\n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td>{{i.telkomcel}}</td>\n              <td>{{i.timor_telecom}}</td>\n              <td>{{i.telemor}}</td>\n            </tr>\n          </table>\n          \n        </ion-col>\n        <ion-col col-12><b>#Voucher</b></ion-col>\n        <ion-col col-12  *ngFor="let i of spenjualan.voucher">\n          <table border="1" width="100%">\n            <tr>\n              <td colspan="1"><b>{{i.product_name}}</b></td>\n            </tr>    \n            <tr>\n              <td colspan="3">Total Amount</td>\n            </tr>\n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td>{{i.telkomcel}}</td>\n              <td>{{i.timor_telecom}}</td>\n              <td>{{i.telemor}}</td>\n            </tr>\n          </table>\n          \n          \n        </ion-col>\n        <ion-col col-12><b>#Bundling</b></ion-col>\n        <ion-col col-12  *ngFor="let i of spenjualan.bundling">\n          \n          <table border="1" width="100%">\n            <tr>\n              <td colspan="1"><b>{{i.product_name}}</b></td>\n            </tr>    \n            <tr>\n              <td colspan="3">Total Amount</td>\n            </tr>\n            <tr>\n              <td>Telkomcel</td>\n              <td>Timor Telecom</td>\n              <td>Telemor</td>\n            </tr>\n            <tr>\n              <td>{{i.telkomcel}}</td>\n              <td>{{i.timor_telecom}}</td>\n              <td>{{i.telemor}}</td>\n            </tr>\n          </table>\n          \n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    \n    <ion-grid *ngSwitchCase="\'sSales\'" >\n      <ion-row>\n        <ion-col col-12 *ngFor="let i of order.simcard">\n          <ion-row>\n            <ion-col col-12  style="border-bottom:solid 1px #EEE;">\n              <b>#Simcard</b>\n            </ion-col>\n          </ion-row>\n          \n          <table border="1" width="100%">\n              <tr>\n                <td colspan="4"><b>Simcard</b></td>\n              </tr>  \n              <tr>\n                <td>QTY</td>\n                <td>Price</td>\n                <td>Discount %</td>\n                <td>Discount $</td>\n                \n              </tr>\n              <tr>\n                <td>{{i.nominal}}</td>\n                <td>{{i.price}}</td>\n                <td>{{i.discount}}</td>\n                <td>{{i.dollar}}</td>\n              </tr>\n              <tr>\n                <td colspan="2">Total</td>\n                <td colspan="2">{{i.total}}</td>\n              </tr>\n            </table>\n\n        </ion-col>\n        <ion-col col-12 *ngFor="let i of order.etopup">\n          <b>#Pulsa Digital</b>\n          <table border="1" width="100%">\n              <tr>\n                <td colspan="4"><b>Pulsa Digital</b></td>\n              </tr>  \n              <tr>\n                <td>Nominal</td>\n                <td>Price</td>\n                <td>Discount %</td>\n                <td>Dollar $</td>\n              </tr>\n              <tr>\n                <td>{{i.nominal}}</td>\n                <td>{{i.price}}</td>\n                <td>{{i.discount}}</td>\n                <td>{{i.dollar}}</td>\n              </tr>\n              <tr>\n                <td colspan="2">Total</td>\n                <td colspan="2">{{i.total}}</td>\n              </tr>\n              <tr>\n                <td colspan="2">Status</td>\n                <td colspan="2">\n                  <span *ngIf="i.status_approval==0" style="background:yellow;padding: 2px;color: #000;">Waiting</span>\n                  <span *ngIf="i.status_approval==1" style="background:green;padding: 2px;color: #FFF;">Approved</span>\n                  <span *ngIf="i.status_approval==2" style="background:tomato;padding: 2px;color: #FFF;">Rejected</span>\n                </td>\n              </tr>\n            </table>\n        </ion-col>\n        <ion-col col-12>\n          <b>#Voucher</b>\n          <hr>\n        </ion-col>\n        <ion-col col-12 *ngFor="let i of order.voucher">\n          \n          <table border="1" width="100%">\n              <tr>\n                <td colspan="4"><b>{{i.product_name}}</b></td>\n              </tr>  \n              <tr>\n                <td>QTY</td>\n                <td>Price</td>\n                <td>Discount %</td>\n                <td>Discount $</td>\n              </tr>\n              <tr>\n                <td>{{i.nominal}}</td>\n                <td>{{i.price}}</td>\n                <td>{{i.discount}}</td>\n                <td>{{i.dollar}}</td>\n              </tr>\n              <tr>\n                <td colspan="2">Total</td>\n                <td colspan="2">{{i.total}}</td>\n              </tr>\n            </table>\n\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    \n    <ion-list *ngSwitchCase="\'activityList\'">\n      <ion-item *ngFor="let i of viewActivity">\n        <ion-icon name="checkmark-circle" style="color: #32db64;" item-start *ngIf="i.check == 1"></ion-icon>\n        <ion-icon name="close-circle" style="color: red;" item-start *ngIf="i.check != 1"></ion-icon>\n        {{i.activity}}\n      </ion-item>\n    </ion-list>\n    \n    <ion-grid *ngSwitchCase="\'survey\'">\n      <ion-row>\n        <ion-col col-12 *ngFor="let i of survey">\n          <ion-row>\n            <ion-col col-12 >{{i.survey_name}} ?</ion-col>\n            <ion-col col-12>\n              <div class="answer">\n                {{i.answer}}\n              </div>\n            </ion-col>\n          </ion-row>\n        </ion-col>\n        \n      </ion-row>\n    </ion-grid>\n    \n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/view-all/view-all.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_3__providers_service_service__["a" /* ServiceProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */]])
    ], ViewAllPage);
    return ViewAllPage;
}());

//# sourceMappingURL=view-all.js.map

/***/ }),

/***/ 342:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditoutletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__outlet_outlet__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








/**
 * Generated class for the AddoutletPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditoutletPage = /** @class */ (function () {
    function EditoutletPage(navCtrl, camera, toastCtrl, navParams, http, geolocation, loadingCtrl, actionSheetCtrl, service) {
        this.navCtrl = navCtrl;
        this.camera = camera;
        this.toastCtrl = toastCtrl;
        this.navParams = navParams;
        this.http = http;
        this.geolocation = geolocation;
        this.loadingCtrl = loadingCtrl;
        this.actionSheetCtrl = actionSheetCtrl;
        this.service = service;
        this.i = [];
        this.p = [];
        this.s = [];
    }
    EditoutletPage.prototype.presentActionSheet = function () {
        var _this = this;
        var actionSheet = this.actionSheetCtrl.create({
            title: "Choose Upload Photo",
            buttons: [
                {
                    text: "Open Gallery",
                    role: "gallery",
                    handler: function () {
                        _this.toGetCamera(_this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: "Open Camera",
                    role: "camera",
                    handler: function () {
                        _this.toGetCamera(_this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: "Cancel",
                    role: "cancel",
                    handler: function () {
                        console.log("Cancel clicked");
                    }
                }
            ]
        });
        actionSheet.present();
    };
    EditoutletPage.prototype.ionViewDidLoad = function () {
        this.img = "../assets/img/users.png";
        var cl = localStorage.getItem('cl');
        this.getIdCluster(cl);
        this.getOutlet();
        this.getAreaCluster();
        this.getTypeOutlet();
    };
    // TO -> GET
    EditoutletPage.prototype.getTypeOutlet = function () {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getTypeOutlet");
        data.subscribe(function (result) {
            if (result['success'] == true) {
                _this.type_outlet = result['data'];
            }
            else {
                console.log("getTypeOutlet : ", result['success']);
            }
        });
    };
    EditoutletPage.prototype.toGetPosto = function () {
        this.getPosto(this.municipio);
    };
    EditoutletPage.prototype.toGetSuco = function () {
        this.getSuco(this.posto_adm);
    };
    EditoutletPage.prototype.toGetAldeia = function () {
        this.getAldeia(this.suco);
    };
    EditoutletPage.prototype.toGetLocation = function () {
        var _this = this;
        this.geolocation
            .getCurrentPosition()
            .then(function (resp) {
            _this.lat = resp.coords.latitude;
            _this.lng = resp.coords.longitude;
        })
            .catch(function (error) {
            _this.toast("Error getting location");
        });
    };
    EditoutletPage.prototype.getBase64 = function (file) {
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function () {
            console.log(reader.result);
        };
        reader.onerror = function (error) {
            console.log("Error: ", error);
        };
    };
    EditoutletPage.prototype.toGetCamera = function (mt) {
        var _this = this;
        var options = {
            quality: 50,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            sourceType: mt
        };
        this.camera.getPicture(options).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            var p = "data:image/jpeg;base64," + imageData;
            _this.img = p;
            _this.photo = p;
        }, function (err) {
            // Handle error
        });
    };
    EditoutletPage.prototype.toUploadPhoto = function (event) {
        var _this = this;
        if (event.target.files && event.target.files[0]) {
            var reader = new FileReader();
            reader.onload = function (event) {
                _this.img = event.target.result;
                _this.address = _this.img;
            };
            this.address = event.target.files[0];
            reader.readAsDataURL(event.target.files[0]);
        }
        var fileList = event.target.files;
        this.photo = fileList[0];
    };
    // POST
    EditoutletPage.prototype.addOutlet = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Please Wait.."
        });
        loading.present();
        if (this.name == "undefined" || this.name == undefined) {
            this.toast("Outlet name can not be empty!");
            loading.dismiss();
        }
        else if (this.contact_number == "undefined" || this.contact_number == undefined) {
            this.toast("Contact No. can not be empty!");
            loading.dismiss();
        }
        else {
            var body = new FormData();
            var rex = this.area.split(',');
            body.append("id", this.navParams.data.id);
            body.append("image", this.photo);
            body.append("name", this.name);
            body.append("idu", this.idu);
            body.append("contact_number", this.contact_number);
            body.append("pd_no", this.digital_number);
            body.append("cl", localStorage.getItem('cl'));
            body.append("address", this.address);
            body.append("email", this.email);
            body.append("typeOutlet", this.type);
            body.append("lat", this.lat);
            body.append("lng", this.lng);
            body.append("municipio", rex[1]);
            body.append("posto_adm", rex[2]);
            body.append("suco", rex[3]);
            body.append("aldeia", rex[0]);
            body.append("photo", this.photoz);
            body.append("created_by", localStorage.getItem("id"));
            body.append("idCh", localStorage.getItem("idCh"));
            // if(body == "") {
            //     this.toast("Error");
            // }
            this.http.post(this.service.urlRoot + "mwa/editOutlets", body)
                .subscribe(function (res) {
                _this.i = res;
                if (_this.i.info == 1) {
                    _this.toast("Sorry your name has been used, please enter other name!");
                }
                else if (_this.i.info == 2) {
                    _this.toast("Sorry your contact number has been used, please enter other number!");
                }
                else if (_this.i.info == 3) {
                    _this.toast("Sorry your digital number credit has been used, please enter other number!");
                }
                else if (_this.i.info == 4) {
                    _this.toast("Sorry your email entered has been used, please enter  the other email!");
                }
                else {
                    _this.toast("Success to Edit Outlet");
                    _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__outlet_outlet__["a" /* OutletPage */]);
                }
            }, function (err) {
                console.log(err);
                _this.toast("Error to Add Outlet");
            });
            loading.dismiss();
        }
    };
    // GET
    EditoutletPage.prototype.getIdCluster = function (id) {
        var _this = this;
        this.http.get(this.service.urlRoot + 'mwa/getIdCluster?id=' + id).subscribe(function (d) {
            _this.clusters = d[0].cluster;
        });
    };
    EditoutletPage.prototype.getAreaCluster = function () {
        var _this = this;
        var data = new FormData;
        this.http.get(this.service.urlRoot + 'mwa/getAreaCluster?cluster=' + localStorage.getItem('cl')).subscribe(function (d) {
            _this.municipio = d['municipio'];
            _this.posto_adm = d['posto_adm'];
        });
    };
    EditoutletPage.prototype.getAreaClusterSuco = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Please Wait.."
        });
        this.http.get(this.service.urlRoot + 'mwa/getAreaCluster?cluster=' + localStorage.getItem('cl') + '&posto=' + this.p).subscribe(function (d) {
            _this.suco = d['suco'];
        });
    };
    EditoutletPage.prototype.getAreaClusterAldeia = function () {
        var _this = this;
        var loading = this.loadingCtrl.create({
            content: "Please Wait.."
        });
        this.http.get(this.service.urlRoot + 'mwa/getAreaCluster?cluster=' + localStorage.getItem('cl') + '&suco=' + this.s).subscribe(function (d) {
            _this.aldeias = d['aldeia'];
        });
    };
    EditoutletPage.prototype.getMuicipio = function () {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getMunicipio");
        data.subscribe(function (result) {
            _this.munic = result;
        });
    };
    EditoutletPage.prototype.getPosto = function (id) {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getPosto?id=" + id);
        data.subscribe(function (result) {
            _this.posto = result;
        });
    };
    EditoutletPage.prototype.getSuco = function (id) {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getSuco?id=" + id);
        data.subscribe(function (result) {
            _this.sucos = result;
        });
    };
    EditoutletPage.prototype.getAldeia = function (id) {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getAldeia?id=" + id);
        data.subscribe(function (result) {
            _this.aldeias = result;
        });
    };
    EditoutletPage.prototype.toast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: "bottom"
        });
        toast.onDidDismiss(function () {
            console.log("Dismissed toast");
        });
        toast.present();
    };
    EditoutletPage.prototype.getOutlet = function () {
        var _this = this;
        var id = this.navParams.data.id;
        this.http.get(this.service.urlRoot + 'mwa/getOutlets?id=' + id).subscribe(function (data) {
            var v = data[0];
            _this.name = v.name;
            _this.contact_number = v.contact_number;
            _this.digital_number = v.pd_no;
            _this.address = v.address;
            _this.lat = v.lat;
            _this.email = v.email;
            _this.type = v.id_typeOutlet;
            _this.idu = v.id_user;
            _this.lng = v.lng;
            _this.p = v.p_id;
            _this.s = v.s_id;
            _this.getAreaClusterSuco();
            _this.getAreaClusterAldeia();
            _this.id_area = v.a_id + ',' + v.m_id + ',' + v.p_id + ',' + v.s_id;
            _this.area = _this.id_area;
            _this.img = _this.service.url + 'uploads/' + v.outlet_photo;
            console.log(_this.p);
        });
    };
    EditoutletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-editoutlet',template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/editoutlet/editoutlet.html"*/'<!--\n  Generated template for the EditoutletPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>Edit Outlet</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding >\n  <ion-label color="danger" stacked>Upload Photo</ion-label>\n  <!-- <ion-input type="file"  success block (change)="toUploadPhoto($event)"></ion-input> -->\n  <button ion-button color="danger" block (click)="presentActionSheet()">\n    Upload Foto\n  </button>\n\n  <ion-grid>\n    <ion-row>\n      <ion-col col-4>\n        <img [src]="img" (click)="toGetCamera($event)" src="https://ionicframework.com/dist/preview-app/www/assets/img/venkman.jpg">\n      </ion-col>\n      <ion-col col-8>\n        <ion-item id="updateOutletProfile-input10">\n          <ion-label color="danger" floating>Outlet Name.</ion-label>\n          <ion-input required type="text" [(ngModel)]="name"></ion-input>\n        </ion-item>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n\n  <ion-item id="updateOutletProfile-input11">\n    <ion-label color="danger" floating>Contact No.</ion-label>\n    <ion-input type="number" [(ngModel)]="contact_number"></ion-input>\n  </ion-item>\n  <ion-item id="updateOutletProfile-input11">\n    <ion-label color="danger" floating>Pulsa Digital No.</ion-label>\n    <ion-input type="number" readonly [(ngModel)]="digital_number"></ion-input>\n  </ion-item>\n  <ion-item id="updateOutletProfile-input13">\n    <ion-label color="danger" floating>Address</ion-label>\n    <ion-textarea type="text" [(ngModel)]="address"></ion-textarea>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-input12">\n    <ion-label color="danger" floating>Cluster</ion-label>\n    <ion-input type="text" readonly [(ngModel)]="clusters" ></ion-input>\n  </ion-item>\n  \n  <ion-item>\n    <ion-label color="danger" floating>Type Outlet</ion-label>\n    <ion-select [(ngModel)]="type">\n      <ion-option *ngFor="let i of type_outlet" value="{{i.id}}" >{{i.name}}</ion-option>\n    </ion-select>\n  </ion-item>\n  \n  <ion-item id="updateOutletProfile-input12">\n    <ion-label color="danger" floating>Municipio</ion-label>\n    <ion-input type="text" readonly [(ngModel)]="municipio" ></ion-input>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-select5">\n    <ion-label color="danger" floating>Posto Administativo</ion-label>\n    <ion-select  (ionChange)="getAreaClusterSuco()" [(ngModel)]="p">\n      <ion-option *ngFor="let i of posto_adm" value="{{i.id}}" >{{i.posto_adm}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-select5">\n    <ion-label color="danger" floating>Suco</ion-label>\n    <ion-select [(ngModel)]="s" (ionChange)="getAreaClusterAldeia()">\n      <ion-option *ngFor="let i of suco" value="{{i.id}}">{{i.suco}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item id="updateOutletProfile-select5">\n    <ion-label color="danger" floating>Aldeia</ion-label>\n    <ion-select [(ngModel)]="area" >\n      <ion-option *ngFor="let i of aldeias" value="{{i.a_id}},{{i.m_id}},{{i.pa_id}},{{i.s_id}}">{{i.aldeia}}</ion-option>\n    </ion-select>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="danger" floating>Email</ion-label>\n    <ion-input type="email" [(ngModel)]="email"></ion-input>\n  </ion-item>\n\n  <ion-item>\n    <button id="updateOutletProfile-button5" (click)="toGetLocation()" color="secondary" ion-button success block>\n      Get Current GPS Location\n    </button>\n  </ion-item>\n\n  <ion-item>\n    <ion-label color="danger" floating>Latitude</ion-label>\n    <ion-input type="text" [(ngModel)]="lat"></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label color="danger" floating>Longitude</ion-label>\n    <ion-input type="text" [(ngModel)]="lng"></ion-input>\n  </ion-item>\n\n  <button id="updateOutletProfile-button6" ion-button color="danger" block (click)="addOutlet()">\n    Save\n  </button>\n\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/editoutlet/editoutlet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* ActionSheetController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_service_service__["a" /* ServiceProvider */]])
    ], EditoutletPage);
    return EditoutletPage;
}());

//# sourceMappingURL=editoutlet.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(250);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(251);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_todo_list_todo_list__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_cloud_sharing_cloud_sharing__ = __webpack_require__(252);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_outlet_outlet__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_near_by_near_by__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_change_password_change_password__ = __webpack_require__(255);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, app, alertCtrl, http, geolocation) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.http = http;
        this.geolocation = geolocation;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */];
        this.initializeApp();
        this.appMenuItems = [
            { title: "Todo List", component: __WEBPACK_IMPORTED_MODULE_5__pages_todo_list_todo_list__["a" /* TodoListPage */], icon: "list-box" },
            { title: "Cloud Sharing", component: __WEBPACK_IMPORTED_MODULE_6__pages_cloud_sharing_cloud_sharing__["a" /* CloudSharingPage */], icon: "share" },
            { title: "Outlet", component: __WEBPACK_IMPORTED_MODULE_7__pages_outlet_outlet__["a" /* OutletPage */], icon: "cart" },
            { title: "Nearby", component: __WEBPACK_IMPORTED_MODULE_8__pages_near_by_near_by__["a" /* NearByPage */], icon: "search" }
        ];
        this.geolocation
            .getCurrentPosition()
            .then(function (resp) {
            _this.updateLocationSales(resp.coords.latitude, resp.coords.longitude);
        })
            .catch(function (error) {
            console.log("Error getting location", error);
        });
    }
    MyApp.prototype.updateLocationSales = function (lat, lng) {
        var body = new FormData();
        body.append("active", "1");
        body.append("lat", lat);
        body.append("lng", lng);
        body.append("users_id", JSON.parse(localStorage.getItem("id")));
        this.http
            .post("http://150.242.111.235/apibonita/index.php/mwa/upLocationSales", body)
            .subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log(err);
        });
    };
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Control statusbar
            _this.statusBar.styleDefault();
            // Control Splash Screen
            _this.splashScreen.hide();
            // Close this application
            _this.platform.registerBackButtonAction(function () {
                var nav = _this.app.getActiveNavs()[0];
                var activeView = nav.getActive();
                if (activeView.name === "BonitaMWAPage") {
                    var alert_1 = _this.alertCtrl.create({
                        title: "App termination",
                        message: "Do you want to close the app?",
                        buttons: [
                            {
                                text: "Cancel",
                                role: "cancel",
                                handler: function () {
                                    console.log("Application exit prevented!");
                                }
                            },
                            {
                                text: "Close App",
                                handler: function () {
                                    _this.platform.exitApp();
                                }
                            }
                        ]
                    });
                    alert_1.present();
                }
                else {
                    nav.pop();
                }
            });
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.push(page.component);
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: "Logout",
            message: "Would you like to logout?",
            buttons: [
                {
                    text: "No",
                    handler: function () {
                        console.log("Failed");
                    }
                },
                {
                    text: "Yes",
                    handler: function () {
                        localStorage.clear();
                        _this.nav.push(__WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    MyApp.prototype.changePassword = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_9__pages_change_password_change_password__["a" /* ChangePasswordPage */]);
    };
    MyApp.prototype.profile = function () {
        this.nav.push(__WEBPACK_IMPORTED_MODULE_10__pages_profile_profile__["a" /* ProfilePage */]);
    };
    MyApp.prototype.getUsers = function () {
        var _this = this;
        var i = JSON.parse(localStorage.getItem("id"));
        var data;
        data = this.http.get("http://150.242.111.235/apibonita/index.php/profile/getProfile/" + i);
        data.subscribe(function (result) {
            _this.users = result;
            console.log(_this.users);
        });
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/app/app.html"*/'<ion-menu side="left" id="authenticated" [content]="content">\n    <ion-header>\n        <ion-toolbar class="user-profile">\n            <ion-grid>\n                <ion-row menuClose (click)="profile()">\n                    <ion-col col-4>\n                        <div class="user-avatar">\n                            <img src="assets/img/users.png">\n                        </div>\n                    </ion-col>\n                    <ion-col style="padding-top: 25px" col-8>\n                        <h2 ion-text class="no-margin text-white">\n                            Profile\n                        </h2>\n                    </ion-col>\n                </ion-row>\n                <ion-row no-padding class="other-data">\n                    <ion-col no-padding class="column">\n                        <button ion-button icon-left small full class="btn-menu" menuClose (click)="changePassword()">\n                            <ion-icon name="key"></ion-icon>\n                            Change Password\n                        </button>\n                    </ion-col>\n                    <ion-col no-padding class="column">\n                        <button ion-button icon-left small full class="btn-menu" menuClose (click)="logout()">\n                            <ion-icon name="log-out"></ion-icon>\n                            Log Out\n                        </button>\n                    </ion-col>\n                </ion-row>\n            </ion-grid>\n        </ion-toolbar>\n    </ion-header>\n\n    <ion-content color="primary">\n        <ion-list class="user-list">\n            <button ion-item menuClose class="text-1x" *ngFor="let menuItem of appMenuItems" (click)="openPage(menuItem)">\n                <ion-icon item-left [name]="menuItem.icon" color="danger"></ion-icon>\n                <span ion-text color="danger">{{menuItem.title}}</span>\n                <!-- <span ion-text class="text-sm" color="danger">{{menuItem.desc}}</span> -->\n            </button>\n        </ion-list>\n    </ion-content>\n</ion-menu>\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_11__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_12__ionic_native_geolocation__["a" /* Geolocation */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NearbyOutletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_socket_io_client__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NearbyOutletPage = /** @class */ (function () {
    function NearbyOutletPage(navCtrl, service, http, geolocation, diagnostic) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.service = service;
        this.http = http;
        this.geolocation = geolocation;
        this.diagnostic = diagnostic;
        this.neighborhoods = [];
        // neighborhoods = [
        //   { lat: -6.239363, lng: 106.927389 },
        //   { lat: -6.242541, lng: 106.931585 },
        //   { lat: -6.242221, lng: 106.929622 },
        //   { lat: -6.241618, lng: 106.931231 },
        //   { lat: -6.244802, lng: 106.931306 },
        //   { lat: -6.243051, lng: 106.927808 }
        // ];
        this.markers = [];
        var successCallback = function (isAvailable) {
            if (isAvailable == true) {
            }
            else {
                alert("Please Enable Your Location.");
            }
        };
        var errorCallback = function (e) {
            alert("Error GPS tidak ditemukan : " + e);
        };
        this.diagnostic
            .isGpsLocationEnabled()
            .then(successCallback)
            .catch(errorCallback);
        this.socket = __WEBPACK_IMPORTED_MODULE_5_socket_io_client__("http://150.242.111.235:3000");
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
            _this.lat = data.coords.latitude;
            _this.lng = data.coords.longitude;
            _this.send(data);
        });
        this.getNearby();
    }
    NearbyOutletPage.prototype.getNearby = function () {
        var _this = this;
        var neighborhoods;
        neighborhoods = this.http.get(this.service.urlRoot + "mwa/getNearby");
        neighborhoods.subscribe(function (result) {
            for (var i = 0; i < result.length; ++i) {
                _this.neighborhoods.push({
                    lat: +result[i].lat,
                    lng: +result[i].lng,
                    name: result[i].name,
                    phone_number: result[i].phone_number,
                    email: result[i].email
                });
            }
            console.log(_this.neighborhoods);
        });
    };
    NearbyOutletPage.prototype.send = function (msg) {
        this.socket.emit("nearby", { id: this.socket.id, msg: msg });
    };
    NearbyOutletPage.prototype.ionViewWillEnter = function () {
        this.loadMap();
    };
    NearbyOutletPage.prototype.rad = function (x) {
        return (x * Math.PI) / 180;
    };
    NearbyOutletPage.prototype.getDistance = function (p1, p2) {
        var R = 6378137;
        var dLat = this.rad(p2.lat - p1.lat);
        var dLong = this.rad(p2.lng - p1.lng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(p1.lat)) *
                Math.cos(this.rad(p2.lat)) *
                Math.sin(dLong / 2) *
                Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    };
    NearbyOutletPage.prototype.doRefresh = function (refresher) {
        console.log("Begin async operation", refresher);
        setTimeout(function () {
            console.log("Async operation has ended");
            // refresher.complete();
        }, 2000);
    };
    NearbyOutletPage.prototype.loadMap = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (position) {
            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            _this.map = new google.maps.Map(_this.mapElement.nativeElement);
            var markerUser = new google.maps.Marker({
                position: latLng,
                map: _this.map,
                animation: google.maps.Animation.DROP
            });
            _this.socket.on("nearby", function (msg) {
                console.log(msg);
                markerUser.setPosition({ lat: _this.lat, lng: _this.lng });
                _this.ukur;
            });
            var content = "<div style='float:right; padding: 10px;'><h5><b>You are here!</b></h5><br/>" +
                lat +
                "," +
                lng +
                "</div>";
            _this.addInfoWindow(markerUser, content);
            var circle = new google.maps.Circle({
                map: _this.map,
                radius: 400,
                fillColor: "#7caeff",
                strokeOpacity: 0.3,
                strokeWeight: 1
            });
            circle.bindTo("center", markerUser, "position");
            _this.map.fitBounds(circle.getBounds());
            for (var i = 0; i < _this.neighborhoods.length; i++) {
                var objuser = {
                    lat: markerUser.position.lat(),
                    lng: markerUser.position.lng()
                };
                var neighLoc = _this.getDistance(_this.neighborhoods[i], objuser);
                if (neighLoc < 400) {
                    _this.addMarker(_this.neighborhoods[i], i * 200, _this.neighborhoods[i].name, _this.neighborhoods[i].phone_number, _this.neighborhoods[i].email);
                    if (neighLoc > 400) {
                        _this.marker.setMap(null);
                    }
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    NearbyOutletPage.prototype.addMarker = function (lat, lng, nme, no, email) {
        var cen = this.map.getCenter();
        if (lat !== undefined || lng !== undefined) {
            cen = new google.maps.LatLng(lat, lng);
        }
        var marker = new google.maps.Marker({
            map: this.map,
            icon: "./assets/img/position.png",
            animation: google.maps.Animation.DROP,
            position: cen
        });
        console.log(lat, lng);
        var content = "<div style='float:right; padding: 10px;'><b>Name : </b>" + nme +
            "<br/><b>Contact No : </b>" + no +
            "<br/><b>Email : </b>" + email +
            "<br/><hr>" + lat.lat + "," + lat.lng +
            "<button ion-button>Detail</button>" +
            "</div>";
        this.addInfoWindow(marker, content);
    };
    NearbyOutletPage.prototype.addInfoWindow = function (marker, content) {
        var _this = this;
        var infoWindow = new google.maps.InfoWindow({
            content: content,
            maxHeight: 100
        });
        google.maps.event.addListener(marker, "click", function () {
            console.log(marker);
            _this.closeLastOpenedInfoWindow();
            infoWindow.open(_this.map, marker);
            _this.lastOpenedInfoWindow = infoWindow;
        });
    };
    NearbyOutletPage.prototype.closeLastOpenedInfoWindow = function () {
        if (this.lastOpenedInfoWindow) {
            this.lastOpenedInfoWindow.close();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("maps"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], NearbyOutletPage.prototype, "mapElement", void 0);
    NearbyOutletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "nearby-outlet",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/nearby-outlet/nearby-outlet.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>\n      Near By\n    </ion-title>\n    <ion-buttons end>\n      <button (click)="doRefresh($event)" ion-button icon-only>\n        <ion-icon name="refresh"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div #maps id="map"></div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/nearby-outlet/nearby-outlet.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_6__providers_service_service__["a" /* ServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__["a" /* Diagnostic */]])
    ], NearbyOutletPage);
    return NearbyOutletPage;
}());

//# sourceMappingURL=nearby-outlet.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NearbyOutletsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_socket_io_client__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_socket_io_client__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NearbyOutletsPage = /** @class */ (function () {
    function NearbyOutletsPage(navCtrl, http, geolocation, diagnostic, service) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.http = http;
        this.geolocation = geolocation;
        this.diagnostic = diagnostic;
        this.service = service;
        this.radius = 300;
        this.neighborhoods = [];
        this.markers = [];
        var successCallback = function (isAvailable) {
            if (isAvailable == true) {
            }
            else {
                alert("Please Enable Your Location.");
            }
        };
        var errorCallback = function (e) {
            alert("Error GPS tidak ditemukan : " + e);
        };
        this.diagnostic
            .isGpsLocationEnabled()
            .then(successCallback)
            .catch(errorCallback);
        this.socket = __WEBPACK_IMPORTED_MODULE_5_socket_io_client__("http://150.242.111.235:3000");
        var watch = this.geolocation.watchPosition();
        watch.subscribe(function (data) {
            _this.lat = data.coords.latitude;
            _this.lng = data.coords.longitude;
            _this.send(data);
        });
        this.getNearby();
    }
    NearbyOutletsPage.prototype.getNearby = function () {
        var _this = this;
        var neighborhoods;
        neighborhoods = this.http.get(this.service.urlRoot + "mwa/getOutletsRadius?lat=-6.179321&lng=106.863293&range=" + this.radius + "&cluster_id=1");
        neighborhoods.subscribe(function (result) {
            for (var i = 0; i < result.length; ++i) {
                _this.neighborhoods.push({
                    lat: +result[i].lat,
                    lng: +result[i].lng,
                    name: result[i].outletname,
                    phone_number: result[i].contact_number,
                    address: result[i].address
                });
            }
            console.log(_this.neighborhoods);
        });
    };
    NearbyOutletsPage.prototype.send = function (msg) {
        this.socket.emit("nearby", msg);
    };
    NearbyOutletsPage.prototype.ionViewDidLoad = function () {
        this.loadMap();
    };
    NearbyOutletsPage.prototype.rad = function (x) {
        return (x * Math.PI) / 180;
    };
    NearbyOutletsPage.prototype.getDistance = function (p1, p2) {
        var R = 6378137;
        var dLat = this.rad(p2.lat - p1.lat);
        var dLong = this.rad(p2.lng - p1.lng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(p1.lat)) *
                Math.cos(this.rad(p2.lat)) *
                Math.sin(dLong / 2) *
                Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    };
    NearbyOutletsPage.prototype.doRefresh = function (refresher) {
        console.log("Begin async operation", refresher);
        setTimeout(function () {
            console.log("Async operation has ended");
            // refresher.complete();
        }, 2000);
    };
    NearbyOutletsPage.prototype.loadMap = function () {
        var _this = this;
        this.geolocation.getCurrentPosition().then(function (position) {
            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            _this.map = new google.maps.Map(_this.mapElement.nativeElement);
            var markerUser = new google.maps.Marker({
                position: latLng,
                map: _this.map,
                animation: google.maps.Animation.DROP
            });
            _this.socket.on("nearby", function (msg) {
                console.log(msg);
                markerUser.setPosition({ lat: _this.lat, lng: _this.lng });
                _this.ukur;
            });
            var content = "<div style='float:right; padding: 10px;'><h5><b>You are here!</b></h5><br/>" +
                lat +
                "," +
                lng +
                "</div>";
            _this.addInfoWindow(markerUser, content);
            var circle = new google.maps.Circle({
                map: _this.map,
                radius: _this.radius,
                fillColor: "#7caeff",
                strokeOpacity: 0.3,
                strokeWeight: 1
            });
            circle.bindTo("center", markerUser, "position");
            _this.map.fitBounds(circle.getBounds());
            for (var i = 0; i < _this.neighborhoods.length; i++) {
                var objuser = {
                    lat: markerUser.position.lat(),
                    lng: markerUser.position.lng()
                };
                var neighLoc = _this.getDistance(_this.neighborhoods[i], objuser);
                if (neighLoc < _this.radius) {
                    _this.addMarker(_this.neighborhoods[i], i * 200, _this.neighborhoods[i].name, _this.neighborhoods[i].phone_number, _this.neighborhoods[i].address);
                    if (neighLoc > _this.radius) {
                        _this.marker.setMap(null);
                    }
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    NearbyOutletsPage.prototype.addMarker = function (lat, lng, nme, no, address) {
        var cen = this.map.getCenter();
        if (lat !== undefined || lng !== undefined) {
            cen = new google.maps.LatLng(lat, lng);
        }
        var marker = new google.maps.Marker({
            map: this.map,
            icon: "./assets/img/position.png",
            animation: google.maps.Animation.DROP,
            position: cen
        });
        console.log(lat, lng);
        var content = "<div style='float:right; padding: 10px;'><b>Name : </b>" + nme +
            "<br/><b>Contact No : </b>" + no +
            "<br/><b>Address : </b>" + address +
            "<br/><hr>" + lat.lat + "," + lat.lng +
            "</div>";
        this.addInfoWindow(marker, content);
    };
    NearbyOutletsPage.prototype.addInfoWindow = function (marker, content) {
        var _this = this;
        var infoWindow = new google.maps.InfoWindow({
            content: content,
            maxHeight: 100
        });
        google.maps.event.addListener(marker, "click", function () {
            console.log(marker);
            _this.closeLastOpenedInfoWindow();
            infoWindow.open(_this.map, marker);
            _this.lastOpenedInfoWindow = infoWindow;
        });
    };
    NearbyOutletsPage.prototype.closeLastOpenedInfoWindow = function () {
        if (this.lastOpenedInfoWindow) {
            this.lastOpenedInfoWindow.close();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("map"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], NearbyOutletsPage.prototype, "mapElement", void 0);
    NearbyOutletsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-nearby-outlets',template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/nearby-outlets/nearby-outlets.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>\n      Near By\n    </ion-title>\n    <ion-buttons end>\n      <button (click)="doRefresh($event)" ion-button icon-only>\n        <ion-icon name="refresh"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div #map id="map"></div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/nearby-outlets/nearby-outlets.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_6__providers_service_service__["a" /* ServiceProvider */]])
    ], NearbyOutletsPage);
    return NearbyOutletsPage;
}());

//# sourceMappingURL=nearby-outlets.js.map

/***/ }),

/***/ 55:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BonitaMWAPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__near_by_near_by__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(199);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__profile_profile__ = __webpack_require__(104);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__todo_list_todo_list__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(100);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_rest_rest__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_geolocation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_diagnostic__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var BonitaMWAPage = /** @class */ (function () {
    function BonitaMWAPage(navCtrl, geolocation, diagnostic, alertCtrl, authService, app, menuCtrl, platform, restProvider, http, popoverCtrl, service) {
        // let successCallback = isAvailable => {
        //   if (isAvailable == true) {
        //   } else {
        //     alert("Please Enable Your Location");
        //   }
        // };
        // let errorCallback = e => {
        //   alert("Error GPS Not Found: " + e);
        // };
        // this.diagnostic
        //   .isGpsLocationEnabled()
        //   .then(successCallback)
        //   .catch(errorCallback);
        var _this = this;
        this.navCtrl = navCtrl;
        this.geolocation = geolocation;
        this.diagnostic = diagnostic;
        this.alertCtrl = alertCtrl;
        this.authService = authService;
        this.app = app;
        this.menuCtrl = menuCtrl;
        this.platform = platform;
        this.restProvider = restProvider;
        this.http = http;
        this.popoverCtrl = popoverCtrl;
        this.service = service;
        this.items = [];
        this.userPostData = { user_id: "", token: "" };
        // this.getLoc();
        // console.log(latLng);
        var data;
        // tabel = getTodolist
        data = this.http.get(this.service.urlRoot + "mwa/getRouteDataTask?id=" +
            JSON.parse(localStorage.getItem("id")) + '&limit=5');
        data.subscribe(function (result) {
            for (var i = 0; i < result["data"].length; ++i) {
                _this.items.push({
                    task_name: result["data"][i].task_name,
                    idRt: result["data"][i].route_trip_fk,
                    idTask: result["data"][i].id,
                    status: _this.getStatus(result["data"][i].status)
                });
            }
            console.log(_this.items);
        });
        this.getUsers();
        if (localStorage.getItem("id") == null) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
        }
        else {
            console.log("berhasil login");
        }
    }
    BonitaMWAPage.prototype.getLoc = function () {
        this.geolocation.getCurrentPosition().then(function (position) {
            var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
        });
    };
    BonitaMWAPage.prototype.LoginPage = function () {
        var root = this.app.getRootNav();
        root.popToRoot();
    };
    BonitaMWAPage.prototype.logout = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: "Logout",
            message: "Would you like to logout?",
            buttons: [
                {
                    text: "No",
                    handler: function () {
                        console.log("Failed");
                    }
                },
                {
                    text: "Yes",
                    handler: function () {
                        localStorage.clear();
                        _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    BonitaMWAPage.prototype.getUsers = function () {
        var _this = this;
        var i = JSON.parse(localStorage.getItem("id"));
        var data;
        data = this.http.get(this.service.urlRoot + "profile/getProfile/" + i);
        data.subscribe(function (result) {
            _this.users = result;
            console.log(_this.users);
        });
    };
    BonitaMWAPage.prototype.getStatus = function (x) {
        if (x === "1") {
            return "In Progress";
        }
        else if (x === "2") {
            return "Follow Up";
        }
        else if (x === "3") {
            return "Completed";
        }
    };
    BonitaMWAPage.prototype.goToProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__profile_profile__["a" /* ProfilePage */]);
    };
    BonitaMWAPage.prototype.goToTodoList = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__todo_list_todo_list__["a" /* TodoListPage */]);
    };
    BonitaMWAPage.prototype.goToNearBy = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__near_by_near_by__["a" /* NearByPage */]);
    };
    BonitaMWAPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-bonita-mwa",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/bonita-mwa/bonita-mwa.html"*/'<ion-header>\n  <ion-navbar hideBackButton color="danger">\n    <button ion-button menuToggle>\n      <ion-icon name="menu"></ion-icon>\n    </button>\n    <ion-title>TELIN MALAYSIA</ion-title>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content class="bg-img">\n  <div class="animated fadeInDown">\n    <ion-card>\n      <div class="item-home" style="position: relative">\n        <ion-fab right top>\n          <button mini ion-fab color="danger" (click)="goToNearBy()">\n            <ion-icon name="pin"></ion-icon>\n          </button>\n        </ion-fab>\n      </div>\n      <ion-item *ngFor="let user of users">\n        <img src="assets/img/users.png" class="image-profile">\n        <h2>{{user.name}}</h2>\n        <p>{{user.username}}</p>\n        <p>{{user.phone_number}}</p>\n      </ion-item>\n    </ion-card>\n    <ion-card>\n      <div style="position: static"></div>\n      <ion-item actions>\n        <span ion-text item-start color="danger" class="item-bold text-1x">Todo List</span>\n        <button ion-button color="danger" clear item-end (click)="goToTodoList()">\n          View More\n        </button>\n      </ion-item>\n      <div *ngFor="let item of items;">\n        <ion-item >\n          <ion-icon color="subtle"  large item-start name=\'list\'></ion-icon>\n          <h2>{{item.task_name}}</h2>\n          <p style="color: red">{{item.status}}</p>\n        </ion-item>\n      </div>\n    </ion-card>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/bonita-mwa/bonita-mwa.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* MenuController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_8__providers_rest_rest__["a" /* RestProvider */],
            __WEBPACK_IMPORTED_MODULE_7__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* PopoverController */],
            __WEBPACK_IMPORTED_MODULE_11__providers_service_service__["a" /* ServiceProvider */]])
    ], BonitaMWAPage);
    return BonitaMWAPage;
}());

//# sourceMappingURL=bonita-mwa.js.map

/***/ }),

/***/ 56:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NearByPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__ = __webpack_require__(39);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_service_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_socket_io_client__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_socket_io_client___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_socket_io_client__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var NearByPage = /** @class */ (function () {
    function NearByPage(service, navCtrl, navParam, http, geolocation, diagnostic) {
        var _this = this;
        this.service = service;
        this.navCtrl = navCtrl;
        this.navParam = navParam;
        this.http = http;
        this.geolocation = geolocation;
        this.diagnostic = diagnostic;
        this.radius = 400;
        this.neighborhoods = [];
        // neighborhoods = [
        //   { lat: -6.239363, lng: 106.927389 },
        //   { lat: -6.242541, lng: 106.931585 },
        //   { lat: -6.242221, lng: 106.929622 },
        //   { lat: -6.241618, lng: 106.931231 },
        //   { lat: -6.244802, lng: 106.931306 },
        //   { lat: -6.243051, lng: 106.927808 }
        // ];
        this.markers = [];
        var successCallback = function (isAvailable) {
            if (isAvailable == true) {
            }
            else {
                alert("Please Enable Your Location.");
            }
        };
        var errorCallback = function (e) {
            alert("Error GPS tidak ditemukan : " + e);
        };
        this.diagnostic
            .isGpsLocationEnabled()
            .then(successCallback)
            .catch(errorCallback);
        this.socket = __WEBPACK_IMPORTED_MODULE_6_socket_io_client__("http://150.242.111.244:3000");
        var watch = this.geolocation.watchPosition();
        var lat = this.navParam.data.lat;
        var lng = this.navParam.data.lng;
        if (lat != undefined && lng != undefined) {
            this.lat = lat;
            this.lng = lng;
        }
        else {
            watch.subscribe(function (data) {
                _this.lat = data.coords.latitude;
                _this.lng = data.coords.longitude;
                _this.send(data);
            });
        }
        this.getNearby();
    }
    NearByPage.prototype.getNearby = function () {
        var _this = this;
        var neighborhoods;
        var lat = this.navParam.data.lat;
        var lng = this.navParam.data.lng;
        if (lat != undefined && lng != undefined) {
            neighborhoods = this.http.get(this.service.urlRoot + "mwa/getOutletsRadius?lat=" + lat + "&lng=" + lng + "&range=" + this.radius + "&cluster_id=" + localStorage.getItem('cl'));
            neighborhoods.subscribe(function (result) {
                for (var i = 0; i < result.length; ++i) {
                    _this.neighborhoods.push({
                        lat: +result[i].lat,
                        lng: +result[i].lng,
                        name: result[i].name,
                        phone_number: result[i].contact_number,
                        email: result[i].address,
                        status: result[i].status,
                        data: result[i]
                    });
                }
                console.log(_this.neighborhoods);
            });
        }
        else {
            neighborhoods = this.http.get(this.service.urlRoot + "mwa/getNearby?id_cluster=" + localStorage.getItem('cl'));
            neighborhoods.subscribe(function (result) {
                for (var i = 0; i < result.length; ++i) {
                    _this.neighborhoods.push({
                        lat: +result[i].lat,
                        lng: +result[i].lng,
                        name: result[i].name,
                        phone_number: result[i].contact_number,
                        email: result[i].email,
                        status: result[i].status,
                        data: result[i]
                    });
                }
                console.log(_this.neighborhoods);
            });
        }
    };
    NearByPage.prototype.send = function (msg) {
        this.socket.emit("nearby", { id: this.socket.id, msg: msg });
    };
    NearByPage.prototype.ionViewWillEnter = function () {
        this.loadMap();
    };
    NearByPage.prototype.rad = function (x) {
        return (x * Math.PI) / 180;
    };
    NearByPage.prototype.getDistance = function (p1, p2) {
        var R = 6378137;
        var dLat = this.rad(p2.lat - p1.lat);
        var dLong = this.rad(p2.lng - p1.lng);
        var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.rad(p1.lat)) *
                Math.cos(this.rad(p2.lat)) *
                Math.sin(dLong / 2) *
                Math.sin(dLong / 2);
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c;
        return d;
    };
    NearByPage.prototype.doRefresh = function (refresher) {
        console.log("Begin async operation", refresher);
        setTimeout(function () {
            console.log("Async operation has ended");
            // refresher.complete();
        }, 2000);
    };
    NearByPage.prototype.loadMap = function () {
        var _this = this;
        var lat1 = this.navParam.data.lat;
        var lng1 = this.navParam.data.lng;
        this.geolocation.getCurrentPosition().then(function (position) {
            if (lat1 != undefined && lng1 != undefined) {
                var latLng = new google.maps.LatLng(_this.lat, _this.lng);
            }
            else {
                var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
            }
            var lat = position.coords.latitude;
            var lng = position.coords.longitude;
            _this.map = new google.maps.Map(_this.mapElement.nativeElement);
            var markerUser = new google.maps.Marker({
                position: latLng,
                map: _this.map,
                animation: google.maps.Animation.DROP
            });
            if (lat1 != undefined && lng1 != undefined) {
                markerUser.setVisible(false);
            }
            else {
                markerUser.setVisible(true);
            }
            _this.socket.on("nearby", function (msg) {
                console.log(msg);
                markerUser.setPosition({ lat: _this.lat, lng: _this.lng });
                _this.ukur;
            });
            var content = "<div style='float:right; padding: 10px;'><h5><b>You are here!</b></h5><br/>" +
                lat +
                "," +
                lng +
                "</div>";
            _this.addInfoWindow(markerUser, content);
            var circle = new google.maps.Circle({
                map: _this.map,
                radius: _this.radius,
                fillColor: "#7caeff",
                strokeOpacity: 0.3,
                strokeWeight: 1
            });
            circle.bindTo("center", markerUser, "position");
            _this.map.fitBounds(circle.getBounds());
            for (var i = 0; i < _this.neighborhoods.length; i++) {
                var objuser = {
                    lat: markerUser.position.lat(),
                    lng: markerUser.position.lng()
                };
                var neighLoc = _this.getDistance(_this.neighborhoods[i], objuser);
                if (neighLoc < _this.radius) {
                    _this.addMarker(_this.neighborhoods[i], i * 200, _this.neighborhoods[i].name, _this.neighborhoods[i].phone_number, _this.neighborhoods[i].email, _this.neighborhoods[i].status, _this.neighborhoods[i].data);
                    if (neighLoc > _this.radius) {
                        _this.marker.setMap(null);
                    }
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    NearByPage.prototype.addMarker = function (lat, lng, nme, no, email, status, data) {
        var lat1 = this.navParam.data.lat;
        var lng1 = this.navParam.data.lng;
        var cen = this.map.getCenter();
        if (lat !== undefined || lng !== undefined) {
            cen = new google.maps.LatLng(lat, lng);
        }
        var img;
        if (status == "b") {
            img = './assets/img/bts.png';
        }
        else if (status == "o") {
            img = './assets/img/position.png';
        }
        else if (status == "s") {
            img = './assets/img/sales.png';
        }
        var marker = new google.maps.Marker({
            map: this.map,
            icon: img,
            animation: google.maps.Animation.DROP,
            position: cen
        });
        console.log('data : ' + data);
        var content;
        if (status == "b") {
            content =
                "<div style='float:right; padding: 10px;'><b>Name Site : </b>" + data.name +
                    "<br/><b>District : </b>" + data.district +
                    "<br/><b>Subdistrict : </b>" + data.subdistrict +
                    "<br/><b>Suco : </b>" + data.suco +
                    "<br/><b>Category : </b>" + data.category +
                    "<br/><b>Payload : </b>" + data.payload +
                    "<br/><b>Recharge : </b>" + data.recharge +
                    "<br/><b>Revenue : </b>" + data.revenue +
                    "<br/><hr>" + lat.lat + "," + lat.lng +
                    "</div>";
        }
        else {
            content =
                "<div style='float:right; padding: 10px;'><b>Name : </b>" + nme +
                    "<br/><b>Contact No : </b>" + no +
                    "<br/><b>Email : </b>" + email +
                    "<br/><hr>" + lat.lat + "," + lat.lng +
                    "</div>";
        }
        this.addInfoWindow(marker, content);
    };
    NearByPage.prototype.addInfoWindow = function (marker, content) {
        var _this = this;
        var infoWindow = new google.maps.InfoWindow({
            content: content,
            maxHeight: 100
        });
        google.maps.event.addListener(marker, "click", function () {
            console.log(marker);
            _this.closeLastOpenedInfoWindow();
            infoWindow.open(_this.map, marker);
            _this.lastOpenedInfoWindow = infoWindow;
        });
    };
    NearByPage.prototype.closeLastOpenedInfoWindow = function () {
        if (this.lastOpenedInfoWindow) {
            this.lastOpenedInfoWindow.close();
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("map"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], NearByPage.prototype, "mapElement", void 0);
    NearByPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-near-by",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/near-by/near-by.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>\n      Near By\n    </ion-title>\n    <ion-buttons end>\n      <button (click)="doRefresh($event)" ion-button icon-only>\n        <ion-icon name="refresh"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div #map id="map"></div>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/near-by/near-by.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_5__providers_service_service__["a" /* ServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_4__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__["a" /* Diagnostic */]])
    ], NearByPage);
    return NearByPage;
}());

//# sourceMappingURL=near-by.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TodoListPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__route_route__ = __webpack_require__(106);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__bonita_mwa_bonita_mwa__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var TodoListPage = /** @class */ (function () {
    function TodoListPage(navCtrl, http, toastCtrl, service) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.http = http;
        this.toastCtrl = toastCtrl;
        this.service = service;
        this.items = [];
        var data;
        // tabel = getTodolist
        data = this.http.get(this.service.urlRoot + "mwa/getRouteDataTask?id=" + JSON.parse(localStorage.getItem("id")));
        data.subscribe(function (result) {
            for (var i = 0; i < result["data"].length; ++i) {
                _this.items.push({
                    task_name: result["data"][i].task_name,
                    idRt: result["data"][i].route_trip_fk,
                    idTask: result["data"][i].id,
                    status: _this.getStatus(result["data"][i].status)
                });
            }
            console.log(_this.items);
        });
    }
    TodoListPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.navBar.backButtonClick = function (e) {
            // todo something
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__bonita_mwa_bonita_mwa__["a" /* BonitaMWAPage */]);
        };
    };
    TodoListPage.prototype.goToRoute = function (idRt, idTask) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__route_route__["a" /* RoutePage */], {
            id: idRt,
            idTask: idTask
        });
    };
    TodoListPage.prototype.getStatus = function (x) {
        if (x === "1") {
            return "In Progress";
        }
        else if (x === "2") {
            return "Follow Up";
        }
        else if (x === "3") {
            return "Completed";
        }
    };
    TodoListPage.prototype.presentToast = function (msg) {
        var toast = this.toastCtrl.create({
            message: msg,
            duration: 3000,
            position: "bottom"
        });
        toast.onDidDismiss(function () {
            console.log("Dismissed toast");
        });
        toast.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Navbar */])
    ], TodoListPage.prototype, "navBar", void 0);
    TodoListPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-todo-list",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/todo-list/todo-list.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>\n      Todo List\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content padding id="page4">\n  <ion-item color="none" id="todo-list-item3" *ngFor="let item of items"  on-click="goToRoute(item.idRt,item.idTask)">\n    <ion-icon color="subtle" large item-start name=\'list\'></ion-icon>\n    <h2>{{item.task_name}}</h2>\n    <p style="color: red">{{item.status}}</p>\n  </ion-item>\n</ion-content>'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/todo-list/todo-list.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["p" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_service_service__["a" /* ServiceProvider */]])
    ], TodoListPage);
    return TodoListPage;
}());

//# sourceMappingURL=todo-list.js.map

/***/ }),

/***/ 63:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OutletPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__update_outlet_profile_update_outlet_profile__ = __webpack_require__(208);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__addoutlet_addoutlet__ = __webpack_require__(209);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__bonita_mwa_bonita_mwa__ = __webpack_require__(55);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var OutletPage = /** @class */ (function () {
    function OutletPage(navCtrl, http, service) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.service = service;
        this.items = [];
        this.order = 0;
        this.lim = 20;
        this.order2 = 0;
        this.lim2 = 20;
    }
    OutletPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.loadData();
        this.navBar.backButtonClick = function (e) {
            // todo something
            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__bonita_mwa_bonita_mwa__["a" /* BonitaMWAPage */]);
        };
    };
    OutletPage.prototype.ionViewWillEnter = function () {
    };
    OutletPage.prototype.doRefresh = function (refresher) {
        this.loadData();
        setTimeout(function () {
            refresher.complete();
        }, 300);
    };
    OutletPage.prototype.loadData = function () {
        var _this = this;
        this.items = [];
        var data;
        data = this.http.get(this.service.urlRoot + "mwa/getOutletMWA?cluster=" + localStorage.getItem('cl') + '&limit=' + this.lim + '&order=0');
        data.subscribe(function (result) {
            if (result.success == true) {
                for (var i = 0; i < result.data.length; ++i) {
                    _this.items.push({
                        id: result.data[i].id,
                        name: result.data[i].name,
                        img: _this.cekImg(result.data[i].outlet_photo)
                    });
                }
            }
        });
    };
    OutletPage.prototype.cekImg = function (img) {
        if (img !== "null" && img !== null) {
            return this.service.url + "uploads/" + img;
        }
        else {
            return "assets/img/users.png";
        }
    };
    OutletPage.prototype.toGetOutlet = function (d) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__update_outlet_profile_update_outlet_profile__["a" /* UpdateOutletProfilePage */], {
            id: d
        });
    };
    OutletPage.prototype.addOutlet = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__addoutlet_addoutlet__["a" /* AddoutletPage */]);
    };
    OutletPage.prototype.doInfinite = function (infiniteScroll) {
        var _this = this;
        if (this.myInput != '' && this.myInput != undefined) {
            this.order2 = (this.order2 + this.lim2);
            this.order = 0;
            setTimeout(function () {
                var data;
                data = _this.http.get(_this.service.urlRoot + "mwa/findOutletMwa?cluster=" + localStorage.getItem('cl') + "&cari=" + _this.myInput + "&limit=" + _this.lim2 + "&order=" + _this.order2);
                data.subscribe(function (result) {
                    if (result.success == true) {
                        for (var i = 0; i < result.data.length; ++i) {
                            _this.items.push({
                                id: result.data[i].id,
                                name: result.data[i].name,
                                img: _this.cekImg(result.data[i].outlet_photo)
                            });
                        }
                    }
                });
                infiniteScroll.complete();
            }, 300);
        }
        else {
            this.order2 = 0;
            setTimeout(function () {
                _this.order = (_this.order + _this.lim);
                var data;
                data = _this.http.get(_this.service.urlRoot + "mwa/getOutletMwa?cluster=" + localStorage.getItem('cl') + "&limit=" + _this.lim + "&order=" + _this.order);
                data.subscribe(function (result) {
                    if (_this.order > result.count) {
                        console.log('gak bisa reload lagi bos');
                    }
                    else {
                        if (result.success == true) {
                            for (var i = 0; i < result.data.length; ++i) {
                                _this.items.push({
                                    id: result.data[i].id,
                                    name: result.data[i].name,
                                    img: _this.cekImg(result.data[i].outlet_photo)
                                });
                            }
                        }
                    }
                });
                infiniteScroll.complete();
            }, 300);
        }
    };
    OutletPage.prototype.onInput = function (event) {
        var _this = this;
        if (this.myInput == '') {
            this.order = 0;
            this.loadData();
        }
        else {
            this.items = [];
            var data = void 0;
            data = this.http.get(this.service.urlRoot + "mwa/findOutletMwa?cluster=" + localStorage.getItem('cl') + "&cari=" + this.myInput + "&limit=" + this.lim2 + "&order=0");
            data.subscribe(function (result) {
                if (result.success == true) {
                    for (var i = 0; i < result.data.length; ++i) {
                        _this.items.push({
                            id: result.data[i].id,
                            name: result.data[i].name,
                            img: _this.cekImg(result.data[i].outlet_photo)
                        });
                    }
                }
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Navbar */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* Navbar */])
    ], OutletPage.prototype, "navBar", void 0);
    OutletPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-outlet",template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/outlet/outlet.html"*/'<ion-header>\n  <ion-navbar color="danger">\n    <ion-title>\n      Outlet\n    </ion-title>\n    <ion-buttons end>\n      <button (click)="addOutlet()" ion-button icon-only>\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n  <ion-searchbar\n  [(ngModel)]="myInput"\n  [showCancelButton]="shouldShowCancel"\n  (ionInput)="onInput($event)"\n  (ionCancel)="onCancel($event)">\n</ion-searchbar>\n</ion-header>\n\n<ion-content padding id="page6">\n\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content\n      pullingIcon="arrow-dropdown"\n      pullingText="Pull to refresh"\n      refreshingText="Refreshing..."></ion-refresher-content>\n  </ion-refresher>\n  <ion-list id="outlet-list4">\n    <ion-item color="none" id="outlet-list-item10" *ngFor="let item of items" (click)="toGetOutlet(item.id)">\n      <ion-avatar item-left>\n        <img src="{{item.img}}" />\n      </ion-avatar>\n      <h2>{{item.name}}</h2>\n    </ion-item>\n  </ion-list>\n\n  <ion-infinite-scroll (ionInfinite)="doInfinite($event)">\n      <ion-infinite-scroll-content></ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n\n</ion-content>  '/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/outlet/outlet.html"*/
        })
        // getMunicipio,getPosto,getSuco,getLaldy
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_6__providers_service_service__["a" /* ServiceProvider */]])
    ], OutletPage);
    return OutletPage;
}());

//# sourceMappingURL=outlet.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ViewActivityPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(29);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(3);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__stock_survey_stock_survey__ = __webpack_require__(183);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_service_service__ = __webpack_require__(10);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






/**
 * Generated class for the ViewActivityPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ViewActivityPage = /** @class */ (function () {
    function ViewActivityPage(navCtrl, service, navParams, http, sanitizer) {
        this.navCtrl = navCtrl;
        this.service = service;
        this.navParams = navParams;
        this.http = http;
        this.sanitizer = sanitizer;
        this.isChecked = [];
        this.id = this.navParams.get('id');
        this.idRtd = this.navParams.get('idRtd');
        this.data = [];
    }
    ViewActivityPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ViewActivityPage');
        this.getViewActiviy();
    };
    ViewActivityPage.prototype.cekStatusString = function (c) {
        if (c == '1') {
            return true;
        }
        else {
            return false;
        }
    };
    ViewActivityPage.prototype.cekStatusInt = function (c) {
        if (c == true) {
            return 1;
        }
        else {
            return 0;
        }
    };
    ViewActivityPage.prototype.getViewActiviy = function () {
        var _this = this;
        var data;
        data = this.http.get(this.service.urlRoot + 'mwa/getActivity?idTask=' + this.id + '&idRtd=' + this.idRtd);
        data.subscribe(function (result) {
            for (var i = 0; i < result.length; i++) {
                var d = result[i];
                _this.isChecked[d.id] = _this.cekStatusString(d.check);
                _this.data.push({
                    id: d.id,
                    tasks_id: d.tasks_id,
                    activity: d.activity,
                    check: _this.cekStatusString(d.check)
                });
            }
            console.log(_this.data);
        });
    };
    ViewActivityPage.prototype.updateViewActivity = function (id, check) {
        var data;
        data = this.http.get(this.service.urlRoot + 'mwa/updateActivity?id=' + id + '&check=' + check);
        data.subscribe(function (result) {
            // this.saved = "<ion-label color='#success' style='font-size:12px;'>saved</ion-label>";
        });
    };
    ViewActivityPage.prototype.doCheck = function (id, check) {
        this.updateViewActivity(id, this.cekStatusInt(this.isChecked[id]));
    };
    ViewActivityPage.prototype.next = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__stock_survey_stock_survey__["a" /* StockSurveyPage */]);
    };
    ViewActivityPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-view-activity',template:/*ion-inline-start:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/view-activity/view-activity.html"*/'<!--\n  Generated template for the ViewActivityPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar color="danger">\n    <ion-title>View Activity List</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n\n  <ion-list >\n\n    <ion-item *ngFor="let item of data">\n      <ion-label>{{item.activity}}</ion-label>\n      <ion-checkbox [(ngModel)]="isChecked[item.id]" (click)="doCheck(item.id,item.check)"></ion-checkbox>\n    </ion-item>\n\n  </ion-list>\n  <button ion-button round outline block (click)="next()">Next</button>\n</ion-content>\n'/*ion-inline-end:"/Users/macbook/Documents/IONIC/app-telin-mwa/src/pages/view-activity/view-activity.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavController */], __WEBPACK_IMPORTED_MODULE_5__providers_service_service__["a" /* ServiceProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["l" /* NavParams */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ViewActivityPage);
    return ViewActivityPage;
}());

//# sourceMappingURL=view-activity.js.map

/***/ })

},[256]);
//# sourceMappingURL=main.js.map